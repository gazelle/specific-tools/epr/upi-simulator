package net.ihe.gazelle.upi.ws.ech_0214;

import net.ihe.gazelle.upi.model.ech_0044.DatePartiallyKnownType;
import net.ihe.gazelle.upi.model.ech_0058.HeaderType;
import net.ihe.gazelle.upi.model.ech_0213_commons.PersonToUPIType;
import net.ihe.gazelle.upi.model.ech_0214.Request;
import net.ihe.gazelle.upi.model.ech_0214.Response;
import net.ihe.gazelle.upi.ech_0214.business.ResponseManager;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;

import javax.xml.datatype.DatatypeConfigurationException;
import javax.xml.datatype.DatatypeFactory;
import javax.xml.datatype.XMLGregorianCalendar;
import java.math.BigInteger;
import java.util.ArrayList;
import java.util.List;

import static org.junit.jupiter.api.Assertions.assertEquals;

class ResponseManagerTest {

    private static ResponseManager responseManager;

    @BeforeAll
    static void setup() {
        responseManager = new ResponseManager();
    }

    @Test
    void createResponseWithNoContent() {
        Request request = new Request();
        HeaderType headerType = new HeaderType();
        request.setHeader(headerType);
        Response response = responseManager.createResponse(request);
        assertEquals("Le contenu de la requête est null", response.getNegativeReport().getNotice().getCodeDescription());
    }

    @Test
    void createResponseWithNoSpidCategoryRequest() {
        Request request = new Request();
        Request.Content content = new Request.Content();
        request.setContent(content);
        Response response = responseManager.createResponse(request);
        assertEquals("Le SPIDCategory de la requête est null", response.getNegativeReport().getNotice().getCodeDescription());
    }

    @Test
    void createResponseWithNoLanguageRequest() {
        Request request = new Request();
        Request.Content content = new Request.Content();
        content.setSPIDCategory("spid");
        request.setContent(content);
        Response response = responseManager.createResponse(request);
        assertEquals("La langue attendue dans la réponse est null", response.getNegativeReport().getNotice().getCodeDescription());
    }

    @Test
    void createResponseWithNoParameterInRequest() {
        Request request = new Request();
        Request.Content content = new Request.Content();
        content.setSPIDCategory("spid");
        content.setResponseLanguage("FR");
        request.setContent(content);
        Response response = responseManager.createResponse(request);
        assertEquals("Aucune réponse n'a pu être fournie", response.getNegativeReport().getNotice().getCodeDescription());
    }

    @Test
    void createResponseWithGetInfoPersonRequestWithExistingVn() {
        long vn = new BigInteger("7560409928839").longValue();
        Request.Content.GetInfoPersonRequest.Pid pid = new Request.Content.GetInfoPersonRequest.Pid();
        pid.setVn(vn);

        Request.Content.GetInfoPersonRequest getInfoPersonRequest = new Request.Content.GetInfoPersonRequest();
        getInfoPersonRequest.setPid(pid);

        List<Request.Content.GetInfoPersonRequest> getInfoPersonRequests = new ArrayList<>();
        getInfoPersonRequests.add(getInfoPersonRequest);

        Request request = new Request();
        Request.Content content = new Request.Content();
        content.setSPIDCategory("spid");
        content.setResponseLanguage("FR");
        content.setGetInfoPersonRequest(getInfoPersonRequests);
        request.setContent(content);
        Response response = responseManager.createResponse(request);
        assert response.getPositiveResponse().getGetInfoPersonResponse().get(0).getPersonFromUPI() != null;
    }

    @Test
    void createResponseWithGetInfoPersonRequestWithNotExistingVn() {
        long vn = new BigInteger("7560409900000").longValue();
        Request.Content.GetInfoPersonRequest.Pid pid = new Request.Content.GetInfoPersonRequest.Pid();
        pid.setVn(vn);

        Request.Content.GetInfoPersonRequest getInfoPersonRequest = new Request.Content.GetInfoPersonRequest();
        getInfoPersonRequest.setPid(pid);

        List<Request.Content.GetInfoPersonRequest> getInfoPersonRequests = new ArrayList<>();
        getInfoPersonRequests.add(getInfoPersonRequest);

        Request request = new Request();
        Request.Content content = new Request.Content();
        content.setSPIDCategory("spid");
        content.setResponseLanguage("FR");
        content.setGetInfoPersonRequest(getInfoPersonRequests);
        request.setContent(content);
        Response response = responseManager.createResponse(request);
        assertEquals("Ce NAVS n'existe pas", response.getPositiveResponse().getGetInfoPersonResponse().get(0).getNegativReportOnGetInfoPerson().getNotice().getCodeDescription());
    }

    @Test
    void createResponseWithGetInfoPersonRequestWithExistingSpid() {
        String spid = "761337610409928839";

        Request.Content.GetInfoPersonRequest.Pid pid = new Request.Content.GetInfoPersonRequest.Pid();
        pid.setSPID(spid);

        Request.Content.GetInfoPersonRequest getInfoPersonRequest = new Request.Content.GetInfoPersonRequest();
        getInfoPersonRequest.setPid(pid);

        List<Request.Content.GetInfoPersonRequest> getInfoPersonRequests = new ArrayList<>();
        getInfoPersonRequests.add(getInfoPersonRequest);

        Request request = new Request();
        Request.Content content = new Request.Content();
        content.setSPIDCategory("spid");
        content.setResponseLanguage("FR");
        content.setGetInfoPersonRequest(getInfoPersonRequests);
        request.setContent(content);
        Response response = responseManager.createResponse(request);
        assert response.getPositiveResponse().getGetInfoPersonResponse().get(0).getPersonFromUPI() != null;
    }

    @Test
    void createResponseWithSearchPersonRequestFound() throws DatatypeConfigurationException {
        Request.Content.SearchPersonRequest searchPersonRequest = new Request.Content.SearchPersonRequest();
        PersonToUPIType personToUPIType = new PersonToUPIType();
        DatePartiallyKnownType dateOfBirth = new DatePartiallyKnownType();
        XMLGregorianCalendar calendar = DatatypeFactory.newInstance().newXMLGregorianCalendar();
        calendar.setYear(1985);
        calendar.setMonth(6);
        calendar.setDay(29);
        dateOfBirth.setYearMonthDay(calendar);
        personToUPIType.setDateOfBirth(dateOfBirth);
        personToUPIType.setFirstName("benjamin");
        personToUPIType.setOfficialName("ANDREONI");

        searchPersonRequest.setSearchedPerson(personToUPIType);
        List<Request.Content.SearchPersonRequest> searchPersonRequests = new ArrayList<>();
        searchPersonRequests.add(searchPersonRequest);
        Request request = new Request();
        Request.Content content = new Request.Content();
        content.setSPIDCategory("spid");
        content.setResponseLanguage("FR");
        content.setSearchPersonRequest(searchPersonRequests);
        request.setContent(content);
        Response response = responseManager.createResponse(request);
        assert response.getPositiveResponse().getSearchPersonResponse().get(0).getFound() != null;
    }

    @Test
    void createResponseWithSearchPersonRequestNotFound() throws DatatypeConfigurationException {
        Request.Content.SearchPersonRequest searchPersonRequest = new Request.Content.SearchPersonRequest();
        PersonToUPIType personToUPIType = new PersonToUPIType();
        DatePartiallyKnownType dateOfBirth = new DatePartiallyKnownType();
        XMLGregorianCalendar calendar = DatatypeFactory.newInstance().newXMLGregorianCalendar();
        calendar.setYear(1985);
        calendar.setMonth(6);
        calendar.setDay(29);
        dateOfBirth.setYearMonthDay(calendar);
        personToUPIType.setDateOfBirth(dateOfBirth);
        personToUPIType.setFirstName("James");
        personToUPIType.setOfficialName("Bond");

        searchPersonRequest.setSearchedPerson(personToUPIType);
        List<Request.Content.SearchPersonRequest> searchPersonRequests = new ArrayList<>();
        searchPersonRequests.add(searchPersonRequest);
        Request request = new Request();
        Request.Content content = new Request.Content();
        content.setSPIDCategory("spid");
        content.setResponseLanguage("FR");
        content.setSearchPersonRequest(searchPersonRequests);
        request.setContent(content);
        Response response = responseManager.createResponse(request);
        assert response.getPositiveResponse().getSearchPersonResponse().get(0).getNotFound() != null;
    }

    @Test
    void createResponseWithCompareDataRequestWithMatchingVnSpid() {
        List<Request.Content.CompareDataRequest> getInfoPersonRequests = new ArrayList<>();
        Request.Content.CompareDataRequest compareDataRequest = new Request.Content.CompareDataRequest();
        Request.Content.CompareDataRequest.Pids pid = new Request.Content.CompareDataRequest.Pids();
        long vn = new BigInteger("7560409928839").longValue();
        String spid = "761337610409928839";
        pid.setVn(vn);
        pid.setSPID(spid);
        compareDataRequest.setPids(pid);
        getInfoPersonRequests.add(compareDataRequest);

        Request request = new Request();
        Request.Content content = new Request.Content();
        content.setSPIDCategory("spid");
        content.setResponseLanguage("FR");
        content.setCompareDataRequest(getInfoPersonRequests);
        request.setContent(content);
        Response response = responseManager.createResponse(request);
        assert response.getPositiveResponse().getCompareDataResponse().get(0).getIdenticalData() != null;
    }

    @Test
    void createResponseWithCompareDataRequestWithNotMatchingVnSpid() {
        List<Request.Content.CompareDataRequest> getInfoPersonRequests = new ArrayList<>();
        Request.Content.CompareDataRequest compareDataRequest = new Request.Content.CompareDataRequest();
        Request.Content.CompareDataRequest.Pids pid = new Request.Content.CompareDataRequest.Pids();
        long vn = new BigInteger("7560409928839").longValue();
        String spid = "761337610409900000";
        pid.setVn(vn);
        pid.setSPID(spid);
        compareDataRequest.setPids(pid);
        getInfoPersonRequests.add(compareDataRequest);

        Request request = new Request();
        Request.Content content = new Request.Content();
        content.setSPIDCategory("spid");
        content.setResponseLanguage("FR");
        content.setCompareDataRequest(getInfoPersonRequests);
        request.setContent(content);
        Response response = responseManager.createResponse(request);
        assert response.getPositiveResponse().getCompareDataResponse().get(0).getDifferentData() != null;
    }

    @Test
    void createResponseWithCompareDataRequestWithUnknownVnSpid() {
        List<Request.Content.CompareDataRequest> getInfoPersonRequests = new ArrayList<>();
        Request.Content.CompareDataRequest compareDataRequest = new Request.Content.CompareDataRequest();
        Request.Content.CompareDataRequest.Pids pid = new Request.Content.CompareDataRequest.Pids();
        long vn = new BigInteger("7560409900000").longValue();
        String spid = "761337610409900000";
        pid.setVn(vn);
        pid.setSPID(spid);
        compareDataRequest.setPids(pid);
        getInfoPersonRequests.add(compareDataRequest);

        Request request = new Request();
        Request.Content content = new Request.Content();
        content.setSPIDCategory("spid");
        content.setResponseLanguage("FR");
        content.setCompareDataRequest(getInfoPersonRequests);
        request.setContent(content);
        Response response = responseManager.createResponse(request);
        assert response.getPositiveResponse().getCompareDataResponse().get(0).getNegativReportOnCompareData() != null;
    }

    @Test
    void createResponseWithDifferentRequestType() throws DatatypeConfigurationException {
        List<Request.Content.CompareDataRequest> getInfoPersonRequests = new ArrayList<>();
        Request.Content.CompareDataRequest compareDataRequest = new Request.Content.CompareDataRequest();
        Request.Content.CompareDataRequest.Pids pid = new Request.Content.CompareDataRequest.Pids();
        long vn = new BigInteger("7560409928839").longValue();
        String spid = "761337610409928839";
        pid.setVn(vn);
        pid.setSPID(spid);
        compareDataRequest.setPids(pid);
        getInfoPersonRequests.add(compareDataRequest);

        Request.Content.SearchPersonRequest searchPersonRequest = new Request.Content.SearchPersonRequest();
        PersonToUPIType personToUPIType = new PersonToUPIType();
        DatePartiallyKnownType dateOfBirth = new DatePartiallyKnownType();
        XMLGregorianCalendar calendar = DatatypeFactory.newInstance().newXMLGregorianCalendar();
        calendar.setYear(1985);
        calendar.setMonth(6);
        calendar.setDay(29);
        dateOfBirth.setYearMonthDay(calendar);
        personToUPIType.setDateOfBirth(dateOfBirth);
        personToUPIType.setFirstName("benjamin");
        personToUPIType.setOfficialName("ANDREONI");

        searchPersonRequest.setSearchedPerson(personToUPIType);
        List<Request.Content.SearchPersonRequest> searchPersonRequests = new ArrayList<>();
        searchPersonRequests.add(searchPersonRequest);

        Request request = new Request();
        Request.Content content = new Request.Content();
        content.setSPIDCategory("spid");
        content.setResponseLanguage("FR");
        content.setSearchPersonRequest(searchPersonRequests);
        content.setCompareDataRequest(getInfoPersonRequests);
        request.setContent(content);
        Response response = responseManager.createResponse(request);
        assertEquals("Plusieurs requêtes ayant des types différents détectées", response.getNegativeReport().getNotice().getCodeDescription());
    }
}
