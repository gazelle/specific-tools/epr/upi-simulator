package net.ihe.gazelle.upi.adapter;

import net.ihe.gazelle.upi.exception.InvalidParameterException;
import net.ihe.gazelle.upi.exception.MultipleFoundException;
import net.ihe.gazelle.upi.exception.NotFoundException;
import net.ihe.gazelle.upi.model.ech_0044.DatePartiallyKnownType;
import net.ihe.gazelle.upi.model.ech_0213_commons.PersonFromUPIType;
import net.ihe.gazelle.upi.model.ech_0213_commons.PersonToUPIType;
import net.ihe.gazelle.upi.model.ech_0214.Request;
import net.ihe.gazelle.upi.model.ech_0214.Response;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;

import javax.xml.datatype.DatatypeConfigurationException;
import javax.xml.datatype.DatatypeFactory;
import javax.xml.datatype.XMLGregorianCalendar;

import java.math.BigInteger;
import java.util.List;

import static org.junit.jupiter.api.Assertions.assertThrows;

class PatientTest {
    private static PatientDAO patient;

    @BeforeAll
    static void setup() throws Exception {
        patient = new Patient();
    }

    @Test
    void getSearchPersonResponseFromSearchPersonRequestWithWrongParameter() throws DatatypeConfigurationException {
        Request.Content.SearchPersonRequest searchPersonRequest = new Request.Content.SearchPersonRequest();
        PersonToUPIType personToUPIType = new PersonToUPIType();
        DatePartiallyKnownType dateOfBirth = new DatePartiallyKnownType();
        XMLGregorianCalendar calendar = DatatypeFactory.newInstance().newXMLGregorianCalendar();
        calendar.setYear(1920);
        calendar.setMonth(11);
        calendar.setDay(11);
        dateOfBirth.setYearMonthDay(calendar);
        personToUPIType.setDateOfBirth(dateOfBirth);
        personToUPIType.setFirstName("<James");
        personToUPIType.setOfficialName("Bond>");

        searchPersonRequest.setSearchedPerson(personToUPIType);

        assertThrows(InvalidParameterException.class, () -> {
            patient.getSearchPersonResponseFromSearchPersonRequest(searchPersonRequest);
        });
    }


    @Test
    void getSearchPersonResponseFromSearchPersonRequestWithZeroResult() throws DatatypeConfigurationException, NotFoundException, InvalidParameterException {
        Request.Content.SearchPersonRequest searchPersonRequest = new Request.Content.SearchPersonRequest();
        PersonToUPIType personToUPIType = new PersonToUPIType();
        DatePartiallyKnownType dateOfBirth = new DatePartiallyKnownType();
        XMLGregorianCalendar calendar = DatatypeFactory.newInstance().newXMLGregorianCalendar();
        calendar.setYear(1920);
        calendar.setMonth(11);
        calendar.setDay(11);
        dateOfBirth.setYearMonthDay(calendar);
        personToUPIType.setDateOfBirth(dateOfBirth);
        personToUPIType.setFirstName("James");
        personToUPIType.setOfficialName("Bond");

        searchPersonRequest.setSearchedPerson(personToUPIType);
        Response.PositiveResponse.SearchPersonResponse searchPersonResponse = patient.getSearchPersonResponseFromSearchPersonRequest(searchPersonRequest);
        assert searchPersonResponse.getNotFound() != null;
    }

    @Test
    void getSearchPersonResponseFromSearchPersonRequestWithOneResult() throws DatatypeConfigurationException, NotFoundException, InvalidParameterException {
        Request.Content.SearchPersonRequest searchPersonRequest = new Request.Content.SearchPersonRequest();
        PersonToUPIType personToUPIType = new PersonToUPIType();
        DatePartiallyKnownType dateOfBirth = new DatePartiallyKnownType();
        XMLGregorianCalendar calendar = DatatypeFactory.newInstance().newXMLGregorianCalendar();
        calendar.setYear(1985);
        calendar.setMonth(6);
        calendar.setDay(29);
        dateOfBirth.setYearMonthDay(calendar);
        personToUPIType.setDateOfBirth(dateOfBirth);
        personToUPIType.setFirstName("benjamin");
        personToUPIType.setOfficialName("ANDREONI");

        searchPersonRequest.setSearchedPerson(personToUPIType);
        Response.PositiveResponse.SearchPersonResponse searchPersonResponse = patient.getSearchPersonResponseFromSearchPersonRequest(searchPersonRequest);
        assert searchPersonResponse.getFound() != null;
    }

    @Test
    void getSearchPersonResponseFromSearchPersonRequestWithTwoResult() throws DatatypeConfigurationException, NotFoundException, InvalidParameterException {
        Request.Content.SearchPersonRequest searchPersonRequest = new Request.Content.SearchPersonRequest();
        PersonToUPIType personToUPIType = new PersonToUPIType();
        DatePartiallyKnownType dateOfBirth = new DatePartiallyKnownType();
        XMLGregorianCalendar calendar = DatatypeFactory.newInstance().newXMLGregorianCalendar();
        calendar.setYear(2016);
        calendar.setMonth(6);
        calendar.setDay(29);
        dateOfBirth.setYearMonthDay(calendar);
        personToUPIType.setDateOfBirth(dateOfBirth);
        personToUPIType.setFirstName("Leo");
        personToUPIType.setOfficialName("Mustafic-Husic");

        searchPersonRequest.setSearchedPerson(personToUPIType);
        Response.PositiveResponse.SearchPersonResponse searchPersonResponse = patient.getSearchPersonResponseFromSearchPersonRequest(searchPersonRequest);
        assert searchPersonResponse.getMaybeFound() != null;
    }

    @Test
    void getInfoPersonFromRequestWithValidVn() throws Exception {
        Request.Content.GetInfoPersonRequest getInfoPersonRequest = new Request.Content.GetInfoPersonRequest();
        Request.Content.GetInfoPersonRequest.Pid pid = new Request.Content.GetInfoPersonRequest.Pid();
        long vn = new BigInteger("7560409928839").longValue();
        pid.setVn(vn);
        getInfoPersonRequest.setPid(pid);
        Response.PositiveResponse.GetInfoPersonResponse getInfoPersonResponse = patient.getInfoPersonFromPersonRequest(getInfoPersonRequest);
        assert getInfoPersonResponse.getPersonFromUPI() != null;
    }

    @Test
    void getInfoPersonFromRequestWithInvalidVn() {
        Request.Content.GetInfoPersonRequest getInfoPersonRequest = new Request.Content.GetInfoPersonRequest();
        Request.Content.GetInfoPersonRequest.Pid pid = new Request.Content.GetInfoPersonRequest.Pid();
        long vn = new BigInteger("7560409900000").longValue();
        pid.setVn(vn);
        getInfoPersonRequest.setPid(pid);

        assertThrows(NotFoundException.class, () -> {
            patient.getInfoPersonFromPersonRequest(getInfoPersonRequest);
        });
    }

    @Test
    void getInfoPersonFromRequestWithNorVnNorSpid() {
        Request.Content.GetInfoPersonRequest getInfoPersonRequest = new Request.Content.GetInfoPersonRequest();
        Request.Content.GetInfoPersonRequest.Pid pid = new Request.Content.GetInfoPersonRequest.Pid();

        pid.setVn(null);
        pid.setSPID(null);
        getInfoPersonRequest.setPid(pid);

        assertThrows(InvalidParameterException.class, () -> {
            patient.getInfoPersonFromPersonRequest(getInfoPersonRequest);
        });
    }

    @Test
    void getInfoPersonFromRequestWithSpid() throws Exception {
        Request.Content.GetInfoPersonRequest getInfoPersonRequest = new Request.Content.GetInfoPersonRequest();
        Request.Content.GetInfoPersonRequest.Pid pid = new Request.Content.GetInfoPersonRequest.Pid();
        String spid = "761337610409928839";
        pid.setSPID(spid);
        getInfoPersonRequest.setPid(pid);
        Response.PositiveResponse.GetInfoPersonResponse getInfoPersonResponse = patient.getInfoPersonFromPersonRequest(getInfoPersonRequest);
        assert getInfoPersonResponse.getPersonFromUPI() != null;
    }

    @Test
    void getCompareDataResponseFromDataRequestWithMatchingVnAndSpid() throws MultipleFoundException, NotFoundException {
        Request.Content.CompareDataRequest compareDataRequest = new Request.Content.CompareDataRequest();
        Request.Content.CompareDataRequest.Pids pid = new Request.Content.CompareDataRequest.Pids();
        long vn = new BigInteger("7560409928839").longValue();
        String spid = "761337610409928839";
        pid.setVn(vn);
        pid.setSPID(spid);
        compareDataRequest.setPids(pid);
        Response.PositiveResponse.CompareDataResponse compareDataResponse = patient.getCompareDataResponseFromDataRequest(compareDataRequest);
        assert compareDataResponse.getIdenticalData() != null;
    }


    @Test
    void getCompareDataResponseFromDataRequestWithExistingVn() throws MultipleFoundException, NotFoundException {
        Request.Content.CompareDataRequest compareDataRequest = new Request.Content.CompareDataRequest();
        Request.Content.CompareDataRequest.Pids pid = new Request.Content.CompareDataRequest.Pids();
        long vn = new BigInteger("7560409928839").longValue();
        String spid = "76133761040900000";
        pid.setVn(vn);
        pid.setSPID(spid);
        compareDataRequest.setPids(pid);
        Response.PositiveResponse.CompareDataResponse compareDataResponse = patient.getCompareDataResponseFromDataRequest(compareDataRequest);
        assert compareDataResponse.getDifferentData() != null;
    }

    @Test
    void getCompareDataResponseFromDataRequestWithExistingSpid() throws MultipleFoundException, NotFoundException {
        Request.Content.CompareDataRequest compareDataRequest = new Request.Content.CompareDataRequest();
        Request.Content.CompareDataRequest.Pids pid = new Request.Content.CompareDataRequest.Pids();
        long invalidVn = new BigInteger("7560409900000").longValue();
        String validSpid = "761337610409928839";
        pid.setVn(invalidVn);
        pid.setSPID(validSpid);
        compareDataRequest.setPids(pid);
        Response.PositiveResponse.CompareDataResponse compareDataResponse = patient.getCompareDataResponseFromDataRequest(compareDataRequest);
        assert compareDataResponse.getDifferentData() != null;
    }

    @Test
    void getCompareDataResponseFromDataRequestWithoutVnOrSpid() throws MultipleFoundException, NotFoundException {
        Request.Content.CompareDataRequest compareDataRequest = new Request.Content.CompareDataRequest();
        Request.Content.CompareDataRequest.Pids pid = new Request.Content.CompareDataRequest.Pids();
        long invalidVn = new BigInteger("7560409900000").longValue();
        String invalidSpid = "761337610409900000";
        pid.setVn(invalidVn);
        pid.setSPID(invalidSpid);
        compareDataRequest.setPids(pid);
        Response.PositiveResponse.CompareDataResponse compareDataResponse = patient.getCompareDataResponseFromDataRequest(compareDataRequest);
        assert compareDataResponse.getNegativReportOnCompareData() != null;
    }

    @Test
    void getPersonFromUpiTypeFromSpidTest() throws MultipleFoundException, NotFoundException {
        String spid = "761337610282287788";
        PersonFromUPIType personFromUPIType = patient.getPersonFromUpiTypeFromSpid(spid);
        assert personFromUPIType.getFirstName() != null;
    }

    @Test
    void getSpidsFromVnTest() throws MultipleFoundException, NotFoundException {
        String spid = "761337610282287788";
        String vn = patient.getVnFromSpid(spid);
        List<String> spids = patient.getSpidsFromVn(vn);
        assert spids.size() > 0;
    }
}