package net.ihe.gazelle.upi.mock.eCH_0214;

import net.ihe.gazelle.upi.ech_0213.utils.UPITransformer;
import net.ihe.gazelle.upi.model.ech_0214.Request;

import javax.xml.bind.JAXBException;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;

public class RequestMock {

    public Request instantiateRequestFromXML(String xmlLocation) {
        try (InputStream inputStream = new FileInputStream((xmlLocation))) {
            return UPITransformer.unmarshallMessage(Request.class, inputStream);
        } catch (IOException | JAXBException e) {
            e.printStackTrace();
        }
        return null;
    }
}
