package net.ihe.gazelle.upi.format.maps;

import net.ihe.gazelle.upi.model.ConstantValues;
import net.ihe.gazelle.upi.model.error.WarnErrorKeyValue;
import net.ihe.gazelle.upi.model.ech_0213.Request;

import java.util.Map;

public class MapInactivate implements IMapToMustache {

    final static String SPID1 = ConstantValues.SPID + 1;
    final static String SPID2 = ConstantValues.SPID + 2;

    @Override
    public Map<String, String> mapReqToMustache(Request req) {

        Map<String, String> mapInactivate;
        Map<String, String> mapOfPidFromReq = getMapOfPids(req);


        if (!areTwoSPIDPresent(mapOfPidFromReq)) {
            return mapForNegativeReport(WarnErrorKeyValue.CODE_ERROR_SPID_MANDATORY, WarnErrorKeyValue.MSG_ERROR_SPID_MANDATORY);
        }
        try {
            Map<String, String> personFromUpiMap = getMapPersonFromUpi(req);
            personFromUpiMap.putAll(mapOfPidFromReq);
            mapInactivate = mapForPostiveReport(req, personFromUpiMap);

            if (!ConstantValues.SPID_TO_INACTIVATE.equals(mapInactivate.get(SPID2))) {
                mapInactivate.remove(SPID2);
            }
            return mapInactivate;

        } catch (Exception e) {
            e.printStackTrace();
        }
        return null;
    }

    private boolean areTwoSPIDPresent(Map<String, String> map) {
        return map.containsKey(SPID1) && map.containsKey(SPID2);
    }
}
