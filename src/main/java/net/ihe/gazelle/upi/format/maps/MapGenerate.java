package net.ihe.gazelle.upi.format.maps;

import net.ihe.gazelle.upi.model.ConstantValues;
import net.ihe.gazelle.upi.model.error.WarnErrorKeyValue;
import net.ihe.gazelle.upi.exception.MultipleFoundException;
import net.ihe.gazelle.upi.exception.NotFoundException;
import net.ihe.gazelle.upi.model.ech_0213.Request;
import net.ihe.gazelle.upi.model.ech_0213_commons.PersonFromUPIType;
import org.apache.commons.lang3.StringUtils;

import java.util.Collections;
import java.util.HashMap;
import java.util.Map;


public class MapGenerate implements IMapToMustache {

    @Override
    public Map<String, String> mapReqToMustache(Request req) {


        Map<String, String> personToUpiMap = getMapPersonToUpi(req);
        Map<String, String> resultingMap = Collections.emptyMap();
        Map<String,String> pidsFromRequest = getMapOfPids(req);

        if (areMandatoryFieldsAbsent(personToUpiMap)) {
            return mapForNegativeReport(WarnErrorKeyValue.CODE_ERROR_XSD_VALUE, WarnErrorKeyValue.MSG_ERROR_XSD_VALUE);
        }

        try {
            Map<String, String> personFromUpiMap = getMapPersonFromUpi(req);

            switch (howManyMandatoryDataAreCorrect(personToUpiMap, req)) {
                case 0:
                case 1:
                    resultingMap = mapForNegativeReport(WarnErrorKeyValue.CODE_ERROR_NAV13_VALUE, WarnErrorKeyValue.MSG_ERROR_NAV13_VALUE);
                    break;
                case 2:
                    resultingMap = mapForPostiveReportWithWarning(req, personFromUpiMap);
                    resultingMap.putAll(pidsFromRequest);
                    resultingMap.put(ConstantValues.SPID, generateSPID(req));
                    break;
                case 3:
                    resultingMap = mapForPostiveReport(req, personFromUpiMap);
                    resultingMap.putAll(pidsFromRequest);
                    resultingMap.put(ConstantValues.SPID, generateSPID(req));
                    break;
                default:
                    break;
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return resultingMap;

    }

    /**
     * @param req
     * @return The generated EPR-SPID of the PersonToUPI
     * @throws Exception
     */
    private String generateSPID(Request req) {
        return "73575" + getMapOfPids(req).get(ConstantValues.VN);
    }

    /**
     * @param personToUpiMap
     * @return
     */
    private boolean areMandatoryFieldsAbsent(Map<String, String> personToUpiMap) {
        return (StringUtils.isBlank(personToUpiMap.get(ConstantValues.FIRST_NAME)) ||
                StringUtils.isBlank(personToUpiMap.get(ConstantValues.OFFICIAL_NAME)) ||
                StringUtils.isBlank(personToUpiMap.get(ConstantValues.BIRTHDAY)));
    }

    private int howManyMandatoryDataAreCorrect(Map<String, String> personMap, Request req) throws NotFoundException, MultipleFoundException {
        PersonFromUPIType personFrom = getpersonFromUpi(req);
        int i = 0;

        if (personFrom.getFirstName().equals(personMap.get(ConstantValues.FIRST_NAME))) {
            i++;
        }
        if (personFrom.getOfficialName().equals(personMap.get(ConstantValues.OFFICIAL_NAME))) {
            i++;
        }
        if (personFrom.getDateOfBirth().getYearMonthDay().toString().equals(personMap.get(ConstantValues.BIRTHDAY))) {
            i++;
        }
        return i;
    }


    private Map<String, String> mapForPostiveReportWithWarning(Request req, Map<String, String> personFromUpiMap) throws MultipleFoundException, NotFoundException {
        Map<String, String> mapForPostiveReportWithWarning = new HashMap<>();

        mapForPostiveReportWithWarning.put(WarnErrorKeyValue.WARNING, WarnErrorKeyValue.WARNING);
        mapForPostiveReportWithWarning.put(WarnErrorKeyValue.CODE_ERROR_KEY, WarnErrorKeyValue.CODE_WARN_DESCREPANCY_VALUE);
        mapForPostiveReportWithWarning.put(WarnErrorKeyValue.MSG_ERROR_KEY, WarnErrorKeyValue.MSG_WARN_DESCREPANCY_VALUE);

        mapForPostiveReportWithWarning.putAll(personFromUpiMap);
        mapForPostiveReportWithWarning.putAll(standardDataFromRequest(req));
        return mapForPostiveReportWithWarning;

    }

}
