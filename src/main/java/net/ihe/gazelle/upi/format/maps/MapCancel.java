package net.ihe.gazelle.upi.format.maps;

import net.ihe.gazelle.upi.model.error.WarnErrorKeyValue;
import net.ihe.gazelle.upi.model.ech_0213.Request;
import java.util.Map;

public class MapCancel implements IMapToMustache{

    @Override
    public Map<String, String> mapReqToMustache(Request req) {

        Map<String, String> mapOfPidFromReq = getMapOfPids(req);

        if (!isOneSPIDPresent(mapOfPidFromReq)) {
           return mapForNegativeReport(WarnErrorKeyValue.CODE_ERROR_SPID_MANDATORY, WarnErrorKeyValue.MSG_ERROR_SPID_MANDATORY);
        }

        try {
            Map<String, String> personFromUpiMap = getMapPersonFromUpi(req);
            Map<String, String> pidsFromUPI = getMapOfPidsFromUPIFromSpid(mapOfPidFromReq);
            Map<String,String> pidsFromUPIMinusTheOneToCancel = removeSPIDToCancel(mapOfPidFromReq,pidsFromUPI);
            personFromUpiMap.putAll(pidsFromUPIMinusTheOneToCancel);
            return mapForPostiveReport(req, personFromUpiMap);
        } catch (Exception e) {
            e.printStackTrace();
        }
        return null;
    }

    private boolean isOneSPIDPresent(Map<String, String> map) {
        return map.containsKey("spid1");
    }

    private Map<String,String> removeSPIDToCancel(Map<String,String> mapPidsFromReq,Map<String,String> mapPidsFromUPI){
        String spidToCancel = mapPidsFromReq.get("spid1");

        if (mapPidsFromUPI.containsValue(spidToCancel)){
            mapPidsFromUPI.values().remove(spidToCancel);
        }
        return mapPidsFromUPI;
    }
}
