package net.ihe.gazelle.upi.format.maps;

import net.ihe.gazelle.upi.model.ConstantValues;
import net.ihe.gazelle.upi.model.error.WarnErrorKeyValue;
import net.ihe.gazelle.upi.adapter.Patient;
import net.ihe.gazelle.upi.adapter.PatientDAO;
import net.ihe.gazelle.upi.exception.MultipleFoundException;
import net.ihe.gazelle.upi.exception.NotFoundException;
import net.ihe.gazelle.upi.model.factory.GeneralObjectFactory;
import net.ihe.gazelle.upi.model.ech_0011.NationalityDataType;
import net.ihe.gazelle.upi.model.ech_0213.Request;
import net.ihe.gazelle.upi.model.ech_0213_commons.PersonFromUPIType;
import net.ihe.gazelle.upi.model.ech_0213_commons.PersonToUPIType;
import net.ihe.gazelle.upi.model.ech_0213_commons.PidsToUPIType;

import javax.xml.bind.JAXBElement;
import java.time.ZoneId;
import java.time.ZonedDateTime;
import java.time.format.DateTimeFormatter;
import java.time.temporal.ChronoUnit;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public interface IMapToMustache {
    Map<String, String> mapReqToMustache(Request req) throws NotFoundException;

    GeneralObjectFactory generalObjectFactory = new GeneralObjectFactory();

    default String dateNow() {
        return ZonedDateTime.now(ZoneId.of("Europe/Zurich")).truncatedTo(ChronoUnit.SECONDS).format(DateTimeFormatter.ISO_LOCAL_DATE_TIME) + "Z";
    }

    /*Data from Request*/

    default String getActionOnSPID(Request req) {
        return req.getContent().getActionOnSPID();
    }

    default String getSPIDCategory(Request req) {
        return req.getContent().getSPIDCategory();
    }

    default String getResponseLanguage(Request req) {
        return req.getContent().getResponseLanguage();
    }

    default PersonToUPIType getPersonToUpi(Request req) {
        PersonToUPIType person = req.getContent().getPersonToUPI();
        if (person == null) {
            person = generalObjectFactory.createECH0213CommonsObject().createPersonToUPIType();
        }
        return person;
    }

    default PersonFromUPIType getpersonFromUpi(Request req) throws NotFoundException {
        PatientDAO p = new Patient();
        return p.getPersonFromUPIFromeCH0213Request(req);
    }

    default Map<String, String> getMapPersonToUpi(Request req) {

        PersonToUPIType person = getPersonToUpi(req);
        Map<String, String> mapPerson = new HashMap<>();

        mapPerson.put(ConstantValues.FIRST_NAME, getFirstName(person));
        mapPerson.put(ConstantValues.OFFICIAL_NAME, getOfficialName(person));
        mapPerson.put(ConstantValues.BIRTHDAY, getBirthDay(person));

        mapPerson.put(ConstantValues.SEX, getSex(person));
        mapPerson.put(ConstantValues.PLACE_OF_BIRTH, getUnknownPlaceOfBirth(person));
        mapPerson.putAll(getMapNationalityDataToUpi(req));

        return mapPerson;
    }

    default Map<String, String> getMapPersonFromUpi(Request req) throws NotFoundException, MultipleFoundException {

        PersonFromUPIType personFromUpi = getpersonFromUpi(req);
        Map<String, String> mapPerson = new HashMap<>();
        mapPerson.put(ConstantValues.FIRST_NAME, getFirstName(personFromUpi));
        mapPerson.put(ConstantValues.OFFICIAL_NAME, getOfficialName(personFromUpi));
        mapPerson.put(ConstantValues.BIRTHDAY, getBirthDay(personFromUpi));
        mapPerson.put(ConstantValues.SEX, getSex(personFromUpi));
        mapPerson.put(ConstantValues.RECORD_TIMESTAMP, getRecordTimeStamp(personFromUpi));
        mapPerson.putAll(getMapNationalityDataToUpi(req));

        return mapPerson;
    }

    /*Method from PersonToUPIType*/
    default String getFirstName(PersonToUPIType person) {
        return person.getFirstName();
    }

    default String getOfficialName(PersonToUPIType person) {
        return person.getOfficialName();
    }

    default String getSex(PersonToUPIType person) {
        return person.getSex();
    }

    default String getBirthDay(PersonToUPIType person) {
        if (person.getDateOfBirth() != null) {
            if (person.getDateOfBirth().getYearMonthDay() != null) {
                return person.getDateOfBirth().getYearMonthDay().toString();
            }
            return null;
        }
        return null;
    }

    default String getUnknownPlaceOfBirth(PersonToUPIType person) {
        return person.getPlaceOfBirth().getUnknown();
    }

    default NationalityDataType getNationalityDataType(PersonToUPIType person) {
        NationalityDataType nationalityDataType = person.getNationalityData();
        if (nationalityDataType == null) {
            nationalityDataType = generalObjectFactory.createECH0011Object().createNationalityDataType();
        }
        return nationalityDataType;
    }

    default Map<String, String> getMapNationalityDataToUpi(Request req) {

        PersonToUPIType person = getPersonToUpi(req);
        NationalityDataType nationalityDataType = getNationalityDataType(person);

        Map<String, String> mapNationalityDataType = new HashMap<>();

        mapNationalityDataType.put(ConstantValues.NATIONALITY_STATUS, nationalityDataType.getNationalityStatus());

        for (NationalityDataType.CountryInfo ci : nationalityDataType.getCountryInfo()) {
            mapNationalityDataType.put(ConstantValues.COUNTRY_ID, ci.getCountry().getCountryId().toString());
            mapNationalityDataType.put(ConstantValues.COUNTRY_ID_ISO2, ci.getCountry().getCountryIdISO2());
            mapNationalityDataType.put(ConstantValues.COUNTRY_NAME_SHORT, ci.getCountry().getCountryNameShort());
        }

        return mapNationalityDataType;

    }

    default Map<String, String> getMapOfPids(Request req) {

        Map<String, String> mapOfPids = new HashMap<>();
        List<PidsToUPIType> pids = req.getContent().getPidsToUPI();
        int j = 1;
        for (PidsToUPIType p : pids) {

            for (JAXBElement jbe : p.getContent()) {

                if ((jbe.getName()) != null && jbe.getValue() != null) {

                    if (jbe.getName().toString().contains(ConstantValues.VN)) {
                        mapOfPids.put(ConstantValues.VN, jbe.getValue().toString());
                    }

                    if (jbe.getName().toString().contains(ConstantValues.SPID.toUpperCase()) && !"".equals(jbe.getValue())) {
                        mapOfPids.put(ConstantValues.SPID + j, jbe.getValue().toString());
                        j++;
                    }
                }
            }
        }
        return mapOfPids;
    }

    default Map<String, String> getMapOfPidsFromUPIFromSpid(Map<String,String> mapOfPids) throws NotFoundException, MultipleFoundException {
        PatientDAO p = new Patient();
        String spidFromReq = mapOfPids.get(ConstantValues.SPID + 1);
        String vn = p.getVnFromSpid(spidFromReq);

        List<String> listSpids =p.getSpidsFromVn(vn);
        Map<String, String> mapOfPidsFromUPI = new HashMap<>();

        int i=1;
        for (String spid : listSpids){
            mapOfPidsFromUPI.put(ConstantValues.SPID+i,spid);
            i++;
        }
        mapOfPidsFromUPI.put(ConstantValues.VN,vn);
        return  mapOfPidsFromUPI;



    }


    /*Method from PersonFromUPIType*/

    default String getFirstName(PersonFromUPIType person) {
        return person.getFirstName();
    }

    default String getOfficialName(PersonFromUPIType person) {
        return person.getOfficialName();
    }

    default String getSex(PersonFromUPIType person) {
        return person.getSex();
    }

    default String getBirthDay(PersonFromUPIType person) {
        if (person.getDateOfBirth() != null) {
            if (person.getDateOfBirth().getYearMonthDay() != null) {
                return person.getDateOfBirth().getYearMonthDay().toString();
            }
            return null;
        }
        return null;
    }

    default String getRecordTimeStamp(PersonFromUPIType person) {
        return person.getRecordTimestamp().toString();
    }

    default Map<String, String> mapForPostiveReport(Request req, Map<String, String> personFromUpiMap) throws MultipleFoundException, NotFoundException {
        Map<String, String> mapForPostiveReport = new HashMap<>();
        mapForPostiveReport.putAll(personFromUpiMap);
        mapForPostiveReport.putAll(standardDataFromRequest(req));
        return mapForPostiveReport;

    }

    default Map<String, String> mapForNegativeReport(String codeValue, String msgValue) {
        Map<String, String> mapForNegativeReport = new HashMap<>();
        mapForNegativeReport.put(WarnErrorKeyValue.NEGATIVE_REPORT, WarnErrorKeyValue.NEGATIVE_REPORT);
        mapForNegativeReport.put(WarnErrorKeyValue.MESSAGE_DATE, dateNow());
        mapForNegativeReport.put(WarnErrorKeyValue.CODE_ERROR_KEY, codeValue);
        mapForNegativeReport.put(WarnErrorKeyValue.MSG_ERROR_KEY, msgValue);
        return mapForNegativeReport;
    }

    default Map<String, String> standardDataFromRequest(Request req) {
        Map<String, String> standardData = new HashMap<>();
        standardData.put(ConstantValues.ACTION_ON_SPID, getActionOnSPID(req));
        standardData.put(ConstantValues.SPID_CATEGORY, getSPIDCategory(req));
        standardData.put(ConstantValues.LANGUAGE, getResponseLanguage(req));

        return standardData;
    }


}
