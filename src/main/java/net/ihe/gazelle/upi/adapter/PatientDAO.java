package net.ihe.gazelle.upi.adapter;

import net.ihe.gazelle.upi.exception.InvalidParameterException;
import net.ihe.gazelle.upi.exception.MultipleFoundException;
import net.ihe.gazelle.upi.exception.NotFoundException;
import net.ihe.gazelle.upi.model.ech_0213_commons.PersonFromUPIType;
import net.ihe.gazelle.upi.model.ech_0214.Request;
import net.ihe.gazelle.upi.model.ech_0214.Response;

import java.util.List;

public interface PatientDAO {


    Response.PositiveResponse.GetInfoPersonResponse getInfoPersonFromPersonRequest(Request.Content.GetInfoPersonRequest getInfoPersonRequest) throws NotFoundException, InvalidParameterException, MultipleFoundException;

    Response.PositiveResponse.SearchPersonResponse getSearchPersonResponseFromSearchPersonRequest(Request.Content.SearchPersonRequest searchPersonRequest) throws NotFoundException, InvalidParameterException;

    Response.PositiveResponse.CompareDataResponse getCompareDataResponseFromDataRequest(Request.Content.CompareDataRequest compareDataRequest) throws NotFoundException, MultipleFoundException;

    PersonFromUPIType getPersonFromUpiTypeFromSpid(String spid) throws NotFoundException, MultipleFoundException;

    PersonFromUPIType getPersonFromUPIFromeCH0213Request(net.ihe.gazelle.upi.model.ech_0213.Request req);

    String getVnFromSpid(String spid) throws NotFoundException, MultipleFoundException;

    List<String> getSpidsFromVn(String vn);
}
