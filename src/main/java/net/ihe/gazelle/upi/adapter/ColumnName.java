package net.ihe.gazelle.upi.adapter;

public class ColumnName {
    public final static String COLUMN_SSN = "SSN";
    protected final static String COLUMN_SSN_STATUS = "SSN status";
    protected final static String COLUMN_EPR_SPID_CANCELED = "canceled EPR-SPID";
    protected final static String COLUMN_EPR_SPID_ACTIVE = "active EPR-SPID";
    protected final static String COLUMN_ONE_EPR_SPID_ACTIVE = "active EPR-SPID 1";
    protected final static String COLUMN_TWO_EPR_SPID_ACTIVE = "active EPR-SPID 2";
    protected final static String COLUMN_EPR_SPID_INACTIVE = "inactive EPR-SPID";
    protected final static String COLUMN_ONE_EPR_SPID_INACTIVE = "inactive EPR-SPID 1";
    protected final static String COLUMN_TWO_EPR_SPID_INACTIVE = "inactive EPR-SPID 2";
    protected final static String COLUMN_EPR_SPID_INDEX = "EPR-SPID";
    protected final static String COLUMN_EPR_SPID_INDEX_ONE = "EPR-SPID1";
    protected final static String COLUMN_EPR_SPID_INDEX_TWO = "EPR-SPID2";
    protected final static String COLUMN_EPR_SPID_INDEX_THREE = "EPR-SPID3";
    protected final static String COLUMN_EPR_SPID_INDEX_FOUR = "EPR-SPID4";
    protected final static String COLUMN_EPR_SPID_INDEX_FIVE = "EPR-SPID5";
    protected final static String COLUMN_EPR_SPID_INDEX_SIX = "EPR-SPID6";
    protected final static String COLUMN_FIRSTNAME = "Firstname";
    protected final static String COLUMN_OFFICIAL_NAME = "OfficialName";
    protected final static String COLUMN_SEX = "Sex";
    protected final static String COLUMN_DATE_OF_BIRTH = "DateOfBirth";
    protected final static String COLUMN_MOTHER_FIRSTNAME = "MothersFirstname";
    protected final static String COLUMN_MOTHER_LASTNAME = "MothersLastname";
    protected final static String COLUMN_FATHER_FIRSTNAME = "FathersFirstname";
    protected final static String COLUMN_FATHER_LASTNAME = "FathersLastname";
    protected final static String COLUMN_NATIONALITY = "Nationality";
    protected final static String COLUMN_COUNTRY = "CountryOfBirth";

    public ColumnName() {
    }

}
