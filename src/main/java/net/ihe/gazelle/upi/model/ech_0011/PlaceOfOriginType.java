
package net.ihe.gazelle.upi.model.ech_0011;

import java.math.BigInteger;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlSchemaType;
import javax.xml.bind.annotation.XmlType;
import net.ihe.gazelle.upi.model.ech_0007.CantonAbbreviationType;


/**
 * <p>Classe Java pour placeOfOriginType complex type.
 * 
 * <p>Le fragment de schéma suivant indique le contenu attendu figurant dans cette classe.
 * 
 * <pre>
 * &lt;complexType name="placeOfOriginType"&gt;
 *   &lt;complexContent&gt;
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *       &lt;sequence&gt;
 *         &lt;element name="originName"&gt;
 *           &lt;simpleType&gt;
 *             &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string"&gt;
 *               &lt;maxLength value="50"/&gt;
 *             &lt;/restriction&gt;
 *           &lt;/simpleType&gt;
 *         &lt;/element&gt;
 *         &lt;element name="canton" type="{http://www.ech.ch/xmlns/eCH-0007/5}cantonAbbreviationType"/&gt;
 *         &lt;element name="placeOfOriginId" type="{http://www.ech.ch/xmlns/eCH-0135/1}placeOfOriginIdType" minOccurs="0"/&gt;
 *         &lt;element name="historyMunicipalityId" type="{http://www.ech.ch/xmlns/eCH-0007/5}historyMunicipalityId" minOccurs="0"/&gt;
 *       &lt;/sequence&gt;
 *     &lt;/restriction&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "placeOfOriginType", propOrder = {
    "originName",
    "canton",
    "placeOfOriginId",
    "historyMunicipalityId"
})
public class PlaceOfOriginType {

    @XmlElement(required = true)
    protected String originName;
    @XmlElement(required = true)
    @XmlSchemaType(name = "token")
    protected CantonAbbreviationType canton;
    @XmlSchemaType(name = "nonNegativeInteger")
    protected BigInteger placeOfOriginId;
    protected Integer historyMunicipalityId;

    /**
     * Obtient la valeur de la propriété originName.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getOriginName() {
        return originName;
    }

    /**
     * Définit la valeur de la propriété originName.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setOriginName(String value) {
        this.originName = value;
    }

    /**
     * Obtient la valeur de la propriété canton.
     * 
     * @return
     *     possible object is
     *     {@link CantonAbbreviationType }
     *     
     */
    public CantonAbbreviationType getCanton() {
        return canton;
    }

    /**
     * Définit la valeur de la propriété canton.
     * 
     * @param value
     *     allowed object is
     *     {@link CantonAbbreviationType }
     *     
     */
    public void setCanton(CantonAbbreviationType value) {
        this.canton = value;
    }

    /**
     * Obtient la valeur de la propriété placeOfOriginId.
     * 
     * @return
     *     possible object is
     *     {@link BigInteger }
     *     
     */
    public BigInteger getPlaceOfOriginId() {
        return placeOfOriginId;
    }

    /**
     * Définit la valeur de la propriété placeOfOriginId.
     * 
     * @param value
     *     allowed object is
     *     {@link BigInteger }
     *     
     */
    public void setPlaceOfOriginId(BigInteger value) {
        this.placeOfOriginId = value;
    }

    /**
     * Obtient la valeur de la propriété historyMunicipalityId.
     * 
     * @return
     *     possible object is
     *     {@link Integer }
     *     
     */
    public Integer getHistoryMunicipalityId() {
        return historyMunicipalityId;
    }

    /**
     * Définit la valeur de la propriété historyMunicipalityId.
     * 
     * @param value
     *     allowed object is
     *     {@link Integer }
     *     
     */
    public void setHistoryMunicipalityId(Integer value) {
        this.historyMunicipalityId = value;
    }

}
