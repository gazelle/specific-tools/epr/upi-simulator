package net.ihe.gazelle.upi.model;

public class ConstantValues {

    /**
     * private constructor to prevent instanciation
     */
    private ConstantValues() {}

    /**
     * String for Request to HashMap
     */

    public static final String RECORD_TIMESTAMP="recordTimestamp";
    public static final String SPID_CATEGORY="spidCategory";
    public static final String ACTION_ON_SPID="actionOnSPID";
    public static final String LANGUAGE="language";
    public static final String VN="vn";
    public static final String SPID="spid";
    public static final String FIRST_NAME="firstName";
    public static final String OFFICIAL_NAME="officialName";
    public static final String SEX="sex";
    public static final String BIRTHDAY="birthday";
    public static final String PLACE_OF_BIRTH="unknown";
    public static final String NATIONALITY_STATUS="nationalityStatus";
    public static final String COUNTRY_ID="countryId";
    public static final String COUNTRY_ID_ISO2="countryIdISO2";
    public static final String COUNTRY_NAME_SHORT="countryNameShort";
    public static final String NATIONALITY_VALIDITY_FROM="nationalityValidFrom";
    public static final String MOTHERS_NAME="motherName";
    public static final String FATHERS_NAME="motherName";
    public static final String SPID_TO_INACTIVATE="73575701N4C71V4730";








}
