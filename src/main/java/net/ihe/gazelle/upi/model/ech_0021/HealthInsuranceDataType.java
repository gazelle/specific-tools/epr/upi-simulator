
package net.ihe.gazelle.upi.model.ech_0021;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlSchemaType;
import javax.xml.bind.annotation.XmlType;
import javax.xml.bind.annotation.adapters.CollapsedStringAdapter;
import javax.xml.bind.annotation.adapters.XmlJavaTypeAdapter;
import javax.xml.datatype.XMLGregorianCalendar;
import net.ihe.gazelle.upi.model.ech_0010.OrganisationMailAddressType;


/**
 * <p>Classe Java pour healthInsuranceDataType complex type.
 * 
 * <p>Le fragment de schéma suivant indique le contenu attendu figurant dans cette classe.
 * 
 * <pre>
 * &lt;complexType name="healthInsuranceDataType"&gt;
 *   &lt;complexContent&gt;
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *       &lt;sequence&gt;
 *         &lt;element name="healthInsured" type="{http://www.ech.ch/xmlns/eCH-0011/8}yesNoType"/&gt;
 *         &lt;element name="insurance" minOccurs="0"&gt;
 *           &lt;complexType&gt;
 *             &lt;complexContent&gt;
 *               &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *                 &lt;choice&gt;
 *                   &lt;element name="insuranceName" type="{http://www.ech.ch/xmlns/eCH-0021/7}baseNameType"/&gt;
 *                   &lt;element name="insuranceAddress" type="{http://www.ech.ch/xmlns/eCH-0010/5}organisationMailAddressType"/&gt;
 *                 &lt;/choice&gt;
 *               &lt;/restriction&gt;
 *             &lt;/complexContent&gt;
 *           &lt;/complexType&gt;
 *         &lt;/element&gt;
 *         &lt;element name="healthInsuranceValidFrom" type="{http://www.w3.org/2001/XMLSchema}date" minOccurs="0"/&gt;
 *       &lt;/sequence&gt;
 *     &lt;/restriction&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "healthInsuranceDataType", propOrder = {
    "healthInsured",
    "insurance",
    "healthInsuranceValidFrom"
})
public class HealthInsuranceDataType {

    @XmlElement(required = true)
    protected String healthInsured;
    protected HealthInsuranceDataType.Insurance insurance;
    @XmlSchemaType(name = "date")
    protected XMLGregorianCalendar healthInsuranceValidFrom;

    /**
     * Obtient la valeur de la propriété healthInsured.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getHealthInsured() {
        return healthInsured;
    }

    /**
     * Définit la valeur de la propriété healthInsured.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setHealthInsured(String value) {
        this.healthInsured = value;
    }

    /**
     * Obtient la valeur de la propriété insurance.
     * 
     * @return
     *     possible object is
     *     {@link HealthInsuranceDataType.Insurance }
     *     
     */
    public HealthInsuranceDataType.Insurance getInsurance() {
        return insurance;
    }

    /**
     * Définit la valeur de la propriété insurance.
     * 
     * @param value
     *     allowed object is
     *     {@link HealthInsuranceDataType.Insurance }
     *     
     */
    public void setInsurance(HealthInsuranceDataType.Insurance value) {
        this.insurance = value;
    }

    /**
     * Obtient la valeur de la propriété healthInsuranceValidFrom.
     * 
     * @return
     *     possible object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public XMLGregorianCalendar getHealthInsuranceValidFrom() {
        return healthInsuranceValidFrom;
    }

    /**
     * Définit la valeur de la propriété healthInsuranceValidFrom.
     * 
     * @param value
     *     allowed object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public void setHealthInsuranceValidFrom(XMLGregorianCalendar value) {
        this.healthInsuranceValidFrom = value;
    }


    /**
     * <p>Classe Java pour anonymous complex type.
     * 
     * <p>Le fragment de schéma suivant indique le contenu attendu figurant dans cette classe.
     * 
     * <pre>
     * &lt;complexType&gt;
     *   &lt;complexContent&gt;
     *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
     *       &lt;choice&gt;
     *         &lt;element name="insuranceName" type="{http://www.ech.ch/xmlns/eCH-0021/7}baseNameType"/&gt;
     *         &lt;element name="insuranceAddress" type="{http://www.ech.ch/xmlns/eCH-0010/5}organisationMailAddressType"/&gt;
     *       &lt;/choice&gt;
     *     &lt;/restriction&gt;
     *   &lt;/complexContent&gt;
     * &lt;/complexType&gt;
     * </pre>
     * 
     * 
     */
    @XmlAccessorType(XmlAccessType.FIELD)
    @XmlType(name = "", propOrder = {
        "insuranceName",
        "insuranceAddress"
    })
    public static class Insurance {

        @XmlJavaTypeAdapter(CollapsedStringAdapter.class)
        @XmlSchemaType(name = "token")
        protected String insuranceName;
        protected OrganisationMailAddressType insuranceAddress;

        /**
         * Obtient la valeur de la propriété insuranceName.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getInsuranceName() {
            return insuranceName;
        }

        /**
         * Définit la valeur de la propriété insuranceName.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setInsuranceName(String value) {
            this.insuranceName = value;
        }

        /**
         * Obtient la valeur de la propriété insuranceAddress.
         * 
         * @return
         *     possible object is
         *     {@link OrganisationMailAddressType }
         *     
         */
        public OrganisationMailAddressType getInsuranceAddress() {
            return insuranceAddress;
        }

        /**
         * Définit la valeur de la propriété insuranceAddress.
         * 
         * @param value
         *     allowed object is
         *     {@link OrganisationMailAddressType }
         *     
         */
        public void setInsuranceAddress(OrganisationMailAddressType value) {
            this.insuranceAddress = value;
        }

    }

}
