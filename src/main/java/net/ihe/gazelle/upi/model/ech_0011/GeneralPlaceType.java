
package net.ihe.gazelle.upi.model.ech_0011;


import net.ihe.gazelle.upi.model.ech_0007.SwissMunicipalityType;
import net.ihe.gazelle.upi.model.ech_0008.CountryType;
import javax.xml.bind.annotation.*;
import javax.xml.bind.annotation.adapters.CollapsedStringAdapter;
import javax.xml.bind.annotation.adapters.XmlJavaTypeAdapter;


/**
 * <p>Classe Java pour generalPlaceType complex type.
 * 
 * <p>Le fragment de schéma suivant indique le contenu attendu figurant dans cette classe.
 * 
 * <pre>
 * &lt;complexType name="generalPlaceType"&gt;
 *   &lt;complexContent&gt;
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *       &lt;choice&gt;
 *         &lt;element name="unknown" type="{http://www.ech.ch/xmlns/eCH-0011/8}unknownType"/&gt;
 *         &lt;element name="swissTown" type="{http://www.ech.ch/xmlns/eCH-0007/5}swissMunicipalityType"/&gt;
 *         &lt;element name="foreignCountry"&gt;
 *           &lt;complexType&gt;
 *             &lt;complexContent&gt;
 *               &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *                 &lt;sequence&gt;
 *                   &lt;element name="country" type="{http://www.ech.ch/xmlns/eCH-0008/3}countryType"/&gt;
 *                   &lt;element name="town" minOccurs="0"&gt;
 *                     &lt;simpleType&gt;
 *                       &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string"&gt;
 *                         &lt;whiteSpace value="collapse"/&gt;
 *                         &lt;maxLength value="100"/&gt;
 *                       &lt;/restriction&gt;
 *                     &lt;/simpleType&gt;
 *                   &lt;/element&gt;
 *                 &lt;/sequence&gt;
 *               &lt;/restriction&gt;
 *             &lt;/complexContent&gt;
 *           &lt;/complexType&gt;
 *         &lt;/element&gt;
 *       &lt;/choice&gt;
 *     &lt;/restriction&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "generalPlaceType", propOrder = {
    "unknown",
    "swissTown",
    "foreignCountry"
})
@XmlSeeAlso({
    DestinationType.class
})
public class GeneralPlaceType {

    @XmlJavaTypeAdapter(CollapsedStringAdapter.class)
    @XmlSchemaType(name = "token")
    protected String unknown;
    protected SwissMunicipalityType swissTown;
    protected GeneralPlaceType.ForeignCountry foreignCountry;

    /**
     * Obtient la valeur de la propriété unknown.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getUnknown() {
        return unknown;
    }

    /**
     * Définit la valeur de la propriété unknown.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setUnknown(String value) {
        this.unknown = value;
    }

    /**
     * Obtient la valeur de la propriété swissTown.
     * 
     * @return
     *     possible object is
     *     {@link SwissMunicipalityType }
     *     
     */
    public SwissMunicipalityType getSwissTown() {
        return swissTown;
    }

    /**
     * Définit la valeur de la propriété swissTown.
     * 
     * @param value
     *     allowed object is
     *     {@link SwissMunicipalityType }
     *     
     */
    public void setSwissTown(SwissMunicipalityType value) {
        this.swissTown = value;
    }

    /**
     * Obtient la valeur de la propriété foreignCountry.
     * 
     * @return
     *     possible object is
     *     {@link GeneralPlaceType.ForeignCountry }
     *     
     */
    public GeneralPlaceType.ForeignCountry getForeignCountry() {
        return foreignCountry;
    }

    /**
     * Définit la valeur de la propriété foreignCountry.
     * 
     * @param value
     *     allowed object is
     *     {@link GeneralPlaceType.ForeignCountry }
     *     
     */
    public void setForeignCountry(GeneralPlaceType.ForeignCountry value) {
        this.foreignCountry = value;
    }


    /**
     * <p>Classe Java pour anonymous complex type.
     * 
     * <p>Le fragment de schéma suivant indique le contenu attendu figurant dans cette classe.
     * 
     * <pre>
     * &lt;complexType&gt;
     *   &lt;complexContent&gt;
     *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
     *       &lt;sequence&gt;
     *         &lt;element name="country" type="{http://www.ech.ch/xmlns/eCH-0008/3}countryType"/&gt;
     *         &lt;element name="town" minOccurs="0"&gt;
     *           &lt;simpleType&gt;
     *             &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string"&gt;
     *               &lt;whiteSpace value="collapse"/&gt;
     *               &lt;maxLength value="100"/&gt;
     *             &lt;/restriction&gt;
     *           &lt;/simpleType&gt;
     *         &lt;/element&gt;
     *       &lt;/sequence&gt;
     *     &lt;/restriction&gt;
     *   &lt;/complexContent&gt;
     * &lt;/complexType&gt;
     * </pre>
     * 
     * 
     */
    @XmlAccessorType(XmlAccessType.FIELD)
    @XmlType(name = "", propOrder = {
        "country",
        "town"
    })
    public static class ForeignCountry {

        @XmlElement(required = true)
        protected CountryType country;
        protected String town;

        /**
         * Obtient la valeur de la propriété country.
         * 
         * @return
         *     possible object is
         *     {@link CountryType }
         *     
         */
        public CountryType getCountry() {
            return country;
        }

        /**
         * Définit la valeur de la propriété country.
         * 
         * @param value
         *     allowed object is
         *     {@link CountryType }
         *     
         */
        public void setCountry(CountryType value) {
            this.country = value;
        }

        /**
         * Obtient la valeur de la propriété town.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getTown() {
            return town;
        }

        /**
         * Définit la valeur de la propriété town.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setTown(String value) {
            this.town = value;
        }

    }

}
