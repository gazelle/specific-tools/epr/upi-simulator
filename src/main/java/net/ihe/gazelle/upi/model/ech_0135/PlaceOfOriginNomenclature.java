
package net.ihe.gazelle.upi.model.ech_0135;

import javax.xml.bind.annotation.*;
import java.util.ArrayList;
import java.util.List;


/**
 * <p>Classe Java pour anonymous complex type.
 * 
 * <p>Le fragment de schéma suivant indique le contenu attendu figurant dans cette classe.
 * 
 * <pre>
 * &lt;complexType&gt;
 *   &lt;complexContent&gt;
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *       &lt;sequence&gt;
 *         &lt;element name="placeOfOrigins"&gt;
 *           &lt;complexType&gt;
 *             &lt;complexContent&gt;
 *               &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *                 &lt;sequence&gt;
 *                   &lt;element name="placeOfOrigin" type="{http://www.ech.ch/xmlns/eCH-0135/1}placeOfOriginType" maxOccurs="unbounded"/&gt;
 *                 &lt;/sequence&gt;
 *               &lt;/restriction&gt;
 *             &lt;/complexContent&gt;
 *           &lt;/complexType&gt;
 *         &lt;/element&gt;
 *       &lt;/sequence&gt;
 *     &lt;/restriction&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = {
    "placeOfOrigins"
})
@XmlRootElement(name = "placeOfOriginNomenclature")
public class PlaceOfOriginNomenclature {

    @XmlElement(required = true)
    protected PlaceOfOriginNomenclature.PlaceOfOrigins placeOfOrigins;

    /**
     * Obtient la valeur de la propriété placeOfOrigins.
     * 
     * @return
     *     possible object is
     *     {@link PlaceOfOriginNomenclature.PlaceOfOrigins }
     *     
     */
    public PlaceOfOriginNomenclature.PlaceOfOrigins getPlaceOfOrigins() {
        return placeOfOrigins;
    }

    /**
     * Définit la valeur de la propriété placeOfOrigins.
     * 
     * @param value
     *     allowed object is
     *     {@link PlaceOfOriginNomenclature.PlaceOfOrigins }
     *     
     */
    public void setPlaceOfOrigins(PlaceOfOriginNomenclature.PlaceOfOrigins value) {
        this.placeOfOrigins = value;
    }


    /**
     * <p>Classe Java pour anonymous complex type.
     * 
     * <p>Le fragment de schéma suivant indique le contenu attendu figurant dans cette classe.
     * 
     * <pre>
     * &lt;complexType&gt;
     *   &lt;complexContent&gt;
     *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
     *       &lt;sequence&gt;
     *         &lt;element name="placeOfOrigin" type="{http://www.ech.ch/xmlns/eCH-0135/1}placeOfOriginType" maxOccurs="unbounded"/&gt;
     *       &lt;/sequence&gt;
     *     &lt;/restriction&gt;
     *   &lt;/complexContent&gt;
     * &lt;/complexType&gt;
     * </pre>
     * 
     * 
     */
    @XmlAccessorType(XmlAccessType.FIELD)
    @XmlType(name = "", propOrder = {
        "placeOfOrigin"
    })
    public static class PlaceOfOrigins {

        @XmlElement(required = true)
        protected List<PlaceOfOriginType> placeOfOrigin;

        /**
         * Gets the value of the placeOfOrigin property.
         * 
         * <p>
         * This accessor method returns a reference to the live list,
         * not a snapshot. Therefore any modification you make to the
         * returned list will be present inside the JAXB object.
         * This is why there is not a <CODE>set</CODE> method for the placeOfOrigin property.
         * 
         * <p>
         * For example, to add a new item, do as follows:
         * <pre>
         *    getPlaceOfOrigin().add(newItem);
         * </pre>
         * 
         * 
         * <p>
         * Objects of the following type(s) are allowed in the list
         * {@link PlaceOfOriginType }
         * 
         * 
         */
        public List<PlaceOfOriginType> getPlaceOfOrigin() {
            if (placeOfOrigin == null) {
                placeOfOrigin = new ArrayList<PlaceOfOriginType>();
            }
            return this.placeOfOrigin;
        }

    }

}
