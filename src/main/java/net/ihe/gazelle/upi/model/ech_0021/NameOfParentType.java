
package net.ihe.gazelle.upi.model.ech_0021;


import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlSchemaType;
import javax.xml.bind.annotation.XmlType;
import javax.xml.bind.annotation.adapters.CollapsedStringAdapter;
import javax.xml.bind.annotation.adapters.XmlJavaTypeAdapter;

/**
 * <p>Classe Java pour nameOfParentType complex type.
 * 
 * <p>Le fragment de schéma suivant indique le contenu attendu figurant dans cette classe.
 * 
 * <pre>
 * &lt;complexType name="nameOfParentType"&gt;
 *   &lt;complexContent&gt;
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *       &lt;sequence&gt;
 *         &lt;choice&gt;
 *           &lt;sequence&gt;
 *             &lt;element name="firstName" type="{http://www.ech.ch/xmlns/eCH-0044/4}officialFirstNameType"/&gt;
 *             &lt;element name="officialName" type="{http://www.ech.ch/xmlns/eCH-0044/4}baseNameType"/&gt;
 *           &lt;/sequence&gt;
 *           &lt;element name="firstNameOnly" type="{http://www.ech.ch/xmlns/eCH-0044/4}officialFirstNameType"/&gt;
 *           &lt;element name="officialNameOnly" type="{http://www.ech.ch/xmlns/eCH-0044/4}baseNameType"/&gt;
 *         &lt;/choice&gt;
 *         &lt;element name="officialProofOfNameOfParentsYesNo" type="{http://www.w3.org/2001/XMLSchema}boolean" minOccurs="0"/&gt;
 *       &lt;/sequence&gt;
 *     &lt;/restriction&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "nameOfParentType", propOrder = {
    "firstName",
    "officialName",
    "firstNameOnly",
    "officialNameOnly",
    "officialProofOfNameOfParentsYesNo"
})
public class NameOfParentType {

    protected String firstName;
    @XmlJavaTypeAdapter(CollapsedStringAdapter.class)
    @XmlSchemaType(name = "token")
    protected String officialName;
    protected String firstNameOnly;
    @XmlJavaTypeAdapter(CollapsedStringAdapter.class)
    @XmlSchemaType(name = "token")
    protected String officialNameOnly;
    protected Boolean officialProofOfNameOfParentsYesNo;

    /**
     * Obtient la valeur de la propriété firstName.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getFirstName() {
        return firstName;
    }

    /**
     * Définit la valeur de la propriété firstName.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setFirstName(String value) {
        this.firstName = value;
    }

    /**
     * Obtient la valeur de la propriété officialName.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getOfficialName() {
        return officialName;
    }

    /**
     * Définit la valeur de la propriété officialName.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setOfficialName(String value) {
        this.officialName = value;
    }

    /**
     * Obtient la valeur de la propriété firstNameOnly.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getFirstNameOnly() {
        return firstNameOnly;
    }

    /**
     * Définit la valeur de la propriété firstNameOnly.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setFirstNameOnly(String value) {
        this.firstNameOnly = value;
    }

    /**
     * Obtient la valeur de la propriété officialNameOnly.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getOfficialNameOnly() {
        return officialNameOnly;
    }

    /**
     * Définit la valeur de la propriété officialNameOnly.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setOfficialNameOnly(String value) {
        this.officialNameOnly = value;
    }

    /**
     * Obtient la valeur de la propriété officialProofOfNameOfParentsYesNo.
     * 
     * @return
     *     possible object is
     *     {@link Boolean }
     *     
     */
    public Boolean isOfficialProofOfNameOfParentsYesNo() {
        return officialProofOfNameOfParentsYesNo;
    }

    /**
     * Définit la valeur de la propriété officialProofOfNameOfParentsYesNo.
     * 
     * @param value
     *     allowed object is
     *     {@link Boolean }
     *     
     */
    public void setOfficialProofOfNameOfParentsYesNo(Boolean value) {
        this.officialProofOfNameOfParentsYesNo = value;
    }

}
