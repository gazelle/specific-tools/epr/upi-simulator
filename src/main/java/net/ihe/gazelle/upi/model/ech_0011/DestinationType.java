
package net.ihe.gazelle.upi.model.ech_0011;

import net.ihe.gazelle.upi.model.ech_0010.AddressInformationType;
import javax.xml.bind.annotation.*;


/**
 * <p>Classe Java pour destinationType complex type.
 * 
 * <p>Le fragment de schéma suivant indique le contenu attendu figurant dans cette classe.
 * 
 * <pre>
 * &lt;complexType name="destinationType"&gt;
 *   &lt;complexContent&gt;
 *     &lt;extension base="{http://www.ech.ch/xmlns/eCH-0011/8}generalPlaceType"&gt;
 *       &lt;sequence&gt;
 *         &lt;element name="mailAddress" type="{http://www.ech.ch/xmlns/eCH-0010/5}addressInformationType" minOccurs="0"/&gt;
 *       &lt;/sequence&gt;
 *     &lt;/extension&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "destinationType", propOrder = {
    "mailAddress"
})
public class DestinationType
    extends GeneralPlaceType
{

    protected AddressInformationType mailAddress;

    /**
     * Obtient la valeur de la propriété mailAddress.
     * 
     * @return
     *     possible object is
     *     {@link AddressInformationType }
     *     
     */
    public AddressInformationType getMailAddress() {
        return mailAddress;
    }

    /**
     * Définit la valeur de la propriété mailAddress.
     * 
     * @param value
     *     allowed object is
     *     {@link AddressInformationType }
     *     
     */
    public void setMailAddress(AddressInformationType value) {
        this.mailAddress = value;
    }

}
