
package net.ihe.gazelle.upi.model.ech_0021;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlType;
import net.ihe.gazelle.upi.model.ech_0011.GeneralPlaceType;


/**
 * <p>Classe Java pour maritalDataAddonType complex type.
 * 
 * <p>Le fragment de schéma suivant indique le contenu attendu figurant dans cette classe.
 * 
 * <pre>
 * &lt;complexType name="maritalDataAddonType"&gt;
 *   &lt;complexContent&gt;
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *       &lt;sequence&gt;
 *         &lt;element name="placeOfMarriage" type="{http://www.ech.ch/xmlns/eCH-0011/8}generalPlaceType" minOccurs="0"/&gt;
 *       &lt;/sequence&gt;
 *     &lt;/restriction&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "maritalDataAddonType", propOrder = {
    "placeOfMarriage"
})
public class MaritalDataAddonType {

    protected GeneralPlaceType placeOfMarriage;

    /**
     * Obtient la valeur de la propriété placeOfMarriage.
     * 
     * @return
     *     possible object is
     *     {@link GeneralPlaceType }
     *     
     */
    public GeneralPlaceType getPlaceOfMarriage() {
        return placeOfMarriage;
    }

    /**
     * Définit la valeur de la propriété placeOfMarriage.
     * 
     * @param value
     *     allowed object is
     *     {@link GeneralPlaceType }
     *     
     */
    public void setPlaceOfMarriage(GeneralPlaceType value) {
        this.placeOfMarriage = value;
    }

}
