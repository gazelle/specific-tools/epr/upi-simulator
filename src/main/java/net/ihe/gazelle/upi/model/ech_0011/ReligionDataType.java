
package net.ihe.gazelle.upi.model.ech_0011;

import javax.xml.bind.annotation.*;
import javax.xml.datatype.XMLGregorianCalendar;


/**
 * <p>Classe Java pour religionDataType complex type.
 * 
 * <p>Le fragment de schéma suivant indique le contenu attendu figurant dans cette classe.
 * 
 * <pre>
 * &lt;complexType name="religionDataType"&gt;
 *   &lt;complexContent&gt;
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *       &lt;sequence&gt;
 *         &lt;element name="religion" type="{http://www.ech.ch/xmlns/eCH-0011/8}religionType"/&gt;
 *         &lt;element name="religionValidFrom" type="{http://www.w3.org/2001/XMLSchema}date" minOccurs="0"/&gt;
 *       &lt;/sequence&gt;
 *     &lt;/restriction&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "religionDataType", propOrder = {
    "religion",
    "religionValidFrom"
})
public class ReligionDataType {

    @XmlElement(required = true)
    protected String religion;
    @XmlSchemaType(name = "date")
    protected XMLGregorianCalendar religionValidFrom;

    /**
     * Obtient la valeur de la propriété religion.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getReligion() {
        return religion;
    }

    /**
     * Définit la valeur de la propriété religion.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setReligion(String value) {
        this.religion = value;
    }

    /**
     * Obtient la valeur de la propriété religionValidFrom.
     * 
     * @return
     *     possible object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public XMLGregorianCalendar getReligionValidFrom() {
        return religionValidFrom;
    }

    /**
     * Définit la valeur de la propriété religionValidFrom.
     * 
     * @param value
     *     allowed object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public void setReligionValidFrom(XMLGregorianCalendar value) {
        this.religionValidFrom = value;
    }

}
