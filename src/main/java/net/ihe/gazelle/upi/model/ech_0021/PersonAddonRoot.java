
package net.ihe.gazelle.upi.model.ech_0021;

import javax.xml.bind.annotation.*;


/**
 * <p>Classe Java pour anonymous complex type.
 * 
 * <p>Le fragment de schéma suivant indique le contenu attendu figurant dans cette classe.
 * 
 * <pre>
 * &lt;complexType&gt;
 *   &lt;complexContent&gt;
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *       &lt;sequence&gt;
 *         &lt;element name="personAddon" type="{http://www.ech.ch/xmlns/eCH-0021/7}personAddonType"/&gt;
 *       &lt;/sequence&gt;
 *     &lt;/restriction&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = {
    "personAddon"
})
@XmlRootElement(name = "personAddonRoot")
public class PersonAddonRoot {

    @XmlElement(required = true)
    protected PersonAddonType personAddon;

    /**
     * Obtient la valeur de la propriété personAddon.
     * 
     * @return
     *     possible object is
     *     {@link PersonAddonType }
     *     
     */
    public PersonAddonType getPersonAddon() {
        return personAddon;
    }

    /**
     * Définit la valeur de la propriété personAddon.
     * 
     * @param value
     *     allowed object is
     *     {@link PersonAddonType }
     *     
     */
    public void setPersonAddon(PersonAddonType value) {
        this.personAddon = value;
    }

}
