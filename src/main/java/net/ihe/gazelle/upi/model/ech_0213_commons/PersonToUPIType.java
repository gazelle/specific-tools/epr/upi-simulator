
package net.ihe.gazelle.upi.model.ech_0213_commons;

import java.util.ArrayList;
import java.util.List;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlSchemaType;
import javax.xml.bind.annotation.XmlType;
import javax.xml.bind.annotation.adapters.CollapsedStringAdapter;
import javax.xml.bind.annotation.adapters.XmlJavaTypeAdapter;
import net.ihe.gazelle.upi.model.ech_0011.GeneralPlaceType;
import net.ihe.gazelle.upi.model.ech_0011.NationalityDataType;
import net.ihe.gazelle.upi.model.ech_0021.NameOfParentType;
import net.ihe.gazelle.upi.model.ech_0044.DatePartiallyKnownType;


/**
 * <p>Classe Java pour personToUPIType complex type.
 * 
 * <p>Le fragment de schéma suivant indique le contenu attendu figurant dans cette classe.
 * 
 * <pre>
 * &lt;complexType name="personToUPIType"&gt;
 *   &lt;complexContent&gt;
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *       &lt;sequence&gt;
 *         &lt;element name="firstName" type="{http://www.ech.ch/xmlns/eCH-0044/4}baseNameType"/&gt;
 *         &lt;element name="officialName" type="{http://www.ech.ch/xmlns/eCH-0044/4}baseNameType"/&gt;
 *         &lt;element name="originalName" type="{http://www.ech.ch/xmlns/eCH-0044/4}baseNameType" minOccurs="0"/&gt;
 *         &lt;element name="sex" type="{http://www.ech.ch/xmlns/eCH-0044/4}sexType" minOccurs="0"/&gt;
 *         &lt;element name="dateOfBirth" type="{http://www.ech.ch/xmlns/eCH-0044/4}datePartiallyKnownType"/&gt;
 *         &lt;element name="placeOfBirth" type="{http://www.ech.ch/xmlns/eCH-0011/8}generalPlaceType" minOccurs="0"/&gt;
 *         &lt;sequence maxOccurs="2" minOccurs="0"&gt;
 *           &lt;element name="mothersName" type="{http://www.ech.ch/xmlns/eCH-0021/7}nameOfParentType"/&gt;
 *         &lt;/sequence&gt;
 *         &lt;sequence maxOccurs="2" minOccurs="0"&gt;
 *           &lt;element name="fathersName" type="{http://www.ech.ch/xmlns/eCH-0021/7}nameOfParentType"/&gt;
 *         &lt;/sequence&gt;
 *         &lt;element name="nationalityData" type="{http://www.ech.ch/xmlns/eCH-0011/8}nationalityDataType" minOccurs="0"/&gt;
 *       &lt;/sequence&gt;
 *     &lt;/restriction&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "personToUPIType", propOrder = {
    "firstName",
    "officialName",
    "originalName",
    "sex",
    "dateOfBirth",
    "placeOfBirth",
    "mothersName",
    "fathersName",
    "nationalityData"
})
public class PersonToUPIType {

    @XmlElement(required = true)
    @XmlJavaTypeAdapter(CollapsedStringAdapter.class)
    @XmlSchemaType(name = "token")
    protected String firstName;
    @XmlElement(required = true)
    @XmlJavaTypeAdapter(CollapsedStringAdapter.class)
    @XmlSchemaType(name = "token")
    protected String officialName;
    @XmlJavaTypeAdapter(CollapsedStringAdapter.class)
    @XmlSchemaType(name = "token")
    protected String originalName;
    protected String sex;
    @XmlElement(required = true)
    protected DatePartiallyKnownType dateOfBirth;
    protected GeneralPlaceType placeOfBirth;
    @XmlElement(nillable = true)
    protected List<NameOfParentType> mothersName;
    @XmlElement(nillable = true)
    protected List<NameOfParentType> fathersName;
    protected NationalityDataType nationalityData;

    /**
     * Obtient la valeur de la propriété firstName.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getFirstName() {
        return firstName;
    }

    /**
     * Définit la valeur de la propriété firstName.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setFirstName(String value) {
        this.firstName = value;
    }

    /**
     * Obtient la valeur de la propriété officialName.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getOfficialName() {
        return officialName;
    }

    /**
     * Définit la valeur de la propriété officialName.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setOfficialName(String value) {
        this.officialName = value;
    }

    /**
     * Obtient la valeur de la propriété originalName.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getOriginalName() {
        return originalName;
    }

    /**
     * Définit la valeur de la propriété originalName.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setOriginalName(String value) {
        this.originalName = value;
    }

    /**
     * Obtient la valeur de la propriété sex.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getSex() {
        return sex;
    }

    /**
     * Définit la valeur de la propriété sex.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setSex(String value) {
        this.sex = value;
    }

    /**
     * Obtient la valeur de la propriété dateOfBirth.
     * 
     * @return
     *     possible object is
     *     {@link DatePartiallyKnownType }
     *     
     */
    public DatePartiallyKnownType getDateOfBirth() {
        return dateOfBirth;
    }

    /**
     * Définit la valeur de la propriété dateOfBirth.
     * 
     * @param value
     *     allowed object is
     *     {@link DatePartiallyKnownType }
     *     
     */
    public void setDateOfBirth(DatePartiallyKnownType value) {
        this.dateOfBirth = value;
    }

    /**
     * Obtient la valeur de la propriété placeOfBirth.
     * 
     * @return
     *     possible object is
     *     {@link GeneralPlaceType }
     *     
     */
    public GeneralPlaceType getPlaceOfBirth() {
        return placeOfBirth;
    }

    /**
     * Définit la valeur de la propriété placeOfBirth.
     * 
     * @param value
     *     allowed object is
     *     {@link GeneralPlaceType }
     *     
     */
    public void setPlaceOfBirth(GeneralPlaceType value) {
        this.placeOfBirth = value;
    }

    /**
     * Gets the value of the mothersName property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the mothersName property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getMothersName().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link NameOfParentType }
     * 
     * 
     */
    public List<NameOfParentType> getMothersName() {
        if (mothersName == null) {
            mothersName = new ArrayList<NameOfParentType>();
        }
        return this.mothersName;
    }

    /**
     * Gets the value of the fathersName property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the fathersName property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getFathersName().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link NameOfParentType }
     * 
     * 
     */
    public List<NameOfParentType> getFathersName() {
        if (fathersName == null) {
            fathersName = new ArrayList<NameOfParentType>();
        }
        return this.fathersName;
    }

    /**
     * Obtient la valeur de la propriété nationalityData.
     * 
     * @return
     *     possible object is
     *     {@link NationalityDataType }
     *     
     */
    public NationalityDataType getNationalityData() {
        return nationalityData;
    }

    /**
     * Définit la valeur de la propriété nationalityData.
     * 
     * @param value
     *     allowed object is
     *     {@link NationalityDataType }
     *     
     */
    public void setNationalityData(NationalityDataType value) {
        this.nationalityData = value;
    }

}
