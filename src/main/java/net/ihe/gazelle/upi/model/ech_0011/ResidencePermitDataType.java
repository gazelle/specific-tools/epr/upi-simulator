
package net.ihe.gazelle.upi.model.ech_0011;

import javax.xml.bind.annotation.*;
import javax.xml.datatype.XMLGregorianCalendar;


/**
 * <p>Classe Java pour residencePermitDataType complex type.
 * 
 * <p>Le fragment de schéma suivant indique le contenu attendu figurant dans cette classe.
 * 
 * <pre>
 * &lt;complexType name="residencePermitDataType"&gt;
 *   &lt;complexContent&gt;
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *       &lt;sequence&gt;
 *         &lt;element name="residencePermit" type="{http://www.ech.ch/xmlns/eCH-0006/2}residencePermitType"/&gt;
 *         &lt;element name="residencePermitValidFrom" type="{http://www.w3.org/2001/XMLSchema}date" minOccurs="0"/&gt;
 *         &lt;element name="residencePermitValidTill" type="{http://www.w3.org/2001/XMLSchema}date" minOccurs="0"/&gt;
 *         &lt;element name="entryDate" type="{http://www.w3.org/2001/XMLSchema}date" minOccurs="0"/&gt;
 *       &lt;/sequence&gt;
 *     &lt;/restriction&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "residencePermitDataType", propOrder = {
    "residencePermit",
    "residencePermitValidFrom",
    "residencePermitValidTill",
    "entryDate"
})
public class ResidencePermitDataType {

    @XmlElement(required = true, nillable = true)
    protected String residencePermit;
    @XmlSchemaType(name = "date")
    protected XMLGregorianCalendar residencePermitValidFrom;
    @XmlSchemaType(name = "date")
    protected XMLGregorianCalendar residencePermitValidTill;
    @XmlSchemaType(name = "date")
    protected XMLGregorianCalendar entryDate;

    /**
     * Obtient la valeur de la propriété residencePermit.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getResidencePermit() {
        return residencePermit;
    }

    /**
     * Définit la valeur de la propriété residencePermit.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setResidencePermit(String value) {
        this.residencePermit = value;
    }

    /**
     * Obtient la valeur de la propriété residencePermitValidFrom.
     * 
     * @return
     *     possible object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public XMLGregorianCalendar getResidencePermitValidFrom() {
        return residencePermitValidFrom;
    }

    /**
     * Définit la valeur de la propriété residencePermitValidFrom.
     * 
     * @param value
     *     allowed object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public void setResidencePermitValidFrom(XMLGregorianCalendar value) {
        this.residencePermitValidFrom = value;
    }

    /**
     * Obtient la valeur de la propriété residencePermitValidTill.
     * 
     * @return
     *     possible object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public XMLGregorianCalendar getResidencePermitValidTill() {
        return residencePermitValidTill;
    }

    /**
     * Définit la valeur de la propriété residencePermitValidTill.
     * 
     * @param value
     *     allowed object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public void setResidencePermitValidTill(XMLGregorianCalendar value) {
        this.residencePermitValidTill = value;
    }

    /**
     * Obtient la valeur de la propriété entryDate.
     * 
     * @return
     *     possible object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public XMLGregorianCalendar getEntryDate() {
        return entryDate;
    }

    /**
     * Définit la valeur de la propriété entryDate.
     * 
     * @param value
     *     allowed object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public void setEntryDate(XMLGregorianCalendar value) {
        this.entryDate = value;
    }

}
