
package net.ihe.gazelle.upi.model.ech_0021;

import javax.xml.bind.annotation.*;
import javax.xml.datatype.XMLGregorianCalendar;


/**
 * <p>Classe Java pour lockDataType complex type.
 * 
 * <p>Le fragment de schéma suivant indique le contenu attendu figurant dans cette classe.
 * 
 * <pre>
 * &lt;complexType name="lockDataType"&gt;
 *   &lt;complexContent&gt;
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *       &lt;sequence&gt;
 *         &lt;element name="dataLock" type="{http://www.ech.ch/xmlns/eCH-0021/7}dataLockType"/&gt;
 *         &lt;element name="dataLockValidFrom" type="{http://www.w3.org/2001/XMLSchema}date" minOccurs="0"/&gt;
 *         &lt;element name="dataLockValidTill" type="{http://www.w3.org/2001/XMLSchema}date" minOccurs="0"/&gt;
 *         &lt;element name="paperLock" type="{http://www.ech.ch/xmlns/eCH-0021/7}paperLockType"/&gt;
 *         &lt;element name="paperLockValidFrom" type="{http://www.w3.org/2001/XMLSchema}date" minOccurs="0"/&gt;
 *         &lt;element name="paperLockValidTill" type="{http://www.w3.org/2001/XMLSchema}date" minOccurs="0"/&gt;
 *       &lt;/sequence&gt;
 *     &lt;/restriction&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "lockDataType", propOrder = {
    "dataLock",
    "dataLockValidFrom",
    "dataLockValidTill",
    "paperLock",
    "paperLockValidFrom",
    "paperLockValidTill"
})
public class LockDataType {

    @XmlElement(required = true)
    protected String dataLock;
    @XmlSchemaType(name = "date")
    protected XMLGregorianCalendar dataLockValidFrom;
    @XmlSchemaType(name = "date")
    protected XMLGregorianCalendar dataLockValidTill;
    @XmlElement(required = true)
    protected String paperLock;
    @XmlSchemaType(name = "date")
    protected XMLGregorianCalendar paperLockValidFrom;
    @XmlSchemaType(name = "date")
    protected XMLGregorianCalendar paperLockValidTill;

    /**
     * Obtient la valeur de la propriété dataLock.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getDataLock() {
        return dataLock;
    }

    /**
     * Définit la valeur de la propriété dataLock.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setDataLock(String value) {
        this.dataLock = value;
    }

    /**
     * Obtient la valeur de la propriété dataLockValidFrom.
     * 
     * @return
     *     possible object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public XMLGregorianCalendar getDataLockValidFrom() {
        return dataLockValidFrom;
    }

    /**
     * Définit la valeur de la propriété dataLockValidFrom.
     * 
     * @param value
     *     allowed object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public void setDataLockValidFrom(XMLGregorianCalendar value) {
        this.dataLockValidFrom = value;
    }

    /**
     * Obtient la valeur de la propriété dataLockValidTill.
     * 
     * @return
     *     possible object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public XMLGregorianCalendar getDataLockValidTill() {
        return dataLockValidTill;
    }

    /**
     * Définit la valeur de la propriété dataLockValidTill.
     * 
     * @param value
     *     allowed object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public void setDataLockValidTill(XMLGregorianCalendar value) {
        this.dataLockValidTill = value;
    }

    /**
     * Obtient la valeur de la propriété paperLock.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getPaperLock() {
        return paperLock;
    }

    /**
     * Définit la valeur de la propriété paperLock.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setPaperLock(String value) {
        this.paperLock = value;
    }

    /**
     * Obtient la valeur de la propriété paperLockValidFrom.
     * 
     * @return
     *     possible object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public XMLGregorianCalendar getPaperLockValidFrom() {
        return paperLockValidFrom;
    }

    /**
     * Définit la valeur de la propriété paperLockValidFrom.
     * 
     * @param value
     *     allowed object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public void setPaperLockValidFrom(XMLGregorianCalendar value) {
        this.paperLockValidFrom = value;
    }

    /**
     * Obtient la valeur de la propriété paperLockValidTill.
     * 
     * @return
     *     possible object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public XMLGregorianCalendar getPaperLockValidTill() {
        return paperLockValidTill;
    }

    /**
     * Définit la valeur de la propriété paperLockValidTill.
     * 
     * @param value
     *     allowed object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public void setPaperLockValidTill(XMLGregorianCalendar value) {
        this.paperLockValidTill = value;
    }

}
