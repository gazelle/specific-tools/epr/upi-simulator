
package net.ihe.gazelle.upi.model.ech_0011;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Classe Java pour deathDataType complex type.
 * 
 * <p>Le fragment de schéma suivant indique le contenu attendu figurant dans cette classe.
 * 
 * <pre>
 * &lt;complexType name="deathDataType"&gt;
 *   &lt;complexContent&gt;
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *       &lt;sequence&gt;
 *         &lt;element name="deathPeriod" type="{http://www.ech.ch/xmlns/eCH-0011/8}deathPeriodType"/&gt;
 *         &lt;element name="placeOfDeath" type="{http://www.ech.ch/xmlns/eCH-0011/8}generalPlaceType" minOccurs="0"/&gt;
 *       &lt;/sequence&gt;
 *     &lt;/restriction&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "deathDataType", propOrder = {
    "deathPeriod",
    "placeOfDeath"
})
public class DeathDataType {

    @XmlElement(required = true)
    protected DeathPeriodType deathPeriod;
    protected GeneralPlaceType placeOfDeath;

    /**
     * Obtient la valeur de la propriété deathPeriod.
     * 
     * @return
     *     possible object is
     *     {@link DeathPeriodType }
     *     
     */
    public DeathPeriodType getDeathPeriod() {
        return deathPeriod;
    }

    /**
     * Définit la valeur de la propriété deathPeriod.
     * 
     * @param value
     *     allowed object is
     *     {@link DeathPeriodType }
     *     
     */
    public void setDeathPeriod(DeathPeriodType value) {
        this.deathPeriod = value;
    }

    /**
     * Obtient la valeur de la propriété placeOfDeath.
     * 
     * @return
     *     possible object is
     *     {@link GeneralPlaceType }
     *     
     */
    public GeneralPlaceType getPlaceOfDeath() {
        return placeOfDeath;
    }

    /**
     * Définit la valeur de la propriété placeOfDeath.
     * 
     * @param value
     *     allowed object is
     *     {@link GeneralPlaceType }
     *     
     */
    public void setPlaceOfDeath(GeneralPlaceType value) {
        this.placeOfDeath = value;
    }

}
