package net.ihe.gazelle.upi.model.error;

public class WarnErrorKeyValue {

    private WarnErrorKeyValue() {
    }

    public static final String CODE_ERROR_KEY="code";
    public static final String MSG_ERROR_KEY="codeDescription";
    public static final String MESSAGE_DATE="messageDate";

    /*WARNING CODE*/
    public static final String WARNING="warning";

    public static final String CODE_WARN_DESCREPANCY_VALUE="210401";
    public static final String MSG_WARN_DESCREPANCY_VALUE="The correspondence between the demographic data and the NAVS leaves some doubt as to the correct identification";

    /*ERROR CODE*/
    public static final String NEGATIVE_REPORT="negativeReport";

    public static final String CODE_ERROR_XSD_VALUE="300001";
    public static final String MSG_ERROR_XSD_VALUE="The structure of the announcement generated from the original announcement is not correct (not valid XSD).";

    public static final String CODE_ERROR_ACTION_ON_SPID_VALUE="300501";
    public static final String MGG_ERROR_ACTION_ON_SPID_VALUE="The actionOnSPID element contains an unexpected value";

    public static final String CODE_ERROR_SPID_MANDATORY="307101";
    public static final String MSG_ERROR_SPID_MANDATORY="The presence of SPID is mandatory";

    public static final String CODE_ERROR_NAV13_VALUE="310402";
    public static final String MSG_ERROR_NAV13_VALUE="There is no correspondence between the demographic data and the announced NAVS13.";

}
