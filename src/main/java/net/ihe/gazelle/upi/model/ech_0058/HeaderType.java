
package net.ihe.gazelle.upi.model.ech_0058;

import javax.xml.bind.annotation.*;
import javax.xml.bind.annotation.adapters.CollapsedStringAdapter;
import javax.xml.bind.annotation.adapters.XmlJavaTypeAdapter;
import javax.xml.datatype.XMLGregorianCalendar;
import java.util.ArrayList;
import java.util.List;


/**
 * <p>Classe Java pour headerType complex type.
 * 
 * <p>Le fragment de schéma suivant indique le contenu attendu figurant dans cette classe.
 * 
 * <pre>
 * &lt;complexType name="headerType"&gt;
 *   &lt;complexContent&gt;
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *       &lt;sequence&gt;
 *         &lt;element name="senderId" type="{http://www.ech.ch/xmlns/eCH-0058/5}participantIdType"/&gt;
 *         &lt;element name="originalSenderId" type="{http://www.ech.ch/xmlns/eCH-0058/5}participantIdType" minOccurs="0"/&gt;
 *         &lt;element name="declarationLocalReference" type="{http://www.ech.ch/xmlns/eCH-0058/5}declarationLocalReferenceType" minOccurs="0"/&gt;
 *         &lt;element name="recipientId" type="{http://www.ech.ch/xmlns/eCH-0058/5}participantIdType" maxOccurs="unbounded" minOccurs="0"/&gt;
 *         &lt;element name="messageId" type="{http://www.ech.ch/xmlns/eCH-0058/5}messageIdType"/&gt;
 *         &lt;element name="referenceMessageId" type="{http://www.ech.ch/xmlns/eCH-0058/5}messageIdType" minOccurs="0"/&gt;
 *         &lt;element name="businessProcessId" type="{http://www.ech.ch/xmlns/eCH-0058/5}businessProcessIdType" minOccurs="0"/&gt;
 *         &lt;element name="ourBusinessReferenceId" type="{http://www.ech.ch/xmlns/eCH-0058/5}businessReferenceIdType" minOccurs="0"/&gt;
 *         &lt;element name="yourBusinessReferenceId" type="{http://www.ech.ch/xmlns/eCH-0058/5}businessReferenceIdType" minOccurs="0"/&gt;
 *         &lt;element name="uniqueIdBusinessTransaction" type="{http://www.ech.ch/xmlns/eCH-0058/5}uniqueIdBusinessTransactionType" minOccurs="0"/&gt;
 *         &lt;element name="messageType" type="{http://www.ech.ch/xmlns/eCH-0058/5}messageTypeType"/&gt;
 *         &lt;element name="subMessageType" type="{http://www.ech.ch/xmlns/eCH-0058/5}subMessageTypeType" minOccurs="0"/&gt;
 *         &lt;element name="sendingApplication" type="{http://www.ech.ch/xmlns/eCH-0058/5}sendingApplicationType"/&gt;
 *         &lt;element name="partialDelivery" type="{http://www.ech.ch/xmlns/eCH-0058/5}partialDeliveryType" minOccurs="0"/&gt;
 *         &lt;element name="subject" type="{http://www.ech.ch/xmlns/eCH-0058/5}subjectType" minOccurs="0"/&gt;
 *         &lt;element name="comment" type="{http://www.ech.ch/xmlns/eCH-0058/5}commentType" minOccurs="0"/&gt;
 *         &lt;element name="messageDate" type="{http://www.ech.ch/xmlns/eCH-0058/5}messageDateType"/&gt;
 *         &lt;element name="initialMessageDate" type="{http://www.ech.ch/xmlns/eCH-0058/5}messageDateType" minOccurs="0"/&gt;
 *         &lt;element name="eventDate" type="{http://www.ech.ch/xmlns/eCH-0058/5}eventDateType" minOccurs="0"/&gt;
 *         &lt;element name="modificationDate" type="{http://www.ech.ch/xmlns/eCH-0058/5}eventDateType" minOccurs="0"/&gt;
 *         &lt;element name="action" type="{http://www.ech.ch/xmlns/eCH-0058/5}actionType"/&gt;
 *         &lt;element name="attachment" type="{http://www.w3.org/2001/XMLSchema}anyType" maxOccurs="unbounded" minOccurs="0"/&gt;
 *         &lt;element name="testDeliveryFlag" type="{http://www.ech.ch/xmlns/eCH-0058/5}testDeliveryFlagType"/&gt;
 *         &lt;element name="responseExpected" type="{http://www.ech.ch/xmlns/eCH-0058/5}responseExpectedType" minOccurs="0"/&gt;
 *         &lt;element name="businessCaseClosed" type="{http://www.ech.ch/xmlns/eCH-0058/5}businessCaseClosedType" minOccurs="0"/&gt;
 *         &lt;element name="namedMetaData" type="{http://www.ech.ch/xmlns/eCH-0058/5}namedMetaDataType" maxOccurs="unbounded" minOccurs="0"/&gt;
 *         &lt;element name="extension" type="{http://www.w3.org/2001/XMLSchema}anyType" minOccurs="0"/&gt;
 *       &lt;/sequence&gt;
 *     &lt;/restriction&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "headerType", propOrder = {
    "senderId",
    "originalSenderId",
    "declarationLocalReference",
    "recipientId",
    "messageId",
    "referenceMessageId",
    "businessProcessId",
    "ourBusinessReferenceId",
    "yourBusinessReferenceId",
    "uniqueIdBusinessTransaction",
    "messageType",
    "subMessageType",
    "sendingApplication",
    "partialDelivery",
    "subject",
    "comment",
    "messageDate",
    "initialMessageDate",
    "eventDate",
    "modificationDate",
    "action",
    "attachment",
    "testDeliveryFlag",
    "responseExpected",
    "businessCaseClosed",
    "namedMetaData",
    "extension"
})
public class HeaderType {

    @XmlElement(required = true)
    @XmlSchemaType(name = "anyURI")
    protected String senderId;
    @XmlSchemaType(name = "anyURI")
    protected String originalSenderId;
    @XmlJavaTypeAdapter(CollapsedStringAdapter.class)
    @XmlSchemaType(name = "token")
    protected String declarationLocalReference;
    @XmlSchemaType(name = "anyURI")
    protected List<String> recipientId;
    @XmlElement(required = true)
    @XmlJavaTypeAdapter(CollapsedStringAdapter.class)
    @XmlSchemaType(name = "token")
    protected String messageId;
    @XmlJavaTypeAdapter(CollapsedStringAdapter.class)
    @XmlSchemaType(name = "token")
    protected String referenceMessageId;
    @XmlJavaTypeAdapter(CollapsedStringAdapter.class)
    @XmlSchemaType(name = "token")
    protected String businessProcessId;
    @XmlJavaTypeAdapter(CollapsedStringAdapter.class)
    @XmlSchemaType(name = "token")
    protected String ourBusinessReferenceId;
    @XmlJavaTypeAdapter(CollapsedStringAdapter.class)
    @XmlSchemaType(name = "token")
    protected String yourBusinessReferenceId;
    @XmlJavaTypeAdapter(CollapsedStringAdapter.class)
    @XmlSchemaType(name = "token")
    protected String uniqueIdBusinessTransaction;
    @XmlElement(required = true)
    @XmlSchemaType(name = "anyURI")
    protected String messageType;
    @XmlJavaTypeAdapter(CollapsedStringAdapter.class)
    @XmlSchemaType(name = "token")
    protected String subMessageType;
    @XmlElement(required = true)
    protected SendingApplicationType sendingApplication;
    protected PartialDeliveryType partialDelivery;
    @XmlJavaTypeAdapter(CollapsedStringAdapter.class)
    @XmlSchemaType(name = "token")
    protected String subject;
    @XmlJavaTypeAdapter(CollapsedStringAdapter.class)
    @XmlSchemaType(name = "token")
    protected String comment;
    @XmlElement(required = true)
    @XmlSchemaType(name = "dateTime")
    protected XMLGregorianCalendar messageDate;
    @XmlSchemaType(name = "dateTime")
    protected XMLGregorianCalendar initialMessageDate;
    @XmlSchemaType(name = "date")
    protected XMLGregorianCalendar eventDate;
    @XmlSchemaType(name = "date")
    protected XMLGregorianCalendar modificationDate;
    @XmlElement(required = true)
    protected String action;
    protected List<Object> attachment;
    protected boolean testDeliveryFlag;
    protected Boolean responseExpected;
    protected Boolean businessCaseClosed;
    protected List<NamedMetaDataType> namedMetaData;
    protected Object extension;

    /**
     * Obtient la valeur de la propriété senderId.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getSenderId() {
        return senderId;
    }

    /**
     * Définit la valeur de la propriété senderId.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setSenderId(String value) {
        this.senderId = value;
    }

    /**
     * Obtient la valeur de la propriété originalSenderId.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getOriginalSenderId() {
        return originalSenderId;
    }

    /**
     * Définit la valeur de la propriété originalSenderId.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setOriginalSenderId(String value) {
        this.originalSenderId = value;
    }

    /**
     * Obtient la valeur de la propriété declarationLocalReference.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getDeclarationLocalReference() {
        return declarationLocalReference;
    }

    /**
     * Définit la valeur de la propriété declarationLocalReference.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setDeclarationLocalReference(String value) {
        this.declarationLocalReference = value;
    }

    /**
     * Gets the value of the recipientId property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the recipientId property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getRecipientId().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link String }
     * 
     * 
     */
    public List<String> getRecipientId() {
        if (recipientId == null) {
            recipientId = new ArrayList<String>();
        }
        return this.recipientId;
    }

    /**
     * Obtient la valeur de la propriété messageId.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getMessageId() {
        return messageId;
    }

    /**
     * Définit la valeur de la propriété messageId.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setMessageId(String value) {
        this.messageId = value;
    }

    /**
     * Obtient la valeur de la propriété referenceMessageId.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getReferenceMessageId() {
        return referenceMessageId;
    }

    /**
     * Définit la valeur de la propriété referenceMessageId.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setReferenceMessageId(String value) {
        this.referenceMessageId = value;
    }

    /**
     * Obtient la valeur de la propriété businessProcessId.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getBusinessProcessId() {
        return businessProcessId;
    }

    /**
     * Définit la valeur de la propriété businessProcessId.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setBusinessProcessId(String value) {
        this.businessProcessId = value;
    }

    /**
     * Obtient la valeur de la propriété ourBusinessReferenceId.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getOurBusinessReferenceId() {
        return ourBusinessReferenceId;
    }

    /**
     * Définit la valeur de la propriété ourBusinessReferenceId.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setOurBusinessReferenceId(String value) {
        this.ourBusinessReferenceId = value;
    }

    /**
     * Obtient la valeur de la propriété yourBusinessReferenceId.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getYourBusinessReferenceId() {
        return yourBusinessReferenceId;
    }

    /**
     * Définit la valeur de la propriété yourBusinessReferenceId.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setYourBusinessReferenceId(String value) {
        this.yourBusinessReferenceId = value;
    }

    /**
     * Obtient la valeur de la propriété uniqueIdBusinessTransaction.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getUniqueIdBusinessTransaction() {
        return uniqueIdBusinessTransaction;
    }

    /**
     * Définit la valeur de la propriété uniqueIdBusinessTransaction.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setUniqueIdBusinessTransaction(String value) {
        this.uniqueIdBusinessTransaction = value;
    }

    /**
     * Obtient la valeur de la propriété messageType.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getMessageType() {
        return messageType;
    }

    /**
     * Définit la valeur de la propriété messageType.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setMessageType(String value) {
        this.messageType = value;
    }

    /**
     * Obtient la valeur de la propriété subMessageType.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getSubMessageType() {
        return subMessageType;
    }

    /**
     * Définit la valeur de la propriété subMessageType.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setSubMessageType(String value) {
        this.subMessageType = value;
    }

    /**
     * Obtient la valeur de la propriété sendingApplication.
     * 
     * @return
     *     possible object is
     *     {@link SendingApplicationType }
     *     
     */
    public SendingApplicationType getSendingApplication() {
        return sendingApplication;
    }

    /**
     * Définit la valeur de la propriété sendingApplication.
     * 
     * @param value
     *     allowed object is
     *     {@link SendingApplicationType }
     *     
     */
    public void setSendingApplication(SendingApplicationType value) {
        this.sendingApplication = value;
    }

    /**
     * Obtient la valeur de la propriété partialDelivery.
     * 
     * @return
     *     possible object is
     *     {@link PartialDeliveryType }
     *     
     */
    public PartialDeliveryType getPartialDelivery() {
        return partialDelivery;
    }

    /**
     * Définit la valeur de la propriété partialDelivery.
     * 
     * @param value
     *     allowed object is
     *     {@link PartialDeliveryType }
     *     
     */
    public void setPartialDelivery(PartialDeliveryType value) {
        this.partialDelivery = value;
    }

    /**
     * Obtient la valeur de la propriété subject.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getSubject() {
        return subject;
    }

    /**
     * Définit la valeur de la propriété subject.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setSubject(String value) {
        this.subject = value;
    }

    /**
     * Obtient la valeur de la propriété comment.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getComment() {
        return comment;
    }

    /**
     * Définit la valeur de la propriété comment.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setComment(String value) {
        this.comment = value;
    }

    /**
     * Obtient la valeur de la propriété messageDate.
     * 
     * @return
     *     possible object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public XMLGregorianCalendar getMessageDate() {
        return messageDate;
    }

    /**
     * Définit la valeur de la propriété messageDate.
     * 
     * @param value
     *     allowed object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public void setMessageDate(XMLGregorianCalendar value) {
        this.messageDate = value;
    }

    /**
     * Obtient la valeur de la propriété initialMessageDate.
     * 
     * @return
     *     possible object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public XMLGregorianCalendar getInitialMessageDate() {
        return initialMessageDate;
    }

    /**
     * Définit la valeur de la propriété initialMessageDate.
     * 
     * @param value
     *     allowed object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public void setInitialMessageDate(XMLGregorianCalendar value) {
        this.initialMessageDate = value;
    }

    /**
     * Obtient la valeur de la propriété eventDate.
     * 
     * @return
     *     possible object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public XMLGregorianCalendar getEventDate() {
        return eventDate;
    }

    /**
     * Définit la valeur de la propriété eventDate.
     * 
     * @param value
     *     allowed object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public void setEventDate(XMLGregorianCalendar value) {
        this.eventDate = value;
    }

    /**
     * Obtient la valeur de la propriété modificationDate.
     * 
     * @return
     *     possible object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public XMLGregorianCalendar getModificationDate() {
        return modificationDate;
    }

    /**
     * Définit la valeur de la propriété modificationDate.
     * 
     * @param value
     *     allowed object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public void setModificationDate(XMLGregorianCalendar value) {
        this.modificationDate = value;
    }

    /**
     * Obtient la valeur de la propriété action.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getAction() {
        return action;
    }

    /**
     * Définit la valeur de la propriété action.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setAction(String value) {
        this.action = value;
    }

    /**
     * Gets the value of the attachment property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the attachment property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getAttachment().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link Object }
     * 
     * 
     */
    public List<Object> getAttachment() {
        if (attachment == null) {
            attachment = new ArrayList<Object>();
        }
        return this.attachment;
    }

    /**
     * Obtient la valeur de la propriété testDeliveryFlag.
     * 
     */
    public boolean isTestDeliveryFlag() {
        return testDeliveryFlag;
    }

    /**
     * Définit la valeur de la propriété testDeliveryFlag.
     * 
     */
    public void setTestDeliveryFlag(boolean value) {
        this.testDeliveryFlag = value;
    }

    /**
     * Obtient la valeur de la propriété responseExpected.
     * 
     * @return
     *     possible object is
     *     {@link Boolean }
     *     
     */
    public Boolean isResponseExpected() {
        return responseExpected;
    }

    /**
     * Définit la valeur de la propriété responseExpected.
     * 
     * @param value
     *     allowed object is
     *     {@link Boolean }
     *     
     */
    public void setResponseExpected(Boolean value) {
        this.responseExpected = value;
    }

    /**
     * Obtient la valeur de la propriété businessCaseClosed.
     * 
     * @return
     *     possible object is
     *     {@link Boolean }
     *     
     */
    public Boolean isBusinessCaseClosed() {
        return businessCaseClosed;
    }

    /**
     * Définit la valeur de la propriété businessCaseClosed.
     * 
     * @param value
     *     allowed object is
     *     {@link Boolean }
     *     
     */
    public void setBusinessCaseClosed(Boolean value) {
        this.businessCaseClosed = value;
    }

    /**
     * Gets the value of the namedMetaData property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the namedMetaData property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getNamedMetaData().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link NamedMetaDataType }
     * 
     * 
     */
    public List<NamedMetaDataType> getNamedMetaData() {
        if (namedMetaData == null) {
            namedMetaData = new ArrayList<NamedMetaDataType>();
        }
        return this.namedMetaData;
    }

    /**
     * Obtient la valeur de la propriété extension.
     * 
     * @return
     *     possible object is
     *     {@link Object }
     *     
     */
    public Object getExtension() {
        return extension;
    }

    /**
     * Définit la valeur de la propriété extension.
     * 
     * @param value
     *     allowed object is
     *     {@link Object }
     *     
     */
    public void setExtension(Object value) {
        this.extension = value;
    }

}
