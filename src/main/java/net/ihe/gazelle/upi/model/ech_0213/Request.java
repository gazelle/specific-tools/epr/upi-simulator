
package net.ihe.gazelle.upi.model.ech_0213;

import java.math.BigInteger;
import java.util.ArrayList;
import java.util.List;
import javax.xml.bind.JAXBElement;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlElementRef;
import javax.xml.bind.annotation.XmlElementRefs;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlSchemaType;
import javax.xml.bind.annotation.XmlType;
import javax.xml.bind.annotation.adapters.CollapsedStringAdapter;
import javax.xml.bind.annotation.adapters.XmlJavaTypeAdapter;
import net.ihe.gazelle.upi.model.ech_0058.HeaderType;
import net.ihe.gazelle.upi.model.ech_0213_commons.PersonToUPIType;
import net.ihe.gazelle.upi.model.ech_0213_commons.PidsToUPIType;


/**
 * <p>Classe Java pour anonymous complex type.
 * 
 * <p>Le fragment de schéma suivant indique le contenu attendu figurant dans cette classe.
 * 
 * <pre>
 * &lt;complexType&gt;
 *   &lt;complexContent&gt;
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *       &lt;sequence&gt;
 *         &lt;element name="header" type="{http://www.ech.ch/xmlns/eCH-0058/5}headerType"/&gt;
 *         &lt;element name="content"&gt;
 *           &lt;complexType&gt;
 *             &lt;complexContent&gt;
 *               &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *                 &lt;sequence&gt;
 *                   &lt;element name="SPIDCategory" type="{http://www.ech.ch/xmlns/eCH-0044/4}personIdCategoryType"/&gt;
 *                   &lt;element name="responseLanguage" type="{http://www.ech.ch/xmlns/eCH-0011/8}languageType"/&gt;
 *                   &lt;element name="actionOnSPID" type="{http://www.ech.ch/xmlns/eCH-0213/1}actionOnSPIDType"/&gt;
 *                   &lt;sequence maxOccurs="unbounded" minOccurs="0"&gt;
 *                     &lt;element name="additionalInputParameterKey" type="{http://www.ech.ch/xmlns/eCH-0213/1}additionalInputParameterKeyType"/&gt;
 *                     &lt;element name="additionalInputParameterValue" type="{http://www.ech.ch/xmlns/eCH-0213/1}additionalInputParameterValueType"/&gt;
 *                   &lt;/sequence&gt;
 *                   &lt;sequence maxOccurs="2"&gt;
 *                     &lt;element name="pidsToUPI" type="{http://www.ech.ch/xmlns/eCH-0213-commons/1}pidsToUPIType"/&gt;
 *                   &lt;/sequence&gt;
 *                   &lt;element name="personToUPI" type="{http://www.ech.ch/xmlns/eCH-0213-commons/1}personToUPIType" minOccurs="0"/&gt;
 *                 &lt;/sequence&gt;
 *               &lt;/restriction&gt;
 *             &lt;/complexContent&gt;
 *           &lt;/complexType&gt;
 *         &lt;/element&gt;
 *       &lt;/sequence&gt;
 *       &lt;attribute name="minorVersion" use="required" type="{http://www.w3.org/2001/XMLSchema}integer" /&gt;
 *     &lt;/restriction&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = {
    "header",
    "content"
})
@XmlRootElement(name = "request")
public class Request {

    @XmlElement(required = true)
    protected HeaderType header;
    @XmlElement(required = true)
    protected Content content;
    @XmlAttribute(name = "minorVersion", required = true)
    protected BigInteger minorVersion;

    /**
     * Obtient la valeur de la propriété header.
     * 
     * @return
     *     possible object is
     *     {@link HeaderType }
     *     
     */
    public HeaderType getHeader() {
        return header;
    }

    /**
     * Définit la valeur de la propriété header.
     * 
     * @param value
     *     allowed object is
     *     {@link HeaderType }
     *     
     */
    public void setHeader(HeaderType value) {
        this.header = value;
    }

    /**
     * Obtient la valeur de la propriété content.
     * 
     * @return
     *     possible object is
     *     {@link Content }
     *     
     */
    public Content getContent() {
        return content;
    }

    /**
     * Définit la valeur de la propriété content.
     * 
     * @param value
     *     allowed object is
     *     {@link Content }
     *     
     */
    public void setContent(Content value) {
        this.content = value;
    }

    /**
     * Obtient la valeur de la propriété minorVersion.
     * 
     * @return
     *     possible object is
     *     {@link BigInteger }
     *     
     */
    public BigInteger getMinorVersion() {
        return minorVersion;
    }

    /**
     * Définit la valeur de la propriété minorVersion.
     * 
     * @param value
     *     allowed object is
     *     {@link BigInteger }
     *     
     */
    public void setMinorVersion(BigInteger value) {
        this.minorVersion = value;
    }


    /**
     * <p>Classe Java pour anonymous complex type.
     * 
     * <p>Le fragment de schéma suivant indique le contenu attendu figurant dans cette classe.
     * 
     * <pre>
     * &lt;complexType&gt;
     *   &lt;complexContent&gt;
     *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
     *       &lt;sequence&gt;
     *         &lt;element name="SPIDCategory" type="{http://www.ech.ch/xmlns/eCH-0044/4}personIdCategoryType"/&gt;
     *         &lt;element name="responseLanguage" type="{http://www.ech.ch/xmlns/eCH-0011/8}languageType"/&gt;
     *         &lt;element name="actionOnSPID" type="{http://www.ech.ch/xmlns/eCH-0213/1}actionOnSPIDType"/&gt;
     *         &lt;sequence maxOccurs="unbounded" minOccurs="0"&gt;
     *           &lt;element name="additionalInputParameterKey" type="{http://www.ech.ch/xmlns/eCH-0213/1}additionalInputParameterKeyType"/&gt;
     *           &lt;element name="additionalInputParameterValue" type="{http://www.ech.ch/xmlns/eCH-0213/1}additionalInputParameterValueType"/&gt;
     *         &lt;/sequence&gt;
     *         &lt;sequence maxOccurs="2"&gt;
     *           &lt;element name="pidsToUPI" type="{http://www.ech.ch/xmlns/eCH-0213-commons/1}pidsToUPIType"/&gt;
     *         &lt;/sequence&gt;
     *         &lt;element name="personToUPI" type="{http://www.ech.ch/xmlns/eCH-0213-commons/1}personToUPIType" minOccurs="0"/&gt;
     *       &lt;/sequence&gt;
     *     &lt;/restriction&gt;
     *   &lt;/complexContent&gt;
     * &lt;/complexType&gt;
     * </pre>
     * 
     * 
     */
    @XmlAccessorType(XmlAccessType.FIELD)
    @XmlType(name = "", propOrder = {
        "spidCategory",
        "responseLanguage",
        "actionOnSPID",
        "additionalInputParameterKeyAndAdditionalInputParameterValue",
        "pidsToUPI",
        "personToUPI"
    })
    public static class Content {

        @XmlElement(name = "SPIDCategory", required = true)
        @XmlJavaTypeAdapter(CollapsedStringAdapter.class)
        @XmlSchemaType(name = "token")
        protected String spidCategory;
        @XmlElement(required = true)
        @XmlJavaTypeAdapter(CollapsedStringAdapter.class)
        @XmlSchemaType(name = "token")
        protected String responseLanguage;
        @XmlElement(required = true)
        @XmlJavaTypeAdapter(CollapsedStringAdapter.class)
        @XmlSchemaType(name = "token")
        protected String actionOnSPID;
        @XmlElementRefs({
            @XmlElementRef(name = "additionalInputParameterKey", namespace = "http://www.ech.ch/xmlns/eCH-0213/1", type = JAXBElement.class, required = false),
            @XmlElementRef(name = "additionalInputParameterValue", namespace = "http://www.ech.ch/xmlns/eCH-0213/1", type = JAXBElement.class, required = false)
        })
        protected List<JAXBElement<String>> additionalInputParameterKeyAndAdditionalInputParameterValue;
        @XmlElement(required = true)
        protected List<PidsToUPIType> pidsToUPI;
        protected PersonToUPIType personToUPI;

        /**
         * Obtient la valeur de la propriété spidCategory.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getSPIDCategory() {
            return spidCategory;
        }

        /**
         * Définit la valeur de la propriété spidCategory.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setSPIDCategory(String value) {
            this.spidCategory = value;
        }

        /**
         * Obtient la valeur de la propriété responseLanguage.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getResponseLanguage() {
            return responseLanguage;
        }

        /**
         * Définit la valeur de la propriété responseLanguage.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setResponseLanguage(String value) {
            this.responseLanguage = value;
        }

        /**
         * Obtient la valeur de la propriété actionOnSPID.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getActionOnSPID() {
            return actionOnSPID;
        }

        /**
         * Définit la valeur de la propriété actionOnSPID.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setActionOnSPID(String value) {
            this.actionOnSPID = value;
        }

        /**
         * Gets the value of the additionalInputParameterKeyAndAdditionalInputParameterValue property.
         * 
         * <p>
         * This accessor method returns a reference to the live list,
         * not a snapshot. Therefore any modification you make to the
         * returned list will be present inside the JAXB object.
         * This is why there is not a <CODE>set</CODE> method for the additionalInputParameterKeyAndAdditionalInputParameterValue property.
         * 
         * <p>
         * For example, to add a new item, do as follows:
         * <pre>
         *    getAdditionalInputParameterKeyAndAdditionalInputParameterValue().add(newItem);
         * </pre>
         * 
         * 
         * <p>
         * Objects of the following type(s) are allowed in the list
         * {@link JAXBElement }{@code <}{@link String }{@code >}
         * {@link JAXBElement }{@code <}{@link String }{@code >}
         * 
         * 
         */
        public List<JAXBElement<String>> getAdditionalInputParameterKeyAndAdditionalInputParameterValue() {
            if (additionalInputParameterKeyAndAdditionalInputParameterValue == null) {
                additionalInputParameterKeyAndAdditionalInputParameterValue = new ArrayList<JAXBElement<String>>();
            }
            return this.additionalInputParameterKeyAndAdditionalInputParameterValue;
        }

        /**
         * Gets the value of the pidsToUPI property.
         * 
         * <p>
         * This accessor method returns a reference to the live list,
         * not a snapshot. Therefore any modification you make to the
         * returned list will be present inside the JAXB object.
         * This is why there is not a <CODE>set</CODE> method for the pidsToUPI property.
         * 
         * <p>
         * For example, to add a new item, do as follows:
         * <pre>
         *    getPidsToUPI().add(newItem);
         * </pre>
         * 
         * 
         * <p>
         * Objects of the following type(s) are allowed in the list
         * {@link PidsToUPIType }
         * 
         * 
         */
        public List<PidsToUPIType> getPidsToUPI() {
            if (pidsToUPI == null) {
                pidsToUPI = new ArrayList<PidsToUPIType>();
            }
            return this.pidsToUPI;
        }

        /**
         * Obtient la valeur de la propriété personToUPI.
         * 
         * @return
         *     possible object is
         *     {@link PersonToUPIType }
         *     
         */
        public PersonToUPIType getPersonToUPI() {
            return personToUPI;
        }

        /**
         * Définit la valeur de la propriété personToUPI.
         * 
         * @param value
         *     allowed object is
         *     {@link PersonToUPIType }
         *     
         */
        public void setPersonToUPI(PersonToUPIType value) {
            this.personToUPI = value;
        }

    }

}
