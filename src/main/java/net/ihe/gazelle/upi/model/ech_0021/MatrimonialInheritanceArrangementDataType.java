
package net.ihe.gazelle.upi.model.ech_0021;

import javax.xml.bind.annotation.*;
import javax.xml.datatype.XMLGregorianCalendar;


/**
 * <p>Classe Java pour matrimonialInheritanceArrangementDataType complex type.
 * 
 * <p>Le fragment de schéma suivant indique le contenu attendu figurant dans cette classe.
 * 
 * <pre>
 * &lt;complexType name="matrimonialInheritanceArrangementDataType"&gt;
 *   &lt;complexContent&gt;
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *       &lt;sequence&gt;
 *         &lt;element name="matrimonialInheritanceArrangement" type="{http://www.ech.ch/xmlns/eCH-0011/8}yesNoType"/&gt;
 *         &lt;element name="matrimonialInheritanceArrangementValidFrom" type="{http://www.w3.org/2001/XMLSchema}date" minOccurs="0"/&gt;
 *       &lt;/sequence&gt;
 *     &lt;/restriction&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "matrimonialInheritanceArrangementDataType", propOrder = {
    "matrimonialInheritanceArrangement",
    "matrimonialInheritanceArrangementValidFrom"
})
public class MatrimonialInheritanceArrangementDataType {

    @XmlElement(required = true)
    protected String matrimonialInheritanceArrangement;
    @XmlSchemaType(name = "date")
    protected XMLGregorianCalendar matrimonialInheritanceArrangementValidFrom;

    /**
     * Obtient la valeur de la propriété matrimonialInheritanceArrangement.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getMatrimonialInheritanceArrangement() {
        return matrimonialInheritanceArrangement;
    }

    /**
     * Définit la valeur de la propriété matrimonialInheritanceArrangement.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setMatrimonialInheritanceArrangement(String value) {
        this.matrimonialInheritanceArrangement = value;
    }

    /**
     * Obtient la valeur de la propriété matrimonialInheritanceArrangementValidFrom.
     * 
     * @return
     *     possible object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public XMLGregorianCalendar getMatrimonialInheritanceArrangementValidFrom() {
        return matrimonialInheritanceArrangementValidFrom;
    }

    /**
     * Définit la valeur de la propriété matrimonialInheritanceArrangementValidFrom.
     * 
     * @param value
     *     allowed object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public void setMatrimonialInheritanceArrangementValidFrom(XMLGregorianCalendar value) {
        this.matrimonialInheritanceArrangementValidFrom = value;
    }

}
