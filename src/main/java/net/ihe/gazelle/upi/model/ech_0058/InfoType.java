
package net.ihe.gazelle.upi.model.ech_0058;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Classe Java pour infoType complex type.
 * 
 * <p>Le fragment de schéma suivant indique le contenu attendu figurant dans cette classe.
 * 
 * <pre>
 * &lt;complexType name="infoType"&gt;
 *   &lt;complexContent&gt;
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *       &lt;choice&gt;
 *         &lt;element name="positiveReport"&gt;
 *           &lt;complexType&gt;
 *             &lt;complexContent&gt;
 *               &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *                 &lt;sequence&gt;
 *                   &lt;element name="notice" type="{http://www.w3.org/2001/XMLSchema}anyType"/&gt;
 *                   &lt;element name="data" type="{http://www.w3.org/2001/XMLSchema}anyType" minOccurs="0"/&gt;
 *                 &lt;/sequence&gt;
 *               &lt;/restriction&gt;
 *             &lt;/complexContent&gt;
 *           &lt;/complexType&gt;
 *         &lt;/element&gt;
 *         &lt;element name="negativeReport"&gt;
 *           &lt;complexType&gt;
 *             &lt;complexContent&gt;
 *               &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *                 &lt;sequence&gt;
 *                   &lt;element name="notice" type="{http://www.w3.org/2001/XMLSchema}anyType"/&gt;
 *                   &lt;element name="data" type="{http://www.w3.org/2001/XMLSchema}anyType" minOccurs="0"/&gt;
 *                 &lt;/sequence&gt;
 *               &lt;/restriction&gt;
 *             &lt;/complexContent&gt;
 *           &lt;/complexType&gt;
 *         &lt;/element&gt;
 *       &lt;/choice&gt;
 *     &lt;/restriction&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "infoType", propOrder = {
    "positiveReport",
    "negativeReport"
})
public class InfoType {

    protected InfoType.PositiveReport positiveReport;
    protected InfoType.NegativeReport negativeReport;

    /**
     * Obtient la valeur de la propriété positiveReport.
     * 
     * @return
     *     possible object is
     *     {@link InfoType.PositiveReport }
     *     
     */
    public InfoType.PositiveReport getPositiveReport() {
        return positiveReport;
    }

    /**
     * Définit la valeur de la propriété positiveReport.
     * 
     * @param value
     *     allowed object is
     *     {@link InfoType.PositiveReport }
     *     
     */
    public void setPositiveReport(InfoType.PositiveReport value) {
        this.positiveReport = value;
    }

    /**
     * Obtient la valeur de la propriété negativeReport.
     * 
     * @return
     *     possible object is
     *     {@link InfoType.NegativeReport }
     *     
     */
    public InfoType.NegativeReport getNegativeReport() {
        return negativeReport;
    }

    /**
     * Définit la valeur de la propriété negativeReport.
     * 
     * @param value
     *     allowed object is
     *     {@link InfoType.NegativeReport }
     *     
     */
    public void setNegativeReport(InfoType.NegativeReport value) {
        this.negativeReport = value;
    }


    /**
     * <p>Classe Java pour anonymous complex type.
     * 
     * <p>Le fragment de schéma suivant indique le contenu attendu figurant dans cette classe.
     * 
     * <pre>
     * &lt;complexType&gt;
     *   &lt;complexContent&gt;
     *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
     *       &lt;sequence&gt;
     *         &lt;element name="notice" type="{http://www.w3.org/2001/XMLSchema}anyType"/&gt;
     *         &lt;element name="data" type="{http://www.w3.org/2001/XMLSchema}anyType" minOccurs="0"/&gt;
     *       &lt;/sequence&gt;
     *     &lt;/restriction&gt;
     *   &lt;/complexContent&gt;
     * &lt;/complexType&gt;
     * </pre>
     * 
     * 
     */
    @XmlAccessorType(XmlAccessType.FIELD)
    @XmlType(name = "", propOrder = {
        "notice",
        "data"
    })
    public static class NegativeReport {

        @XmlElement(required = true)
        protected Object notice;
        protected Object data;

        /**
         * Obtient la valeur de la propriété notice.
         * 
         * @return
         *     possible object is
         *     {@link Object }
         *     
         */
        public Object getNotice() {
            return notice;
        }

        /**
         * Définit la valeur de la propriété notice.
         * 
         * @param value
         *     allowed object is
         *     {@link Object }
         *     
         */
        public void setNotice(Object value) {
            this.notice = value;
        }

        /**
         * Obtient la valeur de la propriété data.
         * 
         * @return
         *     possible object is
         *     {@link Object }
         *     
         */
        public Object getData() {
            return data;
        }

        /**
         * Définit la valeur de la propriété data.
         * 
         * @param value
         *     allowed object is
         *     {@link Object }
         *     
         */
        public void setData(Object value) {
            this.data = value;
        }

    }


    /**
     * <p>Classe Java pour anonymous complex type.
     * 
     * <p>Le fragment de schéma suivant indique le contenu attendu figurant dans cette classe.
     * 
     * <pre>
     * &lt;complexType&gt;
     *   &lt;complexContent&gt;
     *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
     *       &lt;sequence&gt;
     *         &lt;element name="notice" type="{http://www.w3.org/2001/XMLSchema}anyType"/&gt;
     *         &lt;element name="data" type="{http://www.w3.org/2001/XMLSchema}anyType" minOccurs="0"/&gt;
     *       &lt;/sequence&gt;
     *     &lt;/restriction&gt;
     *   &lt;/complexContent&gt;
     * &lt;/complexType&gt;
     * </pre>
     * 
     * 
     */
    @XmlAccessorType(XmlAccessType.FIELD)
    @XmlType(name = "", propOrder = {
        "notice",
        "data"
    })
    public static class PositiveReport {

        @XmlElement(required = true)
        protected Object notice;
        protected Object data;

        /**
         * Obtient la valeur de la propriété notice.
         * 
         * @return
         *     possible object is
         *     {@link Object }
         *     
         */
        public Object getNotice() {
            return notice;
        }

        /**
         * Définit la valeur de la propriété notice.
         * 
         * @param value
         *     allowed object is
         *     {@link Object }
         *     
         */
        public void setNotice(Object value) {
            this.notice = value;
        }

        /**
         * Obtient la valeur de la propriété data.
         * 
         * @return
         *     possible object is
         *     {@link Object }
         *     
         */
        public Object getData() {
            return data;
        }

        /**
         * Définit la valeur de la propriété data.
         * 
         * @param value
         *     allowed object is
         *     {@link Object }
         *     
         */
        public void setData(Object value) {
            this.data = value;
        }

    }

}
