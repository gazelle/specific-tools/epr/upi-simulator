
package net.ihe.gazelle.upi.model.ech_0214;

import java.math.BigInteger;
import java.util.ArrayList;
import java.util.List;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlSchemaType;
import javax.xml.bind.annotation.XmlType;
import javax.xml.bind.annotation.adapters.CollapsedStringAdapter;
import javax.xml.bind.annotation.adapters.XmlJavaTypeAdapter;
import net.ihe.gazelle.upi.model.ech_0058.HeaderType;
import net.ihe.gazelle.upi.model.ech_0213_commons.PersonToUPIType;


/**
 * <p>Classe Java pour anonymous complex type.
 *
 * <p>Le fragment de schéma suivant indique le contenu attendu figurant dans cette classe.
 *
 * <pre>
 * &lt;complexType&gt;
 *   &lt;complexContent&gt;
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *       &lt;sequence&gt;
 *         &lt;element name="header" type="{http://www.ech.ch/xmlns/eCH-0058/5}headerType"/&gt;
 *         &lt;element name="content"&gt;
 *           &lt;complexType&gt;
 *             &lt;complexContent&gt;
 *               &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *                 &lt;sequence&gt;
 *                   &lt;element name="SPIDCategory" type="{http://www.ech.ch/xmlns/eCH-0044/4}personIdCategoryType"/&gt;
 *                   &lt;element name="responseLanguage" type="{http://www.ech.ch/xmlns/eCH-0011/8}languageType"/&gt;
 *                   &lt;choice&gt;
 *                     &lt;element name="getInfoPersonRequest" maxOccurs="unbounded"&gt;
 *                       &lt;complexType&gt;
 *                         &lt;complexContent&gt;
 *                           &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *                             &lt;sequence&gt;
 *                               &lt;element name="getInfoPersonRequestId" type="{http://www.ech.ch/xmlns/eCH-0214/2}subrequestIdType"/&gt;
 *                               &lt;element name="detailLevelOfResponse"&gt;
 *                                 &lt;simpleType&gt;
 *                                   &lt;restriction base="{http://www.w3.org/2001/XMLSchema}token"&gt;
 *                                     &lt;minLength value="1"/&gt;
 *                                     &lt;maxLength value="20"/&gt;
 *                                   &lt;/restriction&gt;
 *                                 &lt;/simpleType&gt;
 *                               &lt;/element&gt;
 *                               &lt;element name="pid"&gt;
 *                                 &lt;complexType&gt;
 *                                   &lt;complexContent&gt;
 *                                     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *                                       &lt;choice&gt;
 *                                         &lt;element name="vn" type="{http://www.ech.ch/xmlns/eCH-0044/4}vnType"/&gt;
 *                                         &lt;element name="SPID" type="{http://www.ech.ch/xmlns/eCH-0213-commons/1}SPIDType"/&gt;
 *                                       &lt;/choice&gt;
 *                                     &lt;/restriction&gt;
 *                                   &lt;/complexContent&gt;
 *                                 &lt;/complexType&gt;
 *                               &lt;/element&gt;
 *                             &lt;/sequence&gt;
 *                           &lt;/restriction&gt;
 *                         &lt;/complexContent&gt;
 *                       &lt;/complexType&gt;
 *                     &lt;/element&gt;
 *                     &lt;element name="searchPersonRequest" maxOccurs="unbounded"&gt;
 *                       &lt;complexType&gt;
 *                         &lt;complexContent&gt;
 *                           &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *                             &lt;sequence&gt;
 *                               &lt;element name="searchPersonRequestId" type="{http://www.ech.ch/xmlns/eCH-0214/2}subrequestIdType"/&gt;
 *                               &lt;element name="algorithm" type="{http://www.ech.ch/xmlns/eCH-0214/2}algorithmType" minOccurs="0"/&gt;
 *                               &lt;element name="searchedPerson" type="{http://www.ech.ch/xmlns/eCH-0213-commons/1}personToUPIType"/&gt;
 *                             &lt;/sequence&gt;
 *                           &lt;/restriction&gt;
 *                         &lt;/complexContent&gt;
 *                       &lt;/complexType&gt;
 *                     &lt;/element&gt;
 *                     &lt;element name="compareDataRequest" maxOccurs="unbounded"&gt;
 *                       &lt;complexType&gt;
 *                         &lt;complexContent&gt;
 *                           &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *                             &lt;sequence&gt;
 *                               &lt;element name="compareDataRequestId" type="{http://www.ech.ch/xmlns/eCH-0214/2}subrequestIdType"/&gt;
 *                               &lt;element name="pids"&gt;
 *                                 &lt;complexType&gt;
 *                                   &lt;complexContent&gt;
 *                                     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *                                       &lt;sequence&gt;
 *                                         &lt;element name="vn" type="{http://www.ech.ch/xmlns/eCH-0044/4}vnType"/&gt;
 *                                         &lt;element name="SPID" type="{http://www.ech.ch/xmlns/eCH-0213-commons/1}SPIDType"/&gt;
 *                                       &lt;/sequence&gt;
 *                                     &lt;/restriction&gt;
 *                                   &lt;/complexContent&gt;
 *                                 &lt;/complexType&gt;
 *                               &lt;/element&gt;
 *                             &lt;/sequence&gt;
 *                           &lt;/restriction&gt;
 *                         &lt;/complexContent&gt;
 *                       &lt;/complexType&gt;
 *                     &lt;/element&gt;
 *                   &lt;/choice&gt;
 *                 &lt;/sequence&gt;
 *               &lt;/restriction&gt;
 *             &lt;/complexContent&gt;
 *           &lt;/complexType&gt;
 *         &lt;/element&gt;
 *       &lt;/sequence&gt;
 *       &lt;attribute name="minorVersion" use="required" type="{http://www.w3.org/2001/XMLSchema}integer" /&gt;
 *     &lt;/restriction&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 *
 *
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = {
    "header",
    "content"
})
@XmlRootElement(name = "request")
public class Request {

    @XmlElement(required = true)
    protected HeaderType header;
    @XmlElement(required = true)
    protected Request.Content content;
    @XmlAttribute(name = "minorVersion", required = true)
    protected BigInteger minorVersion;

    /**
     * Obtient la valeur de la propriété header.
     *
     * @return
     *     possible object is
     *     {@link HeaderType }
     *
     */
    public HeaderType getHeader() {
        return header;
    }

    /**
     * Définit la valeur de la propriété header.
     *
     * @param value
     *     allowed object is
     *     {@link HeaderType }
     *
     */
    public void setHeader(HeaderType value) {
        this.header = value;
    }

    /**
     * Obtient la valeur de la propriété content.
     *
     * @return
     *     possible object is
     *     {@link Request.Content }
     *
     */
    public Request.Content getContent() {
        return content;
    }

    /**
     * Définit la valeur de la propriété content.
     *
     * @param value
     *     allowed object is
     *     {@link Request.Content }
     *
     */
    public void setContent(Request.Content value) {
        this.content = value;
    }

    /**
     * Obtient la valeur de la propriété minorVersion.
     *
     * @return
     *     possible object is
     *     {@link BigInteger }
     *
     */
    public BigInteger getMinorVersion() {
        return minorVersion;
    }

    /**
     * Définit la valeur de la propriété minorVersion.
     *
     * @param value
     *     allowed object is
     *     {@link BigInteger }
     *
     */
    public void setMinorVersion(BigInteger value) {
        this.minorVersion = value;
    }


    /**
     * <p>Classe Java pour anonymous complex type.
     *
     * <p>Le fragment de schéma suivant indique le contenu attendu figurant dans cette classe.
     *
     * <pre>
     * &lt;complexType&gt;
     *   &lt;complexContent&gt;
     *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
     *       &lt;sequence&gt;
     *         &lt;element name="SPIDCategory" type="{http://www.ech.ch/xmlns/eCH-0044/4}personIdCategoryType"/&gt;
     *         &lt;element name="responseLanguage" type="{http://www.ech.ch/xmlns/eCH-0011/8}languageType"/&gt;
     *         &lt;choice&gt;
     *           &lt;element name="getInfoPersonRequest" maxOccurs="unbounded"&gt;
     *             &lt;complexType&gt;
     *               &lt;complexContent&gt;
     *                 &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
     *                   &lt;sequence&gt;
     *                     &lt;element name="getInfoPersonRequestId" type="{http://www.ech.ch/xmlns/eCH-0214/2}subrequestIdType"/&gt;
     *                     &lt;element name="detailLevelOfResponse"&gt;
     *                       &lt;simpleType&gt;
     *                         &lt;restriction base="{http://www.w3.org/2001/XMLSchema}token"&gt;
     *                           &lt;minLength value="1"/&gt;
     *                           &lt;maxLength value="20"/&gt;
     *                         &lt;/restriction&gt;
     *                       &lt;/simpleType&gt;
     *                     &lt;/element&gt;
     *                     &lt;element name="pid"&gt;
     *                       &lt;complexType&gt;
     *                         &lt;complexContent&gt;
     *                           &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
     *                             &lt;choice&gt;
     *                               &lt;element name="vn" type="{http://www.ech.ch/xmlns/eCH-0044/4}vnType"/&gt;
     *                               &lt;element name="SPID" type="{http://www.ech.ch/xmlns/eCH-0213-commons/1}SPIDType"/&gt;
     *                             &lt;/choice&gt;
     *                           &lt;/restriction&gt;
     *                         &lt;/complexContent&gt;
     *                       &lt;/complexType&gt;
     *                     &lt;/element&gt;
     *                   &lt;/sequence&gt;
     *                 &lt;/restriction&gt;
     *               &lt;/complexContent&gt;
     *             &lt;/complexType&gt;
     *           &lt;/element&gt;
     *           &lt;element name="searchPersonRequest" maxOccurs="unbounded"&gt;
     *             &lt;complexType&gt;
     *               &lt;complexContent&gt;
     *                 &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
     *                   &lt;sequence&gt;
     *                     &lt;element name="searchPersonRequestId" type="{http://www.ech.ch/xmlns/eCH-0214/2}subrequestIdType"/&gt;
     *                     &lt;element name="algorithm" type="{http://www.ech.ch/xmlns/eCH-0214/2}algorithmType" minOccurs="0"/&gt;
     *                     &lt;element name="searchedPerson" type="{http://www.ech.ch/xmlns/eCH-0213-commons/1}personToUPIType"/&gt;
     *                   &lt;/sequence&gt;
     *                 &lt;/restriction&gt;
     *               &lt;/complexContent&gt;
     *             &lt;/complexType&gt;
     *           &lt;/element&gt;
     *           &lt;element name="compareDataRequest" maxOccurs="unbounded"&gt;
     *             &lt;complexType&gt;
     *               &lt;complexContent&gt;
     *                 &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
     *                   &lt;sequence&gt;
     *                     &lt;element name="compareDataRequestId" type="{http://www.ech.ch/xmlns/eCH-0214/2}subrequestIdType"/&gt;
     *                     &lt;element name="pids"&gt;
     *                       &lt;complexType&gt;
     *                         &lt;complexContent&gt;
     *                           &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
     *                             &lt;sequence&gt;
     *                               &lt;element name="vn" type="{http://www.ech.ch/xmlns/eCH-0044/4}vnType"/&gt;
     *                               &lt;element name="SPID" type="{http://www.ech.ch/xmlns/eCH-0213-commons/1}SPIDType"/&gt;
     *                             &lt;/sequence&gt;
     *                           &lt;/restriction&gt;
     *                         &lt;/complexContent&gt;
     *                       &lt;/complexType&gt;
     *                     &lt;/element&gt;
     *                   &lt;/sequence&gt;
     *                 &lt;/restriction&gt;
     *               &lt;/complexContent&gt;
     *             &lt;/complexType&gt;
     *           &lt;/element&gt;
     *         &lt;/choice&gt;
     *       &lt;/sequence&gt;
     *     &lt;/restriction&gt;
     *   &lt;/complexContent&gt;
     * &lt;/complexType&gt;
     * </pre>
     *
     *
     */
    @XmlAccessorType(XmlAccessType.FIELD)
    @XmlType(name = "", propOrder = {
        "spidCategory",
        "responseLanguage",
        "getInfoPersonRequest",
        "searchPersonRequest",
        "compareDataRequest"
    })
    public static class Content {

        @XmlElement(name = "SPIDCategory", required = true)
        @XmlJavaTypeAdapter(CollapsedStringAdapter.class)
        @XmlSchemaType(name = "token")
        protected String spidCategory;
        @XmlElement(required = true)
        @XmlJavaTypeAdapter(CollapsedStringAdapter.class)
        @XmlSchemaType(name = "token")
        protected String responseLanguage;
        protected List<Request.Content.GetInfoPersonRequest> getInfoPersonRequest;
        protected List<Request.Content.SearchPersonRequest> searchPersonRequest;
        protected List<Request.Content.CompareDataRequest> compareDataRequest;

        /**
         * Obtient la valeur de la propriété spidCategory.
         *
         * @return
         *     possible object is
         *     {@link String }
         *
         */
        public String getSPIDCategory() {
            return spidCategory;
        }

        /**
         * Définit la valeur de la propriété spidCategory.
         *
         * @param value
         *     allowed object is
         *     {@link String }
         *
         */
        public void setSPIDCategory(String value) {
            this.spidCategory = value;
        }

        /**
         * Obtient la valeur de la propriété responseLanguage.
         *
         * @return
         *     possible object is
         *     {@link String }
         *
         */
        public String getResponseLanguage() {
            return responseLanguage;
        }

        /**
         * Définit la valeur de la propriété responseLanguage.
         *
         * @param value
         *     allowed object is
         *     {@link String }
         *
         */
        public void setResponseLanguage(String value) {
            this.responseLanguage = value;
        }

        /**
         * Gets the value of the getInfoPersonRequest property.
         *
         * <p>
         * This accessor method returns a reference to the live list,
         * not a snapshot. Therefore any modification you make to the
         * returned list will be present inside the JAXB object.
         * This is why there is not a <CODE>set</CODE> method for the getInfoPersonRequest property.
         *
         * <p>
         * For example, to add a new item, do as follows:
         * <pre>
         *    getGetInfoPersonRequest().add(newItem);
         * </pre>
         *
         *
         * <p>
         * Objects of the following type(s) are allowed in the list
         * {@link Request.Content.GetInfoPersonRequest }
         *
         *
         */
        public List<Request.Content.GetInfoPersonRequest> getGetInfoPersonRequest() {
            if (getInfoPersonRequest == null) {
                getInfoPersonRequest = new ArrayList<Request.Content.GetInfoPersonRequest>();
            }
            return this.getInfoPersonRequest;
        }

        public void setGetInfoPersonRequest(List<GetInfoPersonRequest> getInfoPersonRequest) {
            this.getInfoPersonRequest = getInfoPersonRequest;
        }

        /**
         * Gets the value of the searchPersonRequest property.
         *
         * <p>
         * This accessor method returns a reference to the live list,
         * not a snapshot. Therefore any modification you make to the
         * returned list will be present inside the JAXB object.
         * This is why there is not a <CODE>set</CODE> method for the searchPersonRequest property.
         *
         * <p>
         * For example, to add a new item, do as follows:
         * <pre>
         *    getSearchPersonRequest().add(newItem);
         * </pre>
         *
         *
         * <p>
         * Objects of the following type(s) are allowed in the list
         * {@link Request.Content.SearchPersonRequest }
         *
         *
         */
        public List<Request.Content.SearchPersonRequest> getSearchPersonRequest() {
            if (searchPersonRequest == null) {
                searchPersonRequest = new ArrayList<Request.Content.SearchPersonRequest>();
            }
            return this.searchPersonRequest;
        }

        public void setSearchPersonRequest(List<SearchPersonRequest> searchPersonRequest) {
            this.searchPersonRequest = searchPersonRequest;
        }

        /**
         * Gets the value of the compareDataRequest property.
         *
         * <p>
         * This accessor method returns a reference to the live list,
         * not a snapshot. Therefore any modification you make to the
         * returned list will be present inside the JAXB object.
         * This is why there is not a <CODE>set</CODE> method for the compareDataRequest property.
         *
         * <p>
         * For example, to add a new item, do as follows:
         * <pre>
         *    getCompareDataRequest().add(newItem);
         * </pre>
         *
         *
         * <p>
         * Objects of the following type(s) are allowed in the list
         * {@link Request.Content.CompareDataRequest }
         *
         *
         */
        public List<Request.Content.CompareDataRequest> getCompareDataRequest() {
            if (compareDataRequest == null) {
                compareDataRequest = new ArrayList<Request.Content.CompareDataRequest>();
            }
            return this.compareDataRequest;
        }

        public void setCompareDataRequest(List<CompareDataRequest> compareDataRequest) {
            this.compareDataRequest = compareDataRequest;
        }

        /**
         * <p>Classe Java pour anonymous complex type.
         *
         * <p>Le fragment de schéma suivant indique le contenu attendu figurant dans cette classe.
         *
         * <pre>
         * &lt;complexType&gt;
         *   &lt;complexContent&gt;
         *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
         *       &lt;sequence&gt;
         *         &lt;element name="compareDataRequestId" type="{http://www.ech.ch/xmlns/eCH-0214/2}subrequestIdType"/&gt;
         *         &lt;element name="pids"&gt;
         *           &lt;complexType&gt;
         *             &lt;complexContent&gt;
         *               &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
         *                 &lt;sequence&gt;
         *                   &lt;element name="vn" type="{http://www.ech.ch/xmlns/eCH-0044/4}vnType"/&gt;
         *                   &lt;element name="SPID" type="{http://www.ech.ch/xmlns/eCH-0213-commons/1}SPIDType"/&gt;
         *                 &lt;/sequence&gt;
         *               &lt;/restriction&gt;
         *             &lt;/complexContent&gt;
         *           &lt;/complexType&gt;
         *         &lt;/element&gt;
         *       &lt;/sequence&gt;
         *     &lt;/restriction&gt;
         *   &lt;/complexContent&gt;
         * &lt;/complexType&gt;
         * </pre>
         *
         *
         */
        @XmlAccessorType(XmlAccessType.FIELD)
        @XmlType(name = "", propOrder = {
            "compareDataRequestId",
            "pids"
        })
        public static class CompareDataRequest {

            @XmlElement(required = true)
            protected BigInteger compareDataRequestId;
            @XmlElement(required = true)
            protected Request.Content.CompareDataRequest.Pids pids;

            /**
             * Obtient la valeur de la propriété compareDataRequestId.
             *
             * @return
             *     possible object is
             *     {@link BigInteger }
             *
             */
            public BigInteger getCompareDataRequestId() {
                return compareDataRequestId;
            }

            /**
             * Définit la valeur de la propriété compareDataRequestId.
             *
             * @param value
             *     allowed object is
             *     {@link BigInteger }
             *
             */
            public void setCompareDataRequestId(BigInteger value) {
                this.compareDataRequestId = value;
            }

            /**
             * Obtient la valeur de la propriété pids.
             *
             * @return
             *     possible object is
             *     {@link Request.Content.CompareDataRequest.Pids }
             *
             */
            public Request.Content.CompareDataRequest.Pids getPids() {
                return pids;
            }

            /**
             * Définit la valeur de la propriété pids.
             *
             * @param value
             *     allowed object is
             *     {@link Request.Content.CompareDataRequest.Pids }
             *
             */
            public void setPids(Request.Content.CompareDataRequest.Pids value) {
                this.pids = value;
            }


            /**
             * <p>Classe Java pour anonymous complex type.
             *
             * <p>Le fragment de schéma suivant indique le contenu attendu figurant dans cette classe.
             *
             * <pre>
             * &lt;complexType&gt;
             *   &lt;complexContent&gt;
             *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
             *       &lt;sequence&gt;
             *         &lt;element name="vn" type="{http://www.ech.ch/xmlns/eCH-0044/4}vnType"/&gt;
             *         &lt;element name="SPID" type="{http://www.ech.ch/xmlns/eCH-0213-commons/1}SPIDType"/&gt;
             *       &lt;/sequence&gt;
             *     &lt;/restriction&gt;
             *   &lt;/complexContent&gt;
             * &lt;/complexType&gt;
             * </pre>
             *
             *
             */
            @XmlAccessorType(XmlAccessType.FIELD)
            @XmlType(name = "", propOrder = {
                "vn",
                "spid"
            })
            public static class Pids {

                @XmlSchemaType(name = "unsignedLong")
                protected long vn;
                @XmlElement(name = "SPID", required = true)
                @XmlJavaTypeAdapter(CollapsedStringAdapter.class)
                @XmlSchemaType(name = "token")
                protected String spid;

                /**
                 * Obtient la valeur de la propriété vn.
                 *
                 */
                public long getVn() {
                    return vn;
                }

                /**
                 * Définit la valeur de la propriété vn.
                 *
                 */
                public void setVn(long value) {
                    this.vn = value;
                }

                /**
                 * Obtient la valeur de la propriété spid.
                 *
                 * @return
                 *     possible object is
                 *     {@link String }
                 *
                 */
                public String getSPID() {
                    return spid;
                }

                /**
                 * Définit la valeur de la propriété spid.
                 *
                 * @param value
                 *     allowed object is
                 *     {@link String }
                 *
                 */
                public void setSPID(String value) {
                    this.spid = value;
                }

            }

        }


        /**
         * <p>Classe Java pour anonymous complex type.
         *
         * <p>Le fragment de schéma suivant indique le contenu attendu figurant dans cette classe.
         *
         * <pre>
         * &lt;complexType&gt;
         *   &lt;complexContent&gt;
         *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
         *       &lt;sequence&gt;
         *         &lt;element name="getInfoPersonRequestId" type="{http://www.ech.ch/xmlns/eCH-0214/2}subrequestIdType"/&gt;
         *         &lt;element name="detailLevelOfResponse"&gt;
         *           &lt;simpleType&gt;
         *             &lt;restriction base="{http://www.w3.org/2001/XMLSchema}token"&gt;
         *               &lt;minLength value="1"/&gt;
         *               &lt;maxLength value="20"/&gt;
         *             &lt;/restriction&gt;
         *           &lt;/simpleType&gt;
         *         &lt;/element&gt;
         *         &lt;element name="pid"&gt;
         *           &lt;complexType&gt;
         *             &lt;complexContent&gt;
         *               &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
         *                 &lt;choice&gt;
         *                   &lt;element name="vn" type="{http://www.ech.ch/xmlns/eCH-0044/4}vnType"/&gt;
         *                   &lt;element name="SPID" type="{http://www.ech.ch/xmlns/eCH-0213-commons/1}SPIDType"/&gt;
         *                 &lt;/choice&gt;
         *               &lt;/restriction&gt;
         *             &lt;/complexContent&gt;
         *           &lt;/complexType&gt;
         *         &lt;/element&gt;
         *       &lt;/sequence&gt;
         *     &lt;/restriction&gt;
         *   &lt;/complexContent&gt;
         * &lt;/complexType&gt;
         * </pre>
         *
         *
         */
        @XmlAccessorType(XmlAccessType.FIELD)
        @XmlType(name = "", propOrder = {
            "getInfoPersonRequestId",
            "detailLevelOfResponse",
            "pid"
        })
        public static class GetInfoPersonRequest {

            @XmlElement(required = true)
            protected BigInteger getInfoPersonRequestId;
            @XmlElement(required = true)
            @XmlJavaTypeAdapter(CollapsedStringAdapter.class)
            protected String detailLevelOfResponse;
            @XmlElement(required = true)
            protected Request.Content.GetInfoPersonRequest.Pid pid;

            /**
             * Obtient la valeur de la propriété getInfoPersonRequestId.
             *
             * @return
             *     possible object is
             *     {@link BigInteger }
             *
             */
            public BigInteger getGetInfoPersonRequestId() {
                return getInfoPersonRequestId;
            }

            /**
             * Définit la valeur de la propriété getInfoPersonRequestId.
             *
             * @param value
             *     allowed object is
             *     {@link BigInteger }
             *
             */
            public void setGetInfoPersonRequestId(BigInteger value) {
                this.getInfoPersonRequestId = value;
            }

            /**
             * Obtient la valeur de la propriété detailLevelOfResponse.
             *
             * @return
             *     possible object is
             *     {@link String }
             *
             */
            public String getDetailLevelOfResponse() {
                return detailLevelOfResponse;
            }

            /**
             * Définit la valeur de la propriété detailLevelOfResponse.
             *
             * @param value
             *     allowed object is
             *     {@link String }
             *
             */
            public void setDetailLevelOfResponse(String value) {
                this.detailLevelOfResponse = value;
            }

            /**
             * Obtient la valeur de la propriété pid.
             *
             * @return
             *     possible object is
             *     {@link Request.Content.GetInfoPersonRequest.Pid }
             *
             */
            public Request.Content.GetInfoPersonRequest.Pid getPid() {
                return pid;
            }

            /**
             * Définit la valeur de la propriété pid.
             *
             * @param value
             *     allowed object is
             *     {@link Request.Content.GetInfoPersonRequest.Pid }
             *
             */
            public void setPid(Request.Content.GetInfoPersonRequest.Pid value) {
                this.pid = value;
            }


            /**
             * <p>Classe Java pour anonymous complex type.
             *
             * <p>Le fragment de schéma suivant indique le contenu attendu figurant dans cette classe.
             *
             * <pre>
             * &lt;complexType&gt;
             *   &lt;complexContent&gt;
             *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
             *       &lt;choice&gt;
             *         &lt;element name="vn" type="{http://www.ech.ch/xmlns/eCH-0044/4}vnType"/&gt;
             *         &lt;element name="SPID" type="{http://www.ech.ch/xmlns/eCH-0213-commons/1}SPIDType"/&gt;
             *       &lt;/choice&gt;
             *     &lt;/restriction&gt;
             *   &lt;/complexContent&gt;
             * &lt;/complexType&gt;
             * </pre>
             *
             *
             */
            @XmlAccessorType(XmlAccessType.FIELD)
            @XmlType(name = "", propOrder = {
                "vn",
                "spid"
            })
            public static class Pid {

                @XmlSchemaType(name = "unsignedLong")
                protected Long vn;
                @XmlElement(name = "SPID")
                @XmlJavaTypeAdapter(CollapsedStringAdapter.class)
                @XmlSchemaType(name = "token")
                protected String spid;

                /**
                 * Obtient la valeur de la propriété vn.
                 *
                 * @return
                 *     possible object is
                 *     {@link Long }
                 *
                 */
                public Long getVn() {
                    return vn;
                }

                /**
                 * Définit la valeur de la propriété vn.
                 *
                 * @param value
                 *     allowed object is
                 *     {@link Long }
                 *
                 */
                public void setVn(Long value) {
                    this.vn = value;
                }

                /**
                 * Obtient la valeur de la propriété spid.
                 *
                 * @return
                 *     possible object is
                 *     {@link String }
                 *
                 */
                public String getSPID() {
                    return spid;
                }

                /**
                 * Définit la valeur de la propriété spid.
                 *
                 * @param value
                 *     allowed object is
                 *     {@link String }
                 *
                 */
                public void setSPID(String value) {
                    this.spid = value;
                }

            }

        }


        /**
         * <p>Classe Java pour anonymous complex type.
         *
         * <p>Le fragment de schéma suivant indique le contenu attendu figurant dans cette classe.
         *
         * <pre>
         * &lt;complexType&gt;
         *   &lt;complexContent&gt;
         *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
         *       &lt;sequence&gt;
         *         &lt;element name="searchPersonRequestId" type="{http://www.ech.ch/xmlns/eCH-0214/2}subrequestIdType"/&gt;
         *         &lt;element name="algorithm" type="{http://www.ech.ch/xmlns/eCH-0214/2}algorithmType" minOccurs="0"/&gt;
         *         &lt;element name="searchedPerson" type="{http://www.ech.ch/xmlns/eCH-0213-commons/1}personToUPIType"/&gt;
         *       &lt;/sequence&gt;
         *     &lt;/restriction&gt;
         *   &lt;/complexContent&gt;
         * &lt;/complexType&gt;
         * </pre>
         *
         *
         */
        @XmlAccessorType(XmlAccessType.FIELD)
        @XmlType(name = "", propOrder = {
            "searchPersonRequestId",
            "algorithm",
            "searchedPerson"
        })
        public static class SearchPersonRequest {

            @XmlElement(required = true)
            protected BigInteger searchPersonRequestId;
            @XmlJavaTypeAdapter(CollapsedStringAdapter.class)
            @XmlSchemaType(name = "token")
            protected String algorithm;
            @XmlElement(required = true)
            protected PersonToUPIType searchedPerson;

            /**
             * Obtient la valeur de la propriété searchPersonRequestId.
             *
             * @return
             *     possible object is
             *     {@link BigInteger }
             *
             */
            public BigInteger getSearchPersonRequestId() {
                return searchPersonRequestId;
            }

            /**
             * Définit la valeur de la propriété searchPersonRequestId.
             *
             * @param value
             *     allowed object is
             *     {@link BigInteger }
             *
             */
            public void setSearchPersonRequestId(BigInteger value) {
                this.searchPersonRequestId = value;
            }

            /**
             * Obtient la valeur de la propriété algorithm.
             *
             * @return
             *     possible object is
             *     {@link String }
             *
             */
            public String getAlgorithm() {
                return algorithm;
            }

            /**
             * Définit la valeur de la propriété algorithm.
             *
             * @param value
             *     allowed object is
             *     {@link String }
             *
             */
            public void setAlgorithm(String value) {
                this.algorithm = value;
            }

            /**
             * Obtient la valeur de la propriété searchedPerson.
             *
             * @return
             *     possible object is
             *     {@link PersonToUPIType }
             *
             */
            public PersonToUPIType getSearchedPerson() {
                return searchedPerson;
            }

            /**
             * Définit la valeur de la propriété searchedPerson.
             *
             * @param value
             *     allowed object is
             *     {@link PersonToUPIType }
             *
             */
            public void setSearchedPerson(PersonToUPIType value) {
                this.searchedPerson = value;
            }

        }

    }

}
