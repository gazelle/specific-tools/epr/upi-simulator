
package net.ihe.gazelle.upi.model.ech_0021;

import java.math.BigInteger;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlSchemaType;
import javax.xml.bind.annotation.XmlType;
import javax.xml.bind.annotation.adapters.CollapsedStringAdapter;
import javax.xml.bind.annotation.adapters.XmlJavaTypeAdapter;
import net.ihe.gazelle.upi.model.ech_0010.MailAddressType;
import net.ihe.gazelle.upi.model.ech_0011.PartnerIdOrganisationType;
import net.ihe.gazelle.upi.model.ech_0044.PersonIdentificationLightType;
import net.ihe.gazelle.upi.model.ech_0044.PersonIdentificationType;


/**
 * <p>Classe Java pour guardianRelationshipType complex type.
 * 
 * <p>Le fragment de schéma suivant indique le contenu attendu figurant dans cette classe.
 * 
 * <pre>
 * &lt;complexType name="guardianRelationshipType"&gt;
 *   &lt;complexContent&gt;
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *       &lt;sequence&gt;
 *         &lt;element name="guardianRelationshipId" type="{http://www.ech.ch/xmlns/eCH-0021/7}guardianRelationshipIdType"/&gt;
 *         &lt;element name="partner" minOccurs="0"&gt;
 *           &lt;complexType&gt;
 *             &lt;complexContent&gt;
 *               &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *                 &lt;sequence&gt;
 *                   &lt;choice&gt;
 *                     &lt;element name="personIdentification" type="{http://www.ech.ch/xmlns/eCH-0044/4}personIdentificationType"/&gt;
 *                     &lt;element name="personIdentificationPartner" type="{http://www.ech.ch/xmlns/eCH-0044/4}personIdentificationLightType"/&gt;
 *                     &lt;element name="partnerIdOrganisation" type="{http://www.ech.ch/xmlns/eCH-0011/8}partnerIdOrganisationType"/&gt;
 *                   &lt;/choice&gt;
 *                   &lt;element name="address" type="{http://www.ech.ch/xmlns/eCH-0010/5}mailAddressType" minOccurs="0"/&gt;
 *                 &lt;/sequence&gt;
 *               &lt;/restriction&gt;
 *             &lt;/complexContent&gt;
 *           &lt;/complexType&gt;
 *         &lt;/element&gt;
 *         &lt;element name="typeOfRelationship"&gt;
 *           &lt;simpleType&gt;
 *             &lt;restriction base="{http://www.ech.ch/xmlns/eCH-0021/7}typeOfRelationshipType"&gt;
 *               &lt;enumeration value="7"/&gt;
 *               &lt;enumeration value="8"/&gt;
 *               &lt;enumeration value="9"/&gt;
 *               &lt;enumeration value="10"/&gt;
 *             &lt;/restriction&gt;
 *           &lt;/simpleType&gt;
 *         &lt;/element&gt;
 *         &lt;element name="guardianMeasureInfo" type="{http://www.ech.ch/xmlns/eCH-0021/7}guardianMeasureInfoType"/&gt;
 *         &lt;element name="care" type="{http://www.ech.ch/xmlns/eCH-0021/7}careType" minOccurs="0"/&gt;
 *       &lt;/sequence&gt;
 *     &lt;/restriction&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "guardianRelationshipType", propOrder = {
    "guardianRelationshipId",
    "partner",
    "typeOfRelationship",
    "guardianMeasureInfo",
    "care"
})
public class GuardianRelationshipType {

    @XmlElement(required = true)
    @XmlJavaTypeAdapter(CollapsedStringAdapter.class)
    @XmlSchemaType(name = "token")
    protected String guardianRelationshipId;
    protected GuardianRelationshipType.Partner partner;
    @XmlElement(required = true)
    protected String typeOfRelationship;
    @XmlElement(required = true)
    protected GuardianMeasureInfoType guardianMeasureInfo;
    @XmlSchemaType(name = "nonNegativeInteger")
    protected BigInteger care;

    /**
     * Obtient la valeur de la propriété guardianRelationshipId.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getGuardianRelationshipId() {
        return guardianRelationshipId;
    }

    /**
     * Définit la valeur de la propriété guardianRelationshipId.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setGuardianRelationshipId(String value) {
        this.guardianRelationshipId = value;
    }

    /**
     * Obtient la valeur de la propriété partner.
     * 
     * @return
     *     possible object is
     *     {@link GuardianRelationshipType.Partner }
     *     
     */
    public GuardianRelationshipType.Partner getPartner() {
        return partner;
    }

    /**
     * Définit la valeur de la propriété partner.
     * 
     * @param value
     *     allowed object is
     *     {@link GuardianRelationshipType.Partner }
     *     
     */
    public void setPartner(GuardianRelationshipType.Partner value) {
        this.partner = value;
    }

    /**
     * Obtient la valeur de la propriété typeOfRelationship.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getTypeOfRelationship() {
        return typeOfRelationship;
    }

    /**
     * Définit la valeur de la propriété typeOfRelationship.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setTypeOfRelationship(String value) {
        this.typeOfRelationship = value;
    }

    /**
     * Obtient la valeur de la propriété guardianMeasureInfo.
     * 
     * @return
     *     possible object is
     *     {@link GuardianMeasureInfoType }
     *     
     */
    public GuardianMeasureInfoType getGuardianMeasureInfo() {
        return guardianMeasureInfo;
    }

    /**
     * Définit la valeur de la propriété guardianMeasureInfo.
     * 
     * @param value
     *     allowed object is
     *     {@link GuardianMeasureInfoType }
     *     
     */
    public void setGuardianMeasureInfo(GuardianMeasureInfoType value) {
        this.guardianMeasureInfo = value;
    }

    /**
     * Obtient la valeur de la propriété care.
     * 
     * @return
     *     possible object is
     *     {@link BigInteger }
     *     
     */
    public BigInteger getCare() {
        return care;
    }

    /**
     * Définit la valeur de la propriété care.
     * 
     * @param value
     *     allowed object is
     *     {@link BigInteger }
     *     
     */
    public void setCare(BigInteger value) {
        this.care = value;
    }


    /**
     * <p>Classe Java pour anonymous complex type.
     * 
     * <p>Le fragment de schéma suivant indique le contenu attendu figurant dans cette classe.
     * 
     * <pre>
     * &lt;complexType&gt;
     *   &lt;complexContent&gt;
     *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
     *       &lt;sequence&gt;
     *         &lt;choice&gt;
     *           &lt;element name="personIdentification" type="{http://www.ech.ch/xmlns/eCH-0044/4}personIdentificationType"/&gt;
     *           &lt;element name="personIdentificationPartner" type="{http://www.ech.ch/xmlns/eCH-0044/4}personIdentificationLightType"/&gt;
     *           &lt;element name="partnerIdOrganisation" type="{http://www.ech.ch/xmlns/eCH-0011/8}partnerIdOrganisationType"/&gt;
     *         &lt;/choice&gt;
     *         &lt;element name="address" type="{http://www.ech.ch/xmlns/eCH-0010/5}mailAddressType" minOccurs="0"/&gt;
     *       &lt;/sequence&gt;
     *     &lt;/restriction&gt;
     *   &lt;/complexContent&gt;
     * &lt;/complexType&gt;
     * </pre>
     * 
     * 
     */
    @XmlAccessorType(XmlAccessType.FIELD)
    @XmlType(name = "", propOrder = {
        "personIdentification",
        "personIdentificationPartner",
        "partnerIdOrganisation",
        "address"
    })
    public static class Partner {

        protected PersonIdentificationType personIdentification;
        protected PersonIdentificationLightType personIdentificationPartner;
        protected PartnerIdOrganisationType partnerIdOrganisation;
        protected MailAddressType address;

        /**
         * Obtient la valeur de la propriété personIdentification.
         * 
         * @return
         *     possible object is
         *     {@link PersonIdentificationType }
         *     
         */
        public PersonIdentificationType getPersonIdentification() {
            return personIdentification;
        }

        /**
         * Définit la valeur de la propriété personIdentification.
         * 
         * @param value
         *     allowed object is
         *     {@link PersonIdentificationType }
         *     
         */
        public void setPersonIdentification(PersonIdentificationType value) {
            this.personIdentification = value;
        }

        /**
         * Obtient la valeur de la propriété personIdentificationPartner.
         * 
         * @return
         *     possible object is
         *     {@link PersonIdentificationLightType }
         *     
         */
        public PersonIdentificationLightType getPersonIdentificationPartner() {
            return personIdentificationPartner;
        }

        /**
         * Définit la valeur de la propriété personIdentificationPartner.
         * 
         * @param value
         *     allowed object is
         *     {@link PersonIdentificationLightType }
         *     
         */
        public void setPersonIdentificationPartner(PersonIdentificationLightType value) {
            this.personIdentificationPartner = value;
        }

        /**
         * Obtient la valeur de la propriété partnerIdOrganisation.
         * 
         * @return
         *     possible object is
         *     {@link PartnerIdOrganisationType }
         *     
         */
        public PartnerIdOrganisationType getPartnerIdOrganisation() {
            return partnerIdOrganisation;
        }

        /**
         * Définit la valeur de la propriété partnerIdOrganisation.
         * 
         * @param value
         *     allowed object is
         *     {@link PartnerIdOrganisationType }
         *     
         */
        public void setPartnerIdOrganisation(PartnerIdOrganisationType value) {
            this.partnerIdOrganisation = value;
        }

        /**
         * Obtient la valeur de la propriété address.
         * 
         * @return
         *     possible object is
         *     {@link MailAddressType }
         *     
         */
        public MailAddressType getAddress() {
            return address;
        }

        /**
         * Définit la valeur de la propriété address.
         * 
         * @param value
         *     allowed object is
         *     {@link MailAddressType }
         *     
         */
        public void setAddress(MailAddressType value) {
            this.address = value;
        }

    }

}
