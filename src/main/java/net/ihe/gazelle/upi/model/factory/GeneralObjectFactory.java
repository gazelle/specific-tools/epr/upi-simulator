package net.ihe.gazelle.upi.model.factory;


public class GeneralObjectFactory {

    public net.ihe.gazelle.upi.model.ech_0006.ObjectFactory createECH006Object() {
        return new net.ihe.gazelle.upi.model.ech_0006.ObjectFactory();
    }

    public net.ihe.gazelle.upi.model.ech_0007.ObjectFactory createECH007Object() {
        return new net.ihe.gazelle.upi.model.ech_0007.ObjectFactory();
    }

    public net.ihe.gazelle.upi.model.ech_0008.ObjectFactory createECH008Object() {
        return new net.ihe.gazelle.upi.model.ech_0008.ObjectFactory();
    }

    public net.ihe.gazelle.upi.model.ech_0010.ObjectFactory createECH0010Object() {
        return new net.ihe.gazelle.upi.model.ech_0010.ObjectFactory();
    }

    public net.ihe.gazelle.upi.model.ech_0011.ObjectFactory createECH0011Object() {
        return new net.ihe.gazelle.upi.model.ech_0011.ObjectFactory();
    }

    public net.ihe.gazelle.upi.model.ech_0011.ObjectFactory createECH0021Object() {
        return new net.ihe.gazelle.upi.model.ech_0011.ObjectFactory();
    }

    public net.ihe.gazelle.upi.model.ech_0044.ObjectFactory createECH0044Object() {
        return new net.ihe.gazelle.upi.model.ech_0044.ObjectFactory();
    }

    public net.ihe.gazelle.upi.model.ech_0058.ObjectFactory createECH0058Object() {
        return new net.ihe.gazelle.upi.model.ech_0058.ObjectFactory();
    }

    public net.ihe.gazelle.upi.model.ech_0135.ObjectFactory createECH0135Object() {
        return new net.ihe.gazelle.upi.model.ech_0135.ObjectFactory();
    }

    public net.ihe.gazelle.upi.model.ech_0213.ObjectFactory createECH0213Object() {
        return new net.ihe.gazelle.upi.model.ech_0213.ObjectFactory();
    }

    public net.ihe.gazelle.upi.model.ech_0213_commons.ObjectFactory createECH0213CommonsObject() {
        return new net.ihe.gazelle.upi.model.ech_0213_commons.ObjectFactory();
    }

    public net.ihe.gazelle.upi.model.ech_0214.ObjectFactory createECH0214Object() {
        return new net.ihe.gazelle.upi.model.ech_0214.ObjectFactory();
    }


}
