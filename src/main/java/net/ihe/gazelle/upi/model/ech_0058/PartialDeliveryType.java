
package net.ihe.gazelle.upi.model.ech_0058;

import javax.xml.bind.annotation.*;
import javax.xml.bind.annotation.adapters.CollapsedStringAdapter;
import javax.xml.bind.annotation.adapters.XmlJavaTypeAdapter;


/**
 * <p>Classe Java pour partialDeliveryType complex type.
 * 
 * <p>Le fragment de schéma suivant indique le contenu attendu figurant dans cette classe.
 * 
 * <pre>
 * &lt;complexType name="partialDeliveryType"&gt;
 *   &lt;complexContent&gt;
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *       &lt;sequence&gt;
 *         &lt;element name="uniqueIdDelivery" type="{http://www.ech.ch/xmlns/eCH-0058/5}uniqueIdDeliveryType"/&gt;
 *         &lt;element name="totalNumberOfPackages" type="{http://www.ech.ch/xmlns/eCH-0058/5}totalNumberOfPackagesType"/&gt;
 *         &lt;element name="numberOfActualPackage" type="{http://www.ech.ch/xmlns/eCH-0058/5}numberOfActualPackageType"/&gt;
 *       &lt;/sequence&gt;
 *     &lt;/restriction&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "partialDeliveryType", propOrder = {
    "uniqueIdDelivery",
    "totalNumberOfPackages",
    "numberOfActualPackage"
})
public class PartialDeliveryType {

    @XmlElement(required = true)
    @XmlJavaTypeAdapter(CollapsedStringAdapter.class)
    @XmlSchemaType(name = "token")
    protected String uniqueIdDelivery;
    @XmlSchemaType(name = "nonNegativeInteger")
    protected int totalNumberOfPackages;
    @XmlSchemaType(name = "nonNegativeInteger")
    protected int numberOfActualPackage;

    /**
     * Obtient la valeur de la propriété uniqueIdDelivery.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getUniqueIdDelivery() {
        return uniqueIdDelivery;
    }

    /**
     * Définit la valeur de la propriété uniqueIdDelivery.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setUniqueIdDelivery(String value) {
        this.uniqueIdDelivery = value;
    }

    /**
     * Obtient la valeur de la propriété totalNumberOfPackages.
     * 
     */
    public int getTotalNumberOfPackages() {
        return totalNumberOfPackages;
    }

    /**
     * Définit la valeur de la propriété totalNumberOfPackages.
     * 
     */
    public void setTotalNumberOfPackages(int value) {
        this.totalNumberOfPackages = value;
    }

    /**
     * Obtient la valeur de la propriété numberOfActualPackage.
     * 
     */
    public int getNumberOfActualPackage() {
        return numberOfActualPackage;
    }

    /**
     * Définit la valeur de la propriété numberOfActualPackage.
     * 
     */
    public void setNumberOfActualPackage(int value) {
        this.numberOfActualPackage = value;
    }

}
