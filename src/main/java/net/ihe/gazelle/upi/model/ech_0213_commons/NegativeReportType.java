
package net.ihe.gazelle.upi.model.ech_0213_commons;


import javax.xml.bind.annotation.*;
import javax.xml.bind.annotation.adapters.CollapsedStringAdapter;
import javax.xml.bind.annotation.adapters.XmlJavaTypeAdapter;


/**
 * <p>Classe Java pour negativeReportType complex type.
 * 
 * <p>Le fragment de schéma suivant indique le contenu attendu figurant dans cette classe.
 * 
 * <pre>
 * &lt;complexType name="negativeReportType"&gt;
 *   &lt;complexContent&gt;
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *       &lt;sequence&gt;
 *         &lt;element name="notice"&gt;
 *           &lt;complexType&gt;
 *             &lt;complexContent&gt;
 *               &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *                 &lt;sequence&gt;
 *                   &lt;element name="code" type="{http://www.ech.ch/xmlns/eCH-0213-commons/1}codeType"/&gt;
 *                   &lt;sequence minOccurs="0"&gt;
 *                     &lt;element name="descriptionLanguage" type="{http://www.ech.ch/xmlns/eCH-0011/8}languageType"/&gt;
 *                     &lt;element name="codeDescription" type="{http://www.ech.ch/xmlns/eCH-0213-commons/1}codeDescriptionType"/&gt;
 *                   &lt;/sequence&gt;
 *                   &lt;element name="comment" type="{http://www.ech.ch/xmlns/eCH-0213-commons/1}commentType" minOccurs="0"/&gt;
 *                 &lt;/sequence&gt;
 *               &lt;/restriction&gt;
 *             &lt;/complexContent&gt;
 *           &lt;/complexType&gt;
 *         &lt;/element&gt;
 *         &lt;element name="data" type="{http://www.w3.org/2001/XMLSchema}anyType"/&gt;
 *       &lt;/sequence&gt;
 *     &lt;/restriction&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "negativeReportType", propOrder = {
    "notice",
    "data"
})
public class NegativeReportType {

    @XmlElement(required = true)
    protected NegativeReportType.Notice notice;
    @XmlElement(required = true)
    protected Object data;

    /**
     * Obtient la valeur de la propriété notice.
     * 
     * @return
     *     possible object is
     *     {@link NegativeReportType.Notice }
     *     
     */
    public NegativeReportType.Notice getNotice() {
        return notice;
    }

    /**
     * Définit la valeur de la propriété notice.
     * 
     * @param value
     *     allowed object is
     *     {@link NegativeReportType.Notice }
     *     
     */
    public void setNotice(NegativeReportType.Notice value) {
        this.notice = value;
    }

    /**
     * Obtient la valeur de la propriété data.
     * 
     * @return
     *     possible object is
     *     {@link Object }
     *     
     */
    public Object getData() {
        return data;
    }

    /**
     * Définit la valeur de la propriété data.
     * 
     * @param value
     *     allowed object is
     *     {@link Object }
     *     
     */
    public void setData(Object value) {
        this.data = value;
    }


    /**
     * <p>Classe Java pour anonymous complex type.
     * 
     * <p>Le fragment de schéma suivant indique le contenu attendu figurant dans cette classe.
     * 
     * <pre>
     * &lt;complexType&gt;
     *   &lt;complexContent&gt;
     *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
     *       &lt;sequence&gt;
     *         &lt;element name="code" type="{http://www.ech.ch/xmlns/eCH-0213-commons/1}codeType"/&gt;
     *         &lt;sequence minOccurs="0"&gt;
     *           &lt;element name="descriptionLanguage" type="{http://www.ech.ch/xmlns/eCH-0011/8}languageType"/&gt;
     *           &lt;element name="codeDescription" type="{http://www.ech.ch/xmlns/eCH-0213-commons/1}codeDescriptionType"/&gt;
     *         &lt;/sequence&gt;
     *         &lt;element name="comment" type="{http://www.ech.ch/xmlns/eCH-0213-commons/1}commentType" minOccurs="0"/&gt;
     *       &lt;/sequence&gt;
     *     &lt;/restriction&gt;
     *   &lt;/complexContent&gt;
     * &lt;/complexType&gt;
     * </pre>
     * 
     * 
     */
    @XmlAccessorType(XmlAccessType.FIELD)
    @XmlType(name = "", propOrder = {
        "code",
        "descriptionLanguage",
        "codeDescription",
        "comment"
    })
    public static class Notice {

        protected int code;
        @XmlJavaTypeAdapter(CollapsedStringAdapter.class)
        @XmlSchemaType(name = "token")
        protected String descriptionLanguage;
        @XmlJavaTypeAdapter(CollapsedStringAdapter.class)
        @XmlSchemaType(name = "token")
        protected String codeDescription;
        protected String comment;

        /**
         * Obtient la valeur de la propriété code.
         * 
         */
        public int getCode() {
            return code;
        }

        /**
         * Définit la valeur de la propriété code.
         * 
         */
        public void setCode(int value) {
            this.code = value;
        }

        /**
         * Obtient la valeur de la propriété descriptionLanguage.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getDescriptionLanguage() {
            return descriptionLanguage;
        }

        /**
         * Définit la valeur de la propriété descriptionLanguage.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setDescriptionLanguage(String value) {
            this.descriptionLanguage = value;
        }

        /**
         * Obtient la valeur de la propriété codeDescription.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getCodeDescription() {
            return codeDescription;
        }

        /**
         * Définit la valeur de la propriété codeDescription.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setCodeDescription(String value) {
            this.codeDescription = value;
        }

        /**
         * Obtient la valeur de la propriété comment.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getComment() {
            return comment;
        }

        /**
         * Définit la valeur de la propriété comment.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setComment(String value) {
            this.comment = value;
        }

    }

}
