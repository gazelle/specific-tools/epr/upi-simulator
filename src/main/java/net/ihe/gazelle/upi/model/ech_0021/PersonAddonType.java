
package net.ihe.gazelle.upi.model.ech_0021;

import java.util.ArrayList;
import java.util.List;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;
import net.ihe.gazelle.upi.model.ech_0044.PersonIdentificationType;


/**
 * <p>Classe Java pour personAddonType complex type.
 * 
 * <p>Le fragment de schéma suivant indique le contenu attendu figurant dans cette classe.
 * 
 * <pre>
 * &lt;complexType name="personAddonType"&gt;
 *   &lt;complexContent&gt;
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *       &lt;sequence&gt;
 *         &lt;element name="personidentification" type="{http://www.ech.ch/xmlns/eCH-0044/4}personIdentificationType"/&gt;
 *         &lt;element name="personAdditionalData" type="{http://www.ech.ch/xmlns/eCH-0021/7}personAdditionalData" minOccurs="0"/&gt;
 *         &lt;element name="politicalRightData" type="{http://www.ech.ch/xmlns/eCH-0021/7}politicalRightDataType" minOccurs="0"/&gt;
 *         &lt;element name="birthAddonData" type="{http://www.ech.ch/xmlns/eCH-0021/7}birthAddonDataType" minOccurs="0"/&gt;
 *         &lt;element name="lockData" type="{http://www.ech.ch/xmlns/eCH-0021/7}lockDataType" minOccurs="0"/&gt;
 *         &lt;element name="placeOfOriginAddonData" type="{http://www.ech.ch/xmlns/eCH-0021/7}placeOfOriginAddonDataType" maxOccurs="unbounded" minOccurs="0"/&gt;
 *         &lt;element name="jobData" type="{http://www.ech.ch/xmlns/eCH-0021/7}jobDataType" minOccurs="0"/&gt;
 *         &lt;element name="maritalRelationship" type="{http://www.ech.ch/xmlns/eCH-0021/7}maritalRelationshipType" minOccurs="0"/&gt;
 *         &lt;element name="parentalRelationship" type="{http://www.ech.ch/xmlns/eCH-0021/7}parentalRelationshipType" maxOccurs="unbounded" minOccurs="0"/&gt;
 *         &lt;element name="guardianRelationship" type="{http://www.ech.ch/xmlns/eCH-0021/7}guardianRelationshipType" maxOccurs="unbounded" minOccurs="0"/&gt;
 *         &lt;element name="armedForcesData" type="{http://www.ech.ch/xmlns/eCH-0021/7}armedForcesDataType" minOccurs="0"/&gt;
 *         &lt;element name="civilDefenseData" type="{http://www.ech.ch/xmlns/eCH-0021/7}civilDefenseDataType" minOccurs="0"/&gt;
 *         &lt;element name="fireServiceData" type="{http://www.ech.ch/xmlns/eCH-0021/7}fireServiceDataType" minOccurs="0"/&gt;
 *         &lt;element name="healthInsuranceData" type="{http://www.ech.ch/xmlns/eCH-0021/7}healthInsuranceDataType" minOccurs="0"/&gt;
 *         &lt;element name="matrimonialInheritanceArrangementData" type="{http://www.ech.ch/xmlns/eCH-0021/7}matrimonialInheritanceArrangementDataType" minOccurs="0"/&gt;
 *       &lt;/sequence&gt;
 *     &lt;/restriction&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "personAddonType", propOrder = {
    "personidentification",
    "personAdditionalData",
    "politicalRightData",
    "birthAddonData",
    "lockData",
    "placeOfOriginAddonData",
    "jobData",
    "maritalRelationship",
    "parentalRelationship",
    "guardianRelationship",
    "armedForcesData",
    "civilDefenseData",
    "fireServiceData",
    "healthInsuranceData",
    "matrimonialInheritanceArrangementData"
})
public class PersonAddonType {

    @XmlElement(required = true)
    protected PersonIdentificationType personidentification;
    protected PersonAdditionalData personAdditionalData;
    protected PoliticalRightDataType politicalRightData;
    protected BirthAddonDataType birthAddonData;
    protected LockDataType lockData;
    protected List<PlaceOfOriginAddonDataType> placeOfOriginAddonData;
    protected JobDataType jobData;
    protected MaritalRelationshipType maritalRelationship;
    protected List<ParentalRelationshipType> parentalRelationship;
    protected List<GuardianRelationshipType> guardianRelationship;
    protected ArmedForcesDataType armedForcesData;
    protected CivilDefenseDataType civilDefenseData;
    protected FireServiceDataType fireServiceData;
    protected HealthInsuranceDataType healthInsuranceData;
    protected MatrimonialInheritanceArrangementDataType matrimonialInheritanceArrangementData;

    /**
     * Obtient la valeur de la propriété personidentification.
     * 
     * @return
     *     possible object is
     *     {@link PersonIdentificationType }
     *     
     */
    public PersonIdentificationType getPersonidentification() {
        return personidentification;
    }

    /**
     * Définit la valeur de la propriété personidentification.
     * 
     * @param value
     *     allowed object is
     *     {@link PersonIdentificationType }
     *     
     */
    public void setPersonidentification(PersonIdentificationType value) {
        this.personidentification = value;
    }

    /**
     * Obtient la valeur de la propriété personAdditionalData.
     * 
     * @return
     *     possible object is
     *     {@link PersonAdditionalData }
     *     
     */
    public PersonAdditionalData getPersonAdditionalData() {
        return personAdditionalData;
    }

    /**
     * Définit la valeur de la propriété personAdditionalData.
     * 
     * @param value
     *     allowed object is
     *     {@link PersonAdditionalData }
     *     
     */
    public void setPersonAdditionalData(PersonAdditionalData value) {
        this.personAdditionalData = value;
    }

    /**
     * Obtient la valeur de la propriété politicalRightData.
     * 
     * @return
     *     possible object is
     *     {@link PoliticalRightDataType }
     *     
     */
    public PoliticalRightDataType getPoliticalRightData() {
        return politicalRightData;
    }

    /**
     * Définit la valeur de la propriété politicalRightData.
     * 
     * @param value
     *     allowed object is
     *     {@link PoliticalRightDataType }
     *     
     */
    public void setPoliticalRightData(PoliticalRightDataType value) {
        this.politicalRightData = value;
    }

    /**
     * Obtient la valeur de la propriété birthAddonData.
     * 
     * @return
     *     possible object is
     *     {@link BirthAddonDataType }
     *     
     */
    public BirthAddonDataType getBirthAddonData() {
        return birthAddonData;
    }

    /**
     * Définit la valeur de la propriété birthAddonData.
     * 
     * @param value
     *     allowed object is
     *     {@link BirthAddonDataType }
     *     
     */
    public void setBirthAddonData(BirthAddonDataType value) {
        this.birthAddonData = value;
    }

    /**
     * Obtient la valeur de la propriété lockData.
     * 
     * @return
     *     possible object is
     *     {@link LockDataType }
     *     
     */
    public LockDataType getLockData() {
        return lockData;
    }

    /**
     * Définit la valeur de la propriété lockData.
     * 
     * @param value
     *     allowed object is
     *     {@link LockDataType }
     *     
     */
    public void setLockData(LockDataType value) {
        this.lockData = value;
    }

    /**
     * Gets the value of the placeOfOriginAddonData property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the placeOfOriginAddonData property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getPlaceOfOriginAddonData().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link PlaceOfOriginAddonDataType }
     * 
     * 
     */
    public List<PlaceOfOriginAddonDataType> getPlaceOfOriginAddonData() {
        if (placeOfOriginAddonData == null) {
            placeOfOriginAddonData = new ArrayList<PlaceOfOriginAddonDataType>();
        }
        return this.placeOfOriginAddonData;
    }

    /**
     * Obtient la valeur de la propriété jobData.
     * 
     * @return
     *     possible object is
     *     {@link JobDataType }
     *     
     */
    public JobDataType getJobData() {
        return jobData;
    }

    /**
     * Définit la valeur de la propriété jobData.
     * 
     * @param value
     *     allowed object is
     *     {@link JobDataType }
     *     
     */
    public void setJobData(JobDataType value) {
        this.jobData = value;
    }

    /**
     * Obtient la valeur de la propriété maritalRelationship.
     * 
     * @return
     *     possible object is
     *     {@link MaritalRelationshipType }
     *     
     */
    public MaritalRelationshipType getMaritalRelationship() {
        return maritalRelationship;
    }

    /**
     * Définit la valeur de la propriété maritalRelationship.
     * 
     * @param value
     *     allowed object is
     *     {@link MaritalRelationshipType }
     *     
     */
    public void setMaritalRelationship(MaritalRelationshipType value) {
        this.maritalRelationship = value;
    }

    /**
     * Gets the value of the parentalRelationship property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the parentalRelationship property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getParentalRelationship().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link ParentalRelationshipType }
     * 
     * 
     */
    public List<ParentalRelationshipType> getParentalRelationship() {
        if (parentalRelationship == null) {
            parentalRelationship = new ArrayList<ParentalRelationshipType>();
        }
        return this.parentalRelationship;
    }

    /**
     * Gets the value of the guardianRelationship property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the guardianRelationship property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getGuardianRelationship().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link GuardianRelationshipType }
     * 
     * 
     */
    public List<GuardianRelationshipType> getGuardianRelationship() {
        if (guardianRelationship == null) {
            guardianRelationship = new ArrayList<GuardianRelationshipType>();
        }
        return this.guardianRelationship;
    }

    /**
     * Obtient la valeur de la propriété armedForcesData.
     * 
     * @return
     *     possible object is
     *     {@link ArmedForcesDataType }
     *     
     */
    public ArmedForcesDataType getArmedForcesData() {
        return armedForcesData;
    }

    /**
     * Définit la valeur de la propriété armedForcesData.
     * 
     * @param value
     *     allowed object is
     *     {@link ArmedForcesDataType }
     *     
     */
    public void setArmedForcesData(ArmedForcesDataType value) {
        this.armedForcesData = value;
    }

    /**
     * Obtient la valeur de la propriété civilDefenseData.
     * 
     * @return
     *     possible object is
     *     {@link CivilDefenseDataType }
     *     
     */
    public CivilDefenseDataType getCivilDefenseData() {
        return civilDefenseData;
    }

    /**
     * Définit la valeur de la propriété civilDefenseData.
     * 
     * @param value
     *     allowed object is
     *     {@link CivilDefenseDataType }
     *     
     */
    public void setCivilDefenseData(CivilDefenseDataType value) {
        this.civilDefenseData = value;
    }

    /**
     * Obtient la valeur de la propriété fireServiceData.
     * 
     * @return
     *     possible object is
     *     {@link FireServiceDataType }
     *     
     */
    public FireServiceDataType getFireServiceData() {
        return fireServiceData;
    }

    /**
     * Définit la valeur de la propriété fireServiceData.
     * 
     * @param value
     *     allowed object is
     *     {@link FireServiceDataType }
     *     
     */
    public void setFireServiceData(FireServiceDataType value) {
        this.fireServiceData = value;
    }

    /**
     * Obtient la valeur de la propriété healthInsuranceData.
     * 
     * @return
     *     possible object is
     *     {@link HealthInsuranceDataType }
     *     
     */
    public HealthInsuranceDataType getHealthInsuranceData() {
        return healthInsuranceData;
    }

    /**
     * Définit la valeur de la propriété healthInsuranceData.
     * 
     * @param value
     *     allowed object is
     *     {@link HealthInsuranceDataType }
     *     
     */
    public void setHealthInsuranceData(HealthInsuranceDataType value) {
        this.healthInsuranceData = value;
    }

    /**
     * Obtient la valeur de la propriété matrimonialInheritanceArrangementData.
     * 
     * @return
     *     possible object is
     *     {@link MatrimonialInheritanceArrangementDataType }
     *     
     */
    public MatrimonialInheritanceArrangementDataType getMatrimonialInheritanceArrangementData() {
        return matrimonialInheritanceArrangementData;
    }

    /**
     * Définit la valeur de la propriété matrimonialInheritanceArrangementData.
     * 
     * @param value
     *     allowed object is
     *     {@link MatrimonialInheritanceArrangementDataType }
     *     
     */
    public void setMatrimonialInheritanceArrangementData(MatrimonialInheritanceArrangementDataType value) {
        this.matrimonialInheritanceArrangementData = value;
    }

}
