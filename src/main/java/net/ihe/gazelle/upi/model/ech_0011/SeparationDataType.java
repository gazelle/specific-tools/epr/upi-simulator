
package net.ihe.gazelle.upi.model.ech_0011;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlSchemaType;
import javax.xml.bind.annotation.XmlType;
import javax.xml.datatype.XMLGregorianCalendar;


/**
 * <p>Classe Java pour separationDataType complex type.
 * 
 * <p>Le fragment de schéma suivant indique le contenu attendu figurant dans cette classe.
 * 
 * <pre>
 * &lt;complexType name="separationDataType"&gt;
 *   &lt;complexContent&gt;
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *       &lt;sequence&gt;
 *         &lt;element name="separation" type="{http://www.ech.ch/xmlns/eCH-0011/8}separationType" minOccurs="0"/&gt;
 *         &lt;element name="separationValidFrom" type="{http://www.w3.org/2001/XMLSchema}date" minOccurs="0"/&gt;
 *         &lt;element name="separationValidTill" type="{http://www.w3.org/2001/XMLSchema}date" minOccurs="0"/&gt;
 *       &lt;/sequence&gt;
 *     &lt;/restriction&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "separationDataType", propOrder = {
    "separation",
    "separationValidFrom",
    "separationValidTill"
})
public class SeparationDataType {

    protected String separation;
    @XmlSchemaType(name = "date")
    protected XMLGregorianCalendar separationValidFrom;
    @XmlSchemaType(name = "date")
    protected XMLGregorianCalendar separationValidTill;

    /**
     * Obtient la valeur de la propriété separation.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getSeparation() {
        return separation;
    }

    /**
     * Définit la valeur de la propriété separation.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setSeparation(String value) {
        this.separation = value;
    }

    /**
     * Obtient la valeur de la propriété separationValidFrom.
     * 
     * @return
     *     possible object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public XMLGregorianCalendar getSeparationValidFrom() {
        return separationValidFrom;
    }

    /**
     * Définit la valeur de la propriété separationValidFrom.
     * 
     * @param value
     *     allowed object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public void setSeparationValidFrom(XMLGregorianCalendar value) {
        this.separationValidFrom = value;
    }

    /**
     * Obtient la valeur de la propriété separationValidTill.
     * 
     * @return
     *     possible object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public XMLGregorianCalendar getSeparationValidTill() {
        return separationValidTill;
    }

    /**
     * Définit la valeur de la propriété separationValidTill.
     * 
     * @param value
     *     allowed object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public void setSeparationValidTill(XMLGregorianCalendar value) {
        this.separationValidTill = value;
    }

}
