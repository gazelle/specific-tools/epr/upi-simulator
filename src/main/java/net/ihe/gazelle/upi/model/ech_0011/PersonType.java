
package net.ihe.gazelle.upi.model.ech_0011;

import java.util.ArrayList;
import java.util.List;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlSchemaType;
import javax.xml.bind.annotation.XmlType;
import javax.xml.bind.annotation.adapters.CollapsedStringAdapter;
import javax.xml.bind.annotation.adapters.XmlJavaTypeAdapter;
import net.ihe.gazelle.upi.model.ech_0044.PersonIdentificationType;


/**
 * <p>Classe Java pour personType complex type.
 * 
 * <p>Le fragment de schéma suivant indique le contenu attendu figurant dans cette classe.
 * 
 * <pre>
 * &lt;complexType name="personType"&gt;
 *   &lt;complexContent&gt;
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *       &lt;sequence&gt;
 *         &lt;element name="personIdentification" type="{http://www.ech.ch/xmlns/eCH-0044/4}personIdentificationType"/&gt;
 *         &lt;element name="nameData" type="{http://www.ech.ch/xmlns/eCH-0011/8}nameDataType"/&gt;
 *         &lt;element name="birthData" type="{http://www.ech.ch/xmlns/eCH-0011/8}birthDataType"/&gt;
 *         &lt;element name="religionData" type="{http://www.ech.ch/xmlns/eCH-0011/8}religionDataType"/&gt;
 *         &lt;element name="maritalData" type="{http://www.ech.ch/xmlns/eCH-0011/8}maritalDataType"/&gt;
 *         &lt;element name="nationalityData" type="{http://www.ech.ch/xmlns/eCH-0011/8}nationalityDataType"/&gt;
 *         &lt;element name="deathData" type="{http://www.ech.ch/xmlns/eCH-0011/8}deathDataType" minOccurs="0"/&gt;
 *         &lt;element name="contactData" type="{http://www.ech.ch/xmlns/eCH-0011/8}contactDataType" minOccurs="0"/&gt;
 *         &lt;element name="languageOfCorrespondance" type="{http://www.ech.ch/xmlns/eCH-0011/8}languageType" minOccurs="0"/&gt;
 *         &lt;element name="restrictedVotingAndElectionRightFederation" type="{http://www.w3.org/2001/XMLSchema}boolean" minOccurs="0"/&gt;
 *         &lt;choice&gt;
 *           &lt;element name="placeOfOrigin" type="{http://www.ech.ch/xmlns/eCH-0011/8}placeOfOriginType" maxOccurs="unbounded"/&gt;
 *           &lt;element name="residencePermit" type="{http://www.ech.ch/xmlns/eCH-0011/8}residencePermitDataType"/&gt;
 *         &lt;/choice&gt;
 *       &lt;/sequence&gt;
 *     &lt;/restriction&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "personType", propOrder = {
    "personIdentification",
    "nameData",
    "birthData",
    "religionData",
    "maritalData",
    "nationalityData",
    "deathData",
    "contactData",
    "languageOfCorrespondance",
    "restrictedVotingAndElectionRightFederation",
    "placeOfOrigin",
    "residencePermit"
})
public class PersonType {

    @XmlElement(required = true)
    protected PersonIdentificationType personIdentification;
    @XmlElement(required = true)
    protected NameDataType nameData;
    @XmlElement(required = true)
    protected BirthDataType birthData;
    @XmlElement(required = true)
    protected ReligionDataType religionData;
    @XmlElement(required = true)
    protected MaritalDataType maritalData;
    @XmlElement(required = true)
    protected NationalityDataType nationalityData;
    protected DeathDataType deathData;
    protected ContactDataType contactData;
    @XmlJavaTypeAdapter(CollapsedStringAdapter.class)
    @XmlSchemaType(name = "token")
    protected String languageOfCorrespondance;
    protected Boolean restrictedVotingAndElectionRightFederation;
    protected List<PlaceOfOriginType> placeOfOrigin;
    protected ResidencePermitDataType residencePermit;

    /**
     * Obtient la valeur de la propriété personIdentification.
     * 
     * @return
     *     possible object is
     *     {@link PersonIdentificationType }
     *     
     */
    public PersonIdentificationType getPersonIdentification() {
        return personIdentification;
    }

    /**
     * Définit la valeur de la propriété personIdentification.
     * 
     * @param value
     *     allowed object is
     *     {@link PersonIdentificationType }
     *     
     */
    public void setPersonIdentification(PersonIdentificationType value) {
        this.personIdentification = value;
    }

    /**
     * Obtient la valeur de la propriété nameData.
     * 
     * @return
     *     possible object is
     *     {@link NameDataType }
     *     
     */
    public NameDataType getNameData() {
        return nameData;
    }

    /**
     * Définit la valeur de la propriété nameData.
     * 
     * @param value
     *     allowed object is
     *     {@link NameDataType }
     *     
     */
    public void setNameData(NameDataType value) {
        this.nameData = value;
    }

    /**
     * Obtient la valeur de la propriété birthData.
     * 
     * @return
     *     possible object is
     *     {@link BirthDataType }
     *     
     */
    public BirthDataType getBirthData() {
        return birthData;
    }

    /**
     * Définit la valeur de la propriété birthData.
     * 
     * @param value
     *     allowed object is
     *     {@link BirthDataType }
     *     
     */
    public void setBirthData(BirthDataType value) {
        this.birthData = value;
    }

    /**
     * Obtient la valeur de la propriété religionData.
     * 
     * @return
     *     possible object is
     *     {@link ReligionDataType }
     *     
     */
    public ReligionDataType getReligionData() {
        return religionData;
    }

    /**
     * Définit la valeur de la propriété religionData.
     * 
     * @param value
     *     allowed object is
     *     {@link ReligionDataType }
     *     
     */
    public void setReligionData(ReligionDataType value) {
        this.religionData = value;
    }

    /**
     * Obtient la valeur de la propriété maritalData.
     * 
     * @return
     *     possible object is
     *     {@link MaritalDataType }
     *     
     */
    public MaritalDataType getMaritalData() {
        return maritalData;
    }

    /**
     * Définit la valeur de la propriété maritalData.
     * 
     * @param value
     *     allowed object is
     *     {@link MaritalDataType }
     *     
     */
    public void setMaritalData(MaritalDataType value) {
        this.maritalData = value;
    }

    /**
     * Obtient la valeur de la propriété nationalityData.
     * 
     * @return
     *     possible object is
     *     {@link NationalityDataType }
     *     
     */
    public NationalityDataType getNationalityData() {
        return nationalityData;
    }

    /**
     * Définit la valeur de la propriété nationalityData.
     * 
     * @param value
     *     allowed object is
     *     {@link NationalityDataType }
     *     
     */
    public void setNationalityData(NationalityDataType value) {
        this.nationalityData = value;
    }

    /**
     * Obtient la valeur de la propriété deathData.
     * 
     * @return
     *     possible object is
     *     {@link DeathDataType }
     *     
     */
    public DeathDataType getDeathData() {
        return deathData;
    }

    /**
     * Définit la valeur de la propriété deathData.
     * 
     * @param value
     *     allowed object is
     *     {@link DeathDataType }
     *     
     */
    public void setDeathData(DeathDataType value) {
        this.deathData = value;
    }

    /**
     * Obtient la valeur de la propriété contactData.
     * 
     * @return
     *     possible object is
     *     {@link ContactDataType }
     *     
     */
    public ContactDataType getContactData() {
        return contactData;
    }

    /**
     * Définit la valeur de la propriété contactData.
     * 
     * @param value
     *     allowed object is
     *     {@link ContactDataType }
     *     
     */
    public void setContactData(ContactDataType value) {
        this.contactData = value;
    }

    /**
     * Obtient la valeur de la propriété languageOfCorrespondance.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getLanguageOfCorrespondance() {
        return languageOfCorrespondance;
    }

    /**
     * Définit la valeur de la propriété languageOfCorrespondance.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setLanguageOfCorrespondance(String value) {
        this.languageOfCorrespondance = value;
    }

    /**
     * Obtient la valeur de la propriété restrictedVotingAndElectionRightFederation.
     * 
     * @return
     *     possible object is
     *     {@link Boolean }
     *     
     */
    public Boolean isRestrictedVotingAndElectionRightFederation() {
        return restrictedVotingAndElectionRightFederation;
    }

    /**
     * Définit la valeur de la propriété restrictedVotingAndElectionRightFederation.
     * 
     * @param value
     *     allowed object is
     *     {@link Boolean }
     *     
     */
    public void setRestrictedVotingAndElectionRightFederation(Boolean value) {
        this.restrictedVotingAndElectionRightFederation = value;
    }

    /**
     * Gets the value of the placeOfOrigin property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the placeOfOrigin property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getPlaceOfOrigin().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link PlaceOfOriginType }
     * 
     * 
     */
    public List<PlaceOfOriginType> getPlaceOfOrigin() {
        if (placeOfOrigin == null) {
            placeOfOrigin = new ArrayList<PlaceOfOriginType>();
        }
        return this.placeOfOrigin;
    }

    /**
     * Obtient la valeur de la propriété residencePermit.
     * 
     * @return
     *     possible object is
     *     {@link ResidencePermitDataType }
     *     
     */
    public ResidencePermitDataType getResidencePermit() {
        return residencePermit;
    }

    /**
     * Définit la valeur de la propriété residencePermit.
     * 
     * @param value
     *     allowed object is
     *     {@link ResidencePermitDataType }
     *     
     */
    public void setResidencePermit(ResidencePermitDataType value) {
        this.residencePermit = value;
    }

}
