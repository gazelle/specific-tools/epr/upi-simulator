
package net.ihe.gazelle.upi.model.ech_0011;

import javax.xml.bind.annotation.*;
import javax.xml.bind.annotation.adapters.CollapsedStringAdapter;
import javax.xml.bind.annotation.adapters.XmlJavaTypeAdapter;


/**
 * <p>Classe Java pour nameDataType complex type.
 * 
 * <p>Le fragment de schéma suivant indique le contenu attendu figurant dans cette classe.
 * 
 * <pre>
 * &lt;complexType name="nameDataType"&gt;
 *   &lt;complexContent&gt;
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *       &lt;sequence&gt;
 *         &lt;element name="officialName" type="{http://www.ech.ch/xmlns/eCH-0044/4}baseNameType"/&gt;
 *         &lt;element name="firstName" type="{http://www.ech.ch/xmlns/eCH-0044/4}baseNameType"/&gt;
 *         &lt;element name="originalName" type="{http://www.ech.ch/xmlns/eCH-0044/4}baseNameType" minOccurs="0"/&gt;
 *         &lt;element name="allianceName" type="{http://www.ech.ch/xmlns/eCH-0044/4}baseNameType" minOccurs="0"/&gt;
 *         &lt;element name="aliasName" type="{http://www.ech.ch/xmlns/eCH-0044/4}baseNameType" minOccurs="0"/&gt;
 *         &lt;element name="otherName" type="{http://www.ech.ch/xmlns/eCH-0044/4}baseNameType" minOccurs="0"/&gt;
 *         &lt;element name="callName" type="{http://www.ech.ch/xmlns/eCH-0044/4}baseNameType" minOccurs="0"/&gt;
 *         &lt;choice&gt;
 *           &lt;element name="nameOnForeignPassport" type="{http://www.ech.ch/xmlns/eCH-0011/8}foreignerNameType" minOccurs="0"/&gt;
 *           &lt;element name="declaredForeignName" type="{http://www.ech.ch/xmlns/eCH-0011/8}foreignerNameType" minOccurs="0"/&gt;
 *         &lt;/choice&gt;
 *       &lt;/sequence&gt;
 *     &lt;/restriction&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "nameDataType", propOrder = {
    "officialName",
    "firstName",
    "originalName",
    "allianceName",
    "aliasName",
    "otherName",
    "callName",
    "nameOnForeignPassport",
    "declaredForeignName"
})
public class NameDataType {

    @XmlElement(required = true)
    @XmlJavaTypeAdapter(CollapsedStringAdapter.class)
    @XmlSchemaType(name = "token")
    protected String officialName;
    @XmlElement(required = true)
    @XmlJavaTypeAdapter(CollapsedStringAdapter.class)
    @XmlSchemaType(name = "token")
    protected String firstName;
    @XmlJavaTypeAdapter(CollapsedStringAdapter.class)
    @XmlSchemaType(name = "token")
    protected String originalName;
    @XmlJavaTypeAdapter(CollapsedStringAdapter.class)
    @XmlSchemaType(name = "token")
    protected String allianceName;
    @XmlJavaTypeAdapter(CollapsedStringAdapter.class)
    @XmlSchemaType(name = "token")
    protected String aliasName;
    @XmlJavaTypeAdapter(CollapsedStringAdapter.class)
    @XmlSchemaType(name = "token")
    protected String otherName;
    @XmlJavaTypeAdapter(CollapsedStringAdapter.class)
    @XmlSchemaType(name = "token")
    protected String callName;
    protected ForeignerNameType nameOnForeignPassport;
    protected ForeignerNameType declaredForeignName;

    /**
     * Obtient la valeur de la propriété officialName.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getOfficialName() {
        return officialName;
    }

    /**
     * Définit la valeur de la propriété officialName.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setOfficialName(String value) {
        this.officialName = value;
    }

    /**
     * Obtient la valeur de la propriété firstName.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getFirstName() {
        return firstName;
    }

    /**
     * Définit la valeur de la propriété firstName.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setFirstName(String value) {
        this.firstName = value;
    }

    /**
     * Obtient la valeur de la propriété originalName.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getOriginalName() {
        return originalName;
    }

    /**
     * Définit la valeur de la propriété originalName.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setOriginalName(String value) {
        this.originalName = value;
    }

    /**
     * Obtient la valeur de la propriété allianceName.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getAllianceName() {
        return allianceName;
    }

    /**
     * Définit la valeur de la propriété allianceName.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setAllianceName(String value) {
        this.allianceName = value;
    }

    /**
     * Obtient la valeur de la propriété aliasName.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getAliasName() {
        return aliasName;
    }

    /**
     * Définit la valeur de la propriété aliasName.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setAliasName(String value) {
        this.aliasName = value;
    }

    /**
     * Obtient la valeur de la propriété otherName.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getOtherName() {
        return otherName;
    }

    /**
     * Définit la valeur de la propriété otherName.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setOtherName(String value) {
        this.otherName = value;
    }

    /**
     * Obtient la valeur de la propriété callName.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getCallName() {
        return callName;
    }

    /**
     * Définit la valeur de la propriété callName.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setCallName(String value) {
        this.callName = value;
    }

    /**
     * Obtient la valeur de la propriété nameOnForeignPassport.
     * 
     * @return
     *     possible object is
     *     {@link ForeignerNameType }
     *     
     */
    public ForeignerNameType getNameOnForeignPassport() {
        return nameOnForeignPassport;
    }

    /**
     * Définit la valeur de la propriété nameOnForeignPassport.
     * 
     * @param value
     *     allowed object is
     *     {@link ForeignerNameType }
     *     
     */
    public void setNameOnForeignPassport(ForeignerNameType value) {
        this.nameOnForeignPassport = value;
    }

    /**
     * Obtient la valeur de la propriété declaredForeignName.
     * 
     * @return
     *     possible object is
     *     {@link ForeignerNameType }
     *     
     */
    public ForeignerNameType getDeclaredForeignName() {
        return declaredForeignName;
    }

    /**
     * Définit la valeur de la propriété declaredForeignName.
     * 
     * @param value
     *     allowed object is
     *     {@link ForeignerNameType }
     *     
     */
    public void setDeclaredForeignName(ForeignerNameType value) {
        this.declaredForeignName = value;
    }

}
