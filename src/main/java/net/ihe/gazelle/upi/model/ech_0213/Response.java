
package net.ihe.gazelle.upi.model.ech_0213;

import java.math.BigInteger;
import java.util.ArrayList;
import java.util.List;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlSchemaType;
import javax.xml.bind.annotation.XmlType;
import javax.xml.bind.annotation.adapters.CollapsedStringAdapter;
import javax.xml.bind.annotation.adapters.XmlJavaTypeAdapter;
import net.ihe.gazelle.upi.model.ech_0058.HeaderType;
import net.ihe.gazelle.upi.model.ech_0213_commons.NegativeReportType;
import net.ihe.gazelle.upi.model.ech_0213_commons.PersonFromUPIType;
import net.ihe.gazelle.upi.model.ech_0213_commons.PidsFromUPIType;
import net.ihe.gazelle.upi.model.ech_0213_commons.WarningType;


/**
 * <p>Classe Java pour anonymous complex type.
 * 
 * <p>Le fragment de schéma suivant indique le contenu attendu figurant dans cette classe.
 * 
 * <pre>
 * &lt;complexType&gt;
 *   &lt;complexContent&gt;
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *       &lt;sequence&gt;
 *         &lt;element name="header" type="{http://www.ech.ch/xmlns/eCH-0058/5}headerType"/&gt;
 *         &lt;choice&gt;
 *           &lt;element name="positiveResponse"&gt;
 *             &lt;complexType&gt;
 *               &lt;complexContent&gt;
 *                 &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *                   &lt;sequence&gt;
 *                     &lt;element name="SPIDCategory" type="{http://www.ech.ch/xmlns/eCH-0044/4}personIdCategoryType"/&gt;
 *                     &lt;sequence maxOccurs="unbounded" minOccurs="0"&gt;
 *                       &lt;element name="warning" type="{http://www.ech.ch/xmlns/eCH-0213-commons/1}warningType"/&gt;
 *                     &lt;/sequence&gt;
 *                     &lt;element name="pids" type="{http://www.ech.ch/xmlns/eCH-0213-commons/1}pidsFromUPIType"/&gt;
 *                     &lt;element name="personFromUPI" type="{http://www.ech.ch/xmlns/eCH-0213-commons/1}personFromUPIType"/&gt;
 *                   &lt;/sequence&gt;
 *                 &lt;/restriction&gt;
 *               &lt;/complexContent&gt;
 *             &lt;/complexType&gt;
 *           &lt;/element&gt;
 *           &lt;element name="negativeReport" type="{http://www.ech.ch/xmlns/eCH-0213-commons/1}negativeReportType"/&gt;
 *         &lt;/choice&gt;
 *       &lt;/sequence&gt;
 *       &lt;attribute name="minorVersion" use="required" type="{http://www.w3.org/2001/XMLSchema}integer" /&gt;
 *     &lt;/restriction&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = {
    "header",
    "positiveResponse",
    "negativeReport"
})
@XmlRootElement(name = "response")
public class Response {

    @XmlElement(required = true)
    protected HeaderType header;
    protected PositiveResponse positiveResponse;
    protected NegativeReportType negativeReport;
    @XmlAttribute(name = "minorVersion", required = true)
    protected BigInteger minorVersion;

    /**
     * Obtient la valeur de la propriété header.
     * 
     * @return
     *     possible object is
     *     {@link HeaderType }
     *     
     */
    public HeaderType getHeader() {
        return header;
    }

    /**
     * Définit la valeur de la propriété header.
     * 
     * @param value
     *     allowed object is
     *     {@link HeaderType }
     *     
     */
    public void setHeader(HeaderType value) {
        this.header = value;
    }

    /**
     * Obtient la valeur de la propriété positiveResponse.
     * 
     * @return
     *     possible object is
     *     {@link PositiveResponse }
     *     
     */
    public PositiveResponse getPositiveResponse() {
        return positiveResponse;
    }

    /**
     * Définit la valeur de la propriété positiveResponse.
     * 
     * @param value
     *     allowed object is
     *     {@link PositiveResponse }
     *     
     */
    public void setPositiveResponse(PositiveResponse value) {
        this.positiveResponse = value;
    }

    /**
     * Obtient la valeur de la propriété negativeReport.
     * 
     * @return
     *     possible object is
     *     {@link NegativeReportType }
     *     
     */
    public NegativeReportType getNegativeReport() {
        return negativeReport;
    }

    /**
     * Définit la valeur de la propriété negativeReport.
     * 
     * @param value
     *     allowed object is
     *     {@link NegativeReportType }
     *     
     */
    public void setNegativeReport(NegativeReportType value) {
        this.negativeReport = value;
    }

    /**
     * Obtient la valeur de la propriété minorVersion.
     * 
     * @return
     *     possible object is
     *     {@link BigInteger }
     *     
     */
    public BigInteger getMinorVersion() {
        return minorVersion;
    }

    /**
     * Définit la valeur de la propriété minorVersion.
     * 
     * @param value
     *     allowed object is
     *     {@link BigInteger }
     *     
     */
    public void setMinorVersion(BigInteger value) {
        this.minorVersion = value;
    }


    /**
     * <p>Classe Java pour anonymous complex type.
     * 
     * <p>Le fragment de schéma suivant indique le contenu attendu figurant dans cette classe.
     * 
     * <pre>
     * &lt;complexType&gt;
     *   &lt;complexContent&gt;
     *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
     *       &lt;sequence&gt;
     *         &lt;element name="SPIDCategory" type="{http://www.ech.ch/xmlns/eCH-0044/4}personIdCategoryType"/&gt;
     *         &lt;sequence maxOccurs="unbounded" minOccurs="0"&gt;
     *           &lt;element name="warning" type="{http://www.ech.ch/xmlns/eCH-0213-commons/1}warningType"/&gt;
     *         &lt;/sequence&gt;
     *         &lt;element name="pids" type="{http://www.ech.ch/xmlns/eCH-0213-commons/1}pidsFromUPIType"/&gt;
     *         &lt;element name="personFromUPI" type="{http://www.ech.ch/xmlns/eCH-0213-commons/1}personFromUPIType"/&gt;
     *       &lt;/sequence&gt;
     *     &lt;/restriction&gt;
     *   &lt;/complexContent&gt;
     * &lt;/complexType&gt;
     * </pre>
     * 
     * 
     */
    @XmlAccessorType(XmlAccessType.FIELD)
    @XmlType(name = "", propOrder = {
        "spidCategory",
        "warning",
        "pids",
        "personFromUPI"
    })
    public static class PositiveResponse {

        @XmlElement(name = "SPIDCategory", required = true)
        @XmlJavaTypeAdapter(CollapsedStringAdapter.class)
        @XmlSchemaType(name = "token")
        protected String spidCategory;
        protected List<WarningType> warning;
        @XmlElement(required = true)
        protected PidsFromUPIType pids;
        @XmlElement(required = true)
        protected PersonFromUPIType personFromUPI;

        /**
         * Obtient la valeur de la propriété spidCategory.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getSPIDCategory() {
            return spidCategory;
        }

        /**
         * Définit la valeur de la propriété spidCategory.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setSPIDCategory(String value) {
            this.spidCategory = value;
        }

        /**
         * Gets the value of the warning property.
         * 
         * <p>
         * This accessor method returns a reference to the live list,
         * not a snapshot. Therefore any modification you make to the
         * returned list will be present inside the JAXB object.
         * This is why there is not a <CODE>set</CODE> method for the warning property.
         * 
         * <p>
         * For example, to add a new item, do as follows:
         * <pre>
         *    getWarning().add(newItem);
         * </pre>
         * 
         * 
         * <p>
         * Objects of the following type(s) are allowed in the list
         * {@link WarningType }
         * 
         * 
         */
        public List<WarningType> getWarning() {
            if (warning == null) {
                warning = new ArrayList<WarningType>();
            }
            return this.warning;
        }

        /**
         * Obtient la valeur de la propriété pids.
         * 
         * @return
         *     possible object is
         *     {@link PidsFromUPIType }
         *     
         */
        public PidsFromUPIType getPids() {
            return pids;
        }

        /**
         * Définit la valeur de la propriété pids.
         * 
         * @param value
         *     allowed object is
         *     {@link PidsFromUPIType }
         *     
         */
        public void setPids(PidsFromUPIType value) {
            this.pids = value;
        }

        /**
         * Obtient la valeur de la propriété personFromUPI.
         * 
         * @return
         *     possible object is
         *     {@link PersonFromUPIType }
         *     
         */
        public PersonFromUPIType getPersonFromUPI() {
            return personFromUPI;
        }

        /**
         * Définit la valeur de la propriété personFromUPI.
         * 
         * @param value
         *     allowed object is
         *     {@link PersonFromUPIType }
         *     
         */
        public void setPersonFromUPI(PersonFromUPIType value) {
            this.personFromUPI = value;
        }

    }

}
