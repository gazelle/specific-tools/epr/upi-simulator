
package net.ihe.gazelle.upi.model.ech_0021;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Classe Java pour placeOfOriginAddonRestrictedNaturalizeDataType complex type.
 * 
 * <p>Le fragment de schéma suivant indique le contenu attendu figurant dans cette classe.
 * 
 * <pre>
 * &lt;complexType name="placeOfOriginAddonRestrictedNaturalizeDataType"&gt;
 *   &lt;complexContent&gt;
 *     &lt;restriction base="{http://www.ech.ch/xmlns/eCH-0021/7}placeOfOriginAddonDataType"&gt;
 *       &lt;sequence&gt;
 *         &lt;element name="naturalizationDate" type="{http://www.w3.org/2001/XMLSchema}date"/&gt;
 *       &lt;/sequence&gt;
 *     &lt;/restriction&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "placeOfOriginAddonRestrictedNaturalizeDataType")
public class PlaceOfOriginAddonRestrictedNaturalizeDataType
    extends PlaceOfOriginAddonDataType
{


}
