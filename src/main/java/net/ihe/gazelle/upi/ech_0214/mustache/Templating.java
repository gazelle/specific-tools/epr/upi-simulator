package net.ihe.gazelle.upi.ech_0214.mustache;

import com.github.mustachejava.DefaultMustacheFactory;
import com.github.mustachejava.Mustache;
import com.github.mustachejava.MustacheFactory;

import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBException;
import javax.xml.bind.Unmarshaller;
import java.io.ByteArrayInputStream;
import java.io.InputStream;
import java.io.StringWriter;
import java.io.Writer;
import java.util.HashMap;

public class Templating {
    public net.ihe.gazelle.upi.model.ech_0214.Response createResponse(net.ihe.gazelle.upi.model.ech_0214.Response response) throws JAXBException {
        Writer writer = mustache(response);
        return unmarshall(writer);
    }

    private Writer mustache(net.ihe.gazelle.upi.model.ech_0214.Response response) {
        HashMap<String, Object> scopes = new HashMap<>();
        scopes.put("response", response);
        Writer writer = new StringWriter();
        MustacheFactory mf = new DefaultMustacheFactory();

        Mustache mustache = mf.compile("mustacheTemplates/ech_0214_response.mustache");

        mustache.execute(writer, scopes);
        return writer;

    }

    private net.ihe.gazelle.upi.model.ech_0214.Response unmarshall(Writer writer) throws JAXBException {
        ByteArrayInputStream responseContent = new ByteArrayInputStream(writer.toString().getBytes());
        return unmarshallMessage(net.ihe.gazelle.upi.model.ech_0214.Response.class, responseContent);
    }

    private <T> T unmarshallMessage(Class<T> messageType, InputStream is) throws JAXBException {
        JAXBContext jc = JAXBContext.newInstance(messageType);
        Unmarshaller u = jc.createUnmarshaller();
        T object = (T) u.unmarshal(is);
        return object;
    }
}
