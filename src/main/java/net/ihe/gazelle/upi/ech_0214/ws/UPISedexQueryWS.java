package net.ihe.gazelle.upi.ech_0214.ws;

import net.ihe.gazelle.upi.model.ech_0214.Request;
import net.ihe.gazelle.upi.model.ech_0214.Response;
import net.ihe.gazelle.upi.ech_0214.business.ResponseManager;

import javax.ejb.Stateless;
import javax.jws.WebMethod;
import javax.jws.WebParam;
import javax.jws.WebResult;
import javax.jws.WebService;
import javax.jws.soap.SOAPBinding;

@Stateless
@SOAPBinding(style = SOAPBinding.Style.DOCUMENT, parameterStyle = SOAPBinding.ParameterStyle.BARE)
@WebService(targetNamespace = "http://www.zas.admin.ch/wupispid/ws/queryService/2", name = "UPISedexQueryWS")
public class UPISedexQueryWS {

    @WebMethod
    @WebResult(name = "response", targetNamespace = "http://www.ech.ch/xmlns/eCH-0214/2", partName = "body")
    public Response querySpid(@WebParam(partName = "body", name = "request", targetNamespace = "http://www.ech.ch/xmlns/eCH-0214/2") Request body){
        ResponseManager responseManager = new ResponseManager();
        return responseManager.createResponse(body);
    }


}
