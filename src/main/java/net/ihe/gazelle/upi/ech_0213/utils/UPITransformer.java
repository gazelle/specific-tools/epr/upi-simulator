package net.ihe.gazelle.upi.ech_0213.utils;

import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBException;
import javax.xml.bind.Marshaller;
import javax.xml.bind.Unmarshaller;
import java.io.ByteArrayOutputStream;
import java.io.InputStream;
import java.io.OutputStream;

public class UPITransformer {

    private UPITransformer(){}

    @SuppressWarnings("unchecked")
    public static <T> T  unmarshallMessage(Class<T> messageType, InputStream is) throws JAXBException {
        JAXBContext jc = JAXBContext.newInstance(messageType);
        Unmarshaller u = jc.createUnmarshaller();
        return (T) u.unmarshal(is);
    }

    public static <T> void marshallMessage(Class<T> messageType, OutputStream out, T message) throws JAXBException{
        JAXBContext jc = JAXBContext.newInstance(messageType);
        Marshaller m = jc.createMarshaller();
        m.setProperty(Marshaller.JAXB_FORMATTED_OUTPUT, Boolean.TRUE);
        m.marshal(message, out);
    }

    public static <T> byte[] getMessageAsByteArray(Class<T> messageType, T message) throws JAXBException {
        ByteArrayOutputStream baos = new ByteArrayOutputStream();
        marshallMessage(messageType, baos, message);
        return baos.toByteArray();
    }
}
