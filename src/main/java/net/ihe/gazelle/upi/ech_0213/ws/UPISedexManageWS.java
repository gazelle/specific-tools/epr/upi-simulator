package net.ihe.gazelle.upi.ech_0213.ws;

import net.ihe.gazelle.upi.model.ech_0213.Request;
import net.ihe.gazelle.upi.model.ech_0213.Response;
import net.ihe.gazelle.upi.ech_0213.business.ResponseManager;

import javax.ejb.Stateless;
import javax.jws.WebMethod;
import javax.jws.WebParam;
import javax.jws.WebResult;
import javax.jws.WebService;
import javax.jws.soap.SOAPBinding;


@Stateless
@SOAPBinding(style = SOAPBinding.Style.DOCUMENT, parameterStyle = SOAPBinding.ParameterStyle.BARE)
@WebService(targetNamespace = "http://www.zas.admin.ch/wupispid/ws/managementService/1", name = "UPISedexManageWS")
public class UPISedexManageWS {

    private ResponseManager responseManager = new ResponseManager();

    @WebMethod
    @WebResult(name = "response", targetNamespace = "http://www.ech.ch/xmlns/eCH-0213/", partName = "body")
    public Response manageSPID(@WebParam(partName = "body", name = "request", targetNamespace = "http://www.ech.ch/xmlns/eCH-0213/1") Request body) {
        try {
            return responseManager.createResponse(body);
        } catch (Exception e) {
            e.printStackTrace();
        }
        return null;
    }
}
