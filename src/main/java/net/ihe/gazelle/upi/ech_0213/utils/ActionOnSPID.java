package net.ihe.gazelle.upi.ech_0213.utils;

import java.util.Map;
import java.util.function.Function;
import java.util.stream.Collectors;
import java.util.stream.Stream;

public enum ActionOnSPID {
    GENERATE,
    INACTIVATE,
    CANCEL;

    private static final Map<String, ActionOnSPID> NAME_MAP = Stream.of(values())
            .collect(Collectors.toMap(ActionOnSPID::toString, Function.identity()));

    public static ActionOnSPID fromString(final String action) {
        return NAME_MAP.get(action);
    }
}
