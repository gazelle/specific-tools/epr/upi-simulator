package net.ihe.gazelle.upi.exception;

public class UpiException extends Exception {
    public UpiException() {
        super();
    }

    public UpiException(String message) {
        super(message);
    }

    public UpiException(String message, Throwable cause) {
        super(message, cause);
    }

    public UpiException(Throwable cause) {
        super(cause);
    }

    protected UpiException(String message, Throwable cause,
                           boolean enableSuppression,
                           boolean writableStackTrace) {
        super(message, cause, enableSuppression, writableStackTrace);
    }
}
