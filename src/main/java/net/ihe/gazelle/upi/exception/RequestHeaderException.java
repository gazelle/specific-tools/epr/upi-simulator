package net.ihe.gazelle.upi.exception;

public class RequestHeaderException extends Exception {
    public RequestHeaderException() {
        super();
    }

    public RequestHeaderException(String message) {
        super(message);
    }

    public RequestHeaderException(String message, Throwable cause) {
        super(message, cause);
    }

    public RequestHeaderException(Throwable cause) {
        super(cause);
    }

    protected RequestHeaderException(String message, Throwable cause,
                                     boolean enableSuppression,
                                     boolean writableStackTrace) {
        super(message, cause, enableSuppression, writableStackTrace);
    }
}
