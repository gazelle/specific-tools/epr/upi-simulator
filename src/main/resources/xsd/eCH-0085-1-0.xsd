<?xml version="1.0" encoding="UTF-8"?>
<xs:schema
        xmlns:xs="http://www.w3.org/2001/XMLSchema"
        xmlns:eCH-0044="http://www.ech.ch/xmlns/eCH-0044/1"
        xmlns:eCH-0011="http://www.ech.ch/xmlns/eCH-0011/3"
        xmlns:eCH-0072="http://www.ech.ch/xmlns/eCH-0072/1"
        xmlns:eCH-0084="http://www.ech.ch/xmlns/eCH-0084/1"
        xmlns:eCH-0085="http://www.ech.ch/xmlns/eCH-0085/1"
        targetNamespace="http://www.ech.ch/xmlns/eCH-0085/1"
        elementFormDefault="qualified" attributeFormDefault="unqualified" version="1.0">
	
	<xs:annotation>
		<xs:documentation xml:lang="en">
		  UPI (unique person identifier) query interface.
      Schema for UPI related query requests and response messages.      
      
      $Id: eCH-0085-1-0.xsd,v 1.7 2009-03-20 12:55:18 tqn Exp $
		</xs:documentation>
	</xs:annotation>
  
  <xs:import namespace="http://www.ech.ch/xmlns/eCH-0011/3" schemaLocation="eCH-0011-3-0.xsd"/>
  <xs:import namespace="http://www.ech.ch/xmlns/eCH-0044/1" schemaLocation="eCH-0044-1-0.xsd"/>
  <xs:import namespace="http://www.ech.ch/xmlns/eCH-0072/1" schemaLocation="eCH-0072-1-0.xsd"/>
  <xs:import namespace="http://www.ech.ch/xmlns/eCH-0084/1" schemaLocation="eCH-0084-commons-1-0.xsd"/>
  
  <xs:element name="searchPersonRequest">
		<xs:annotation>
			<xs:documentation>Searches a person in the UPI without his ID as input.</xs:documentation>
		</xs:annotation>
		<xs:complexType>
			<xs:sequence>
			  <xs:element name="firstNames" type="eCH-0084:baseNameUPI_Type">
					<xs:annotation>
						<xs:documentation>Mandatory</xs:documentation>
					</xs:annotation>
				</xs:element>
			  <xs:element name="officialName" type="eCH-0084:baseNameUPI_Type">
					<xs:annotation>
						<xs:documentation>Mandatory</xs:documentation>
					</xs:annotation>
				</xs:element>
			  <xs:element name="originalName" type="eCH-0084:baseNameUPI_Type" minOccurs="0"/>
				<xs:element name="sex" type="eCH-0044:sexType" minOccurs="0"/>
				<xs:element name="dateOfBirth" type="eCH-0044:datePartiallyKnownType">
					<xs:annotation>
						<xs:documentation>Mandatory</xs:documentation>
					</xs:annotation>
				</xs:element>
				<xs:element name="nationality">
					<xs:complexType>
						<xs:sequence>
							<xs:element name="nationalityStatus" type="eCH-0011:nationalityStatusType"/>
							<xs:element name="countryId" type="eCH-0072:countryIdType" minOccurs="0"/>
						</xs:sequence>
					</xs:complexType>
				</xs:element>
			  <xs:element name="placeOfBirth" type="eCH-0084:placeOfBirthType"/>
			  <xs:element name="mothersName" type="eCH-0084:fullName_Type" minOccurs="0"/>
			  <xs:element name="fathersName" type="eCH-0084:fullName_Type" minOccurs="0"/>
			</xs:sequence>
		</xs:complexType>
  </xs:element>
	
	<xs:element name="searchPersonResponse">
		<xs:annotation>
			<xs:documentation xml:lang="en">
				Response to a "searchPerson" request.
			</xs:documentation>
		</xs:annotation>
		<xs:complexType>
			<xs:sequence>
				<xs:element name="timestamp" type="xs:dateTime">
					<xs:annotation>
						<xs:documentation xml:lang="en">The time, when the response was generated.</xs:documentation>
					</xs:annotation>
				</xs:element>
				<xs:choice>
					<xs:element name="accepted">
						<xs:annotation>
							<xs:documentation xml:lang="en">
								Is present, if the query has been processed successfully.
							</xs:documentation>
						</xs:annotation>
						<xs:complexType>
							<xs:choice>
								<xs:element name="found">
									<xs:annotation>
										<xs:documentation>The presence of this tag means that exactly on person matches the search criteria</xs:documentation>
									</xs:annotation>
									<xs:complexType>
										<xs:sequence>
											<xs:element name="ahvvn" type="eCH-0044:vnType"/>
										  <xs:element name="valuesStoredUnderAhvvn" type="eCH-0084:valuesStoredUnderAhvvn_Type">
												<xs:annotation>
													<xs:documentation xml:lang="en">This element  contains the current values (recorded in the UPI at the moment mentioned in the element called timestamp) of the attributes of the person having the AHVN contained in the element called ahvn. </xs:documentation>
												</xs:annotation>
											</xs:element>
										</xs:sequence>
									</xs:complexType>
								</xs:element>
								<xs:element name="maybeFound">
									<xs:annotation>
										<xs:documentation>The presence of this tag means that one or several candidates match (eventually only roughly) the search criteria. Please check if one candidate is the person you search.</xs:documentation>
									</xs:annotation>
									<xs:complexType>
										<xs:sequence>
											<xs:element name="candidate" maxOccurs="unbounded">
												<xs:annotation>
													<xs:documentation>Listed by decreasing order of likeliness.</xs:documentation>
												</xs:annotation>
												<xs:complexType>
													<xs:sequence>
														<xs:element name="ahvvn" type="eCH-0044:vnType"/>
													  <xs:element name="valuesStoredUnderAhvvn" type="eCH-0084:valuesStoredUnderAhvvn_Type">
															<xs:annotation>
																<xs:documentation xml:lang="en">This element contains the current values (recorded in the UPI at the moment mentioned in the element called timestamp) of the attributes of the person having the AHVN contained in the element called ahvvn. </xs:documentation>
															</xs:annotation>
														</xs:element>
													  <xs:element name="historicalValues" type="eCH-0084:personInformationShortOptType" minOccurs="0" maxOccurs="unbounded">
															<xs:annotation>
																<xs:documentation>In chronological order (oldest first). The presence of this element makes it possible to find a person on the basis of old attibute values.</xs:documentation>
															</xs:annotation>
														</xs:element>
													</xs:sequence>
												</xs:complexType>
											</xs:element>
										</xs:sequence>
									</xs:complexType>
								</xs:element>
								<xs:element name="notFound">
									<xs:annotation>
										<xs:documentation>The presence of this tag means that no person matching the search criteria has been found in the UPI.</xs:documentation>
									</xs:annotation>
								</xs:element>
							</xs:choice>
						</xs:complexType>
					</xs:element>
					<xs:element name="refused">
						<xs:annotation>
							<xs:documentation xml:lang="en">
								Is present, if the query contained an error (or to few search criteria) and has not been processed normally.
							</xs:documentation>
						</xs:annotation>
						<xs:complexType>
							<xs:sequence>
								<xs:element name="reason">
									<xs:annotation>
										<xs:documentation xml:lang="en">The reason is for refusing is: 1 = not valid against XSD or an other 
										  formal reason (not self consistent), or 2 = one "almost match" found, more search criteria needed, 
										  3 =  more than one match found, more search criteria needed, or 10 = service not available, 
										  try later.
										</xs:documentation>
									</xs:annotation>
									<xs:simpleType>
										<xs:restriction base="xs:short">
											<xs:enumeration value="1"/>
										  <xs:enumeration value="2"/>
											<xs:enumeration value="3"/>
										  <xs:enumeration value="10"/>
										</xs:restriction>
									</xs:simpleType>
								</xs:element>
								<xs:element name="detailedReason" minOccurs="0">
									<xs:annotation>
										<xs:documentation xml:lang="en">Starts with the detailed error code followed by a short standard text.</xs:documentation>
									</xs:annotation>
									<xs:simpleType>
										<xs:restriction base="xs:string">
											<xs:maxLength value="50"/>
										</xs:restriction>
									</xs:simpleType>
								</xs:element>
								<xs:element name="comment" minOccurs="0">
									<xs:annotation>
										<xs:documentation xml:lang="en">Manually added free form comment.</xs:documentation>
									</xs:annotation>
									<xs:simpleType>
										<xs:restriction base="xs:string">
											<xs:maxLength value="100"/>
										</xs:restriction>
									</xs:simpleType>
								</xs:element>
							</xs:sequence>
						</xs:complexType>
					</xs:element>
				</xs:choice>
			</xs:sequence>
		</xs:complexType>
	</xs:element>
	
	<xs:element name="getInfoPersonRequest">
		<xs:annotation>
			<xs:documentation>Gets the informations stored under a given persons ID</xs:documentation>
		</xs:annotation>
		<xs:complexType>
			<xs:sequence>
				<xs:element name="ahvvn" type="eCH-0044:vnType"/>
			</xs:sequence>
		</xs:complexType>
	</xs:element>

	<xs:element name="getInfoPersonResponse">
		<xs:annotation>
			<xs:documentation xml:lang="en">
				Response to a "getInfoPerson" request.
			</xs:documentation>
		</xs:annotation>
		<xs:complexType>
			<xs:sequence>
				<xs:element name="timestamp" type="xs:dateTime">
					<xs:annotation>
						<xs:documentation xml:lang="en">The time, when the response was generated.</xs:documentation>
					</xs:annotation>
				</xs:element>
				<xs:element name="InputAhvvn" type="eCH-0044:vnType">
					<xs:annotation>
						<xs:documentation>Ahvvn sent as query parameter.</xs:documentation>
					</xs:annotation>
				</xs:element>
				<xs:choice>
					<xs:element name="accepted">
						<xs:annotation>
							<xs:documentation xml:lang="en">
								Is present, if the query has been processed successfully.
							</xs:documentation>
						</xs:annotation>
						<xs:complexType>
							<xs:sequence>
								<xs:element name="latestAhvvn" type="eCH-0044:vnType">
									<xs:annotation>
										<xs:documentation>If a person has more that one ahvvn than the latest one is sent.</xs:documentation>
									</xs:annotation>
								</xs:element>
								<xs:element name="valuesStoredUnderAhvvn" type="eCH-0084:valuesStoredUnderAhvvn_Type">
									<xs:annotation>
										<xs:documentation xml:lang="en">This element  contains the current values (recorded in the UPI at the moment mentioned in the element called timestamp) of the attributes of the person having the AHVN contained in the element called latestAhvn. </xs:documentation>
									</xs:annotation>
								</xs:element>
							</xs:sequence>
						</xs:complexType>
					</xs:element>
					<xs:element name="refused">
						<xs:annotation>
							<xs:documentation xml:lang="en">
								Is present, if the query contained an error (or that the person ID "ahvvn" sent as query parameter was not found in the UPI) and has not been processed normally.
							</xs:documentation>
						</xs:annotation>
						<xs:complexType>
							<xs:sequence>
							  <xs:element name="reason">
							    <xs:annotation>
							      <xs:documentation xml:lang="en">
							        The reason is for refusing is: 1 = not valid against XSD or an other formal reason (wrong 
							        control number of ahvn for instance), or 3 = person id not found, or 4 ahvnn no more valid, 
							        or 10 = service not available.
							      </xs:documentation>
							    </xs:annotation>
							    <xs:simpleType>
							      <xs:restriction base="xs:short">
							        <xs:enumeration value="1"/>
							        <xs:enumeration value="3"/>
							        <xs:enumeration value="4"/>
							        <xs:enumeration value="10"/>
							      </xs:restriction>
							    </xs:simpleType>
							  </xs:element>
								<xs:element name="detailedReason" minOccurs="0">
									<xs:annotation>
										<xs:documentation xml:lang="en">Starts with the error code followed by a short standard text.</xs:documentation>
									</xs:annotation>
									<xs:simpleType>
										<xs:restriction base="xs:string">
											<xs:maxLength value="50"/>
										</xs:restriction>
									</xs:simpleType>
								</xs:element>
							</xs:sequence>
						</xs:complexType>
					</xs:element>
				</xs:choice>
			</xs:sequence>
		</xs:complexType>
	</xs:element>
  
  <xs:element name="getAhvvnRequest">
    <xs:annotation>
      <xs:documentation>Gets the latest Ahvvn for "any" given person identifier</xs:documentation>
    </xs:annotation>
    <xs:complexType>
      <xs:sequence>
        <xs:element name="personIdentification" type="eCH-0085:personIdentificationType"/>
      </xs:sequence>
    </xs:complexType>
  </xs:element>
  
  <xs:element name="getAhvvnResponse">
    <xs:annotation>
      <xs:documentation xml:lang="en">
        Response to a "getAhvvnRequest" request.
      </xs:documentation>
    </xs:annotation>
    <xs:complexType>
      <xs:sequence>
        <xs:element name="timestamp" type="xs:dateTime">
          <xs:annotation>
            <xs:documentation xml:lang="en">The time, when the response was generated.</xs:documentation>
          </xs:annotation>
        </xs:element>
        <xs:element name="personIdentification" type="eCH-0085:personIdentificationType">
          <xs:annotation>
            <xs:documentation>Person id sent as query parameter.</xs:documentation>
          </xs:annotation>
        </xs:element>
        <xs:choice>
          <xs:element name="accepted">
            <xs:annotation>
              <xs:documentation xml:lang="en">
                Is present, if the query has been processed successfully.
              </xs:documentation>
            </xs:annotation>
            <xs:complexType>
              <xs:sequence>
                <xs:element name="latestAhvvn" type="eCH-0044:vnType">
                  <xs:annotation>
                    <xs:documentation>If a person has more that one ahvvn than the latest one is sent.</xs:documentation>
                  </xs:annotation>
                </xs:element>
              </xs:sequence>
            </xs:complexType>
          </xs:element>
          <xs:element name="refused">
            <xs:annotation>
              <xs:documentation xml:lang="en">
                Is present, if the query contained an error (or that the person ID "ahvvn" sent as query parameter was not found in the UPI) and has not been processed normally.
              </xs:documentation>
            </xs:annotation>
            <xs:complexType>
              <xs:sequence>
                <xs:element name="reason">
                  <xs:annotation>
                    <xs:documentation xml:lang="en">
                      The reason is for refusing is: 1 = not valid against XSD or an other formal reason (wrong 
                      control number of ahvn for instance), or 3 = person id not found, or 4 ahvnn no more valid, 
                      or 10 = service not available.
                    </xs:documentation>
                  </xs:annotation>
                  <xs:simpleType>
                    <xs:restriction base="xs:short">
                      <xs:enumeration value="1"/>
                      <xs:enumeration value="3"/>
                      <xs:enumeration value="4"/>
                      <xs:enumeration value="10"/>
                    </xs:restriction>
                  </xs:simpleType>
                </xs:element>
                <xs:element name="detailedReason" minOccurs="0">
                  <xs:annotation>
                    <xs:documentation xml:lang="en">Starts with the error code followed by a short standard text.</xs:documentation>
                  </xs:annotation>
                  <xs:simpleType>
                    <xs:restriction base="xs:string">
                      <xs:maxLength value="50"/>
                    </xs:restriction>
                  </xs:simpleType>
                </xs:element>
              </xs:sequence>
            </xs:complexType>
          </xs:element>
        </xs:choice>
      </xs:sequence>
    </xs:complexType>
  </xs:element>
  
  <xs:complexType name="personIdentificationType">
    <xs:sequence>
      <xs:choice>
        <xs:element name="vn" type="eCH-0044:vnType"/>
        <xs:element name="localPersonId" type="eCH-0044:namedPersonIdType">
          <xs:annotation>
            <xs:documentation>Can for instance be used for an Ahvvn with 11 positions, or the STAR Number.</xs:documentation>
          </xs:annotation>
        </xs:element>
        <xs:element name="EuPersonId" type="eCH-0044:namedPersonIdType"/>
      </xs:choice>
    </xs:sequence>
  </xs:complexType>
</xs:schema>
